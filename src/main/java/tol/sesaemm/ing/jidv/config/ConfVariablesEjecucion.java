/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.config;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ConfVariablesEjecucion
{

    public static String tokenCLIENT = "usuario";
    public static String tokenSECRET = "secreto";

    private String HOST = "";
    private String USUARIO = "";
    private String BD = "";
    private String CONTRASENIA = "";
    private int PUERTO = 0;
    private String LLAVE_PUBLICA = "";
    private String LLAVE_PRIVADA = "";

    public ConfVariablesEjecucion()
    {
        HOST = "localhost";
        USUARIO = "usuario";
        BD = "plataforma-digital-estatal";
        CONTRASENIA = "contrasenia";
        PUERTO = 27017;
        LLAVE_PUBLICA = "";
        LLAVE_PRIVADA = "";
    }

    public String getHOST()
    {
        return HOST;
    }

    public void setHOST(String HOST)
    {
        this.HOST = HOST;
    }

    public String getUSUARIO()
    {
        return USUARIO;
    }

    public void setUSUARIO(String USUARIO)
    {
        this.USUARIO = USUARIO;
    }

    public String getBD()
    {
        return BD;
    }

    public void setBD(String BD)
    {
        this.BD = BD;
    }

    public String getCONTRASENIA()
    {
        return CONTRASENIA;
    }

    public void setCONTRASENIA(String CONTRASENIA)
    {
        this.CONTRASENIA = CONTRASENIA;
    }

    public int getPUERTO()
    {
        return PUERTO;
    }

    public void setPUERTO(int PUERTO)
    {
        this.PUERTO = PUERTO;
    }

    public String getLLAVE_PUBLICA()
    {
        return LLAVE_PUBLICA;
    }

    public void setLLAVE_PUBLICA(String LLAVE_PUBLICA)
    {
        this.LLAVE_PUBLICA = LLAVE_PUBLICA;
    }

    public String getLLAVE_PRIVADA()
    {
        return LLAVE_PRIVADA;
    }

    public void setLLAVE_PRIVADA(String LLAVE_PRIVADA)
    {
        this.LLAVE_PRIVADA = LLAVE_PRIVADA;
    }
}
