/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Collation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.ing.jidv.config.ConfVariablesEjecucion;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.logicaNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;
import tol.sesaemm.javabeans.s1declaraciones.Declaraciones;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesPagination;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesPost;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesResults;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DAOWs
{

    /**
     * Guardar acciones (eventos) del usuario en el uso de la API en bitacora
     *
     * @param estructuraPost      Estructura para la peticion POST
     * @param valoresDePaginacion Valores de paginacion
     *
     * @param strEstatusConsulta  Estatus de la consulta
     *
     * @throws Exception
     */
    public static void guardarBitacoraDeEventos(Object estructuraPost, Object valoresDePaginacion, String strEstatusConsulta) throws Exception
    { 

        MongoClient conectarBaseDatos;                                      
        MongoDatabase database;                                             
        MongoCollection<Document> coleccion;                                
        DeclaracionesPost estructuraPostDeclaraciones;                      
        DeclaracionesPagination valoresDePaginacionDeclaraciones;           
        ArrayList<Document> arregloListaFiltrosDeBusqueda;                  
        Document documentoBitacora;                                         
        Document documentoObjetos;                                          
        Document documentoObjetosIntervalos;                                
        Date fecha;                                                         
        SimpleDateFormat formateador;                                       
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        fecha = new Date();
        formateador = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        conectarBaseDatos = DAOBase.conectarBaseDatos();

        if (conectarBaseDatos != null)
        { 

            database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
            coleccion = database.getCollection("bitacora_servicios_web");

            arregloListaFiltrosDeBusqueda = new ArrayList<>();
            documentoBitacora = new Document();

            
            if (estructuraPost instanceof DeclaracionesPost)
            { 

                estructuraPostDeclaraciones = (DeclaracionesPost) estructuraPost;

                
                if (estructuraPostDeclaraciones.getQuery().getId() != null)
                { 
                    arregloListaFiltrosDeBusqueda.add(new Document("filtro", "id").append("valor", estructuraPostDeclaraciones.getQuery().getId()));
                } 

                if (estructuraPostDeclaraciones.getQuery().getNombres() != null)
                { 
                    arregloListaFiltrosDeBusqueda.add(new Document("filtro", "nombres").append("valor", estructuraPostDeclaraciones.getQuery().getNombres()));
                } 

                if (estructuraPostDeclaraciones.getQuery().getPrimerApellido() != null)
                { 
                    arregloListaFiltrosDeBusqueda.add(new Document("filtro", "primerApellido").append("valor", estructuraPostDeclaraciones.getQuery().getPrimerApellido()));
                } 

                if (estructuraPostDeclaraciones.getQuery().getSegundoApellido() != null)
                { 
                    arregloListaFiltrosDeBusqueda.add(new Document("filtro", "segundoApellido").append("valor", estructuraPostDeclaraciones.getQuery().getSegundoApellido()));
                } 

                if (estructuraPostDeclaraciones.getQuery().getEscolaridadNivel() != null)
                { 
                    arregloListaFiltrosDeBusqueda.add(new Document("filtro", "escolaridadNivel").append("valor", estructuraPostDeclaraciones.getQuery().getEscolaridadNivel()));
                } 

                if (estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision() != null)
                { 

                    documentoObjetos = new Document();

                    if (estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision().getNombreEntePublico() != null)
                    { 
                        documentoObjetos.append("nombreEntePublico", estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision().getNombreEntePublico());
                    } 

                    if (estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision().getEntidadFederativa() != null)
                    { 
                        documentoObjetos.append("entidadFederativa", estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision().getEntidadFederativa());
                    } 

                    if (estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision().getMunicipioAlcaldia() != null)
                    { 
                        documentoObjetos.append("municipioAlcaldia", estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision().getMunicipioAlcaldia());
                    } 

                    if (estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision().getEmpleoCargoComision() != null)
                    { 
                        documentoObjetos.append("empleoCargoComision", estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision().getEmpleoCargoComision());
                    } 

                    if (estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision().getNivelOrdenGobierno() != null)
                    { 
                        documentoObjetos.append("nivelOrdenGobierno", estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision().getNivelOrdenGobierno());
                    } 

                    if (estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision().getNivelEmpleoCargoComision() != null)
                    { 
                        documentoObjetos.append("nivelEmpleoCargoComision", estructuraPostDeclaraciones.getQuery().getDatosEmpleoCargoComision().getNivelEmpleoCargoComision());
                    } 

                    if (!documentoObjetos.isEmpty())
                    { 
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "datosEmpleoCargoComision").append("valor", documentoObjetos));
                    } 
                } 
                
                documentoObjetos = new Document();

                if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieConstruccion() != null)
                { 

                    documentoObjetosIntervalos = new Document();

                    if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMin() != null && estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMax() != null)
                    { 
                        documentoObjetosIntervalos
                                .append("min", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMin())
                                .append("max", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMax());
                    } 
                    else if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMin() != null)
                    { 
                        documentoObjetosIntervalos.append("min", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMin());
                    } 
                    else if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMax() != null)
                    { 
                        documentoObjetosIntervalos.append("max", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMax());
                    } 

                    if (!documentoObjetosIntervalos.isEmpty())
                    { 
                        documentoObjetos.append("superficieConstruccion", documentoObjetosIntervalos);
                    } 
                } 

                if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieTerreno() != null)
                { 

                    documentoObjetosIntervalos = new Document();

                    if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieTerreno().getMin() != null && estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieTerreno().getMax() != null)
                    { 
                        documentoObjetosIntervalos
                                .append("min", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieTerreno().getMin())
                                .append("max", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieTerreno().getMax());
                    } 
                    else if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieTerreno().getMin() != null)
                    { 
                        documentoObjetosIntervalos.append("min", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieTerreno().getMin());
                    } 
                    else if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieTerreno().getMax() != null)
                    { 
                        documentoObjetosIntervalos.append("max", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getSuperficieTerreno().getMax());
                    } 

                    if (!documentoObjetosIntervalos.isEmpty())
                    { 
                        documentoObjetos.append("superficieTerreno", documentoObjetosIntervalos);
                    } 
                } 

                if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getFormaAdquisicion() != null)
                { 
                    documentoObjetos.append("formaAdquisicion", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getFormaAdquisicion());
                } 

                if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getValorAdquisicion() != null)
                { 

                    documentoObjetosIntervalos = new Document();

                    if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getValorAdquisicion().getMin() != null && estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getValorAdquisicion().getMax() != null)
                    { 
                        documentoObjetosIntervalos
                                .append("min", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getValorAdquisicion().getMin())
                                .append("max", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getValorAdquisicion().getMax());
                    } 
                    else if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getValorAdquisicion().getMin() != null)
                    { 
                        documentoObjetosIntervalos.append("min", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getValorAdquisicion().getMin());
                    } 
                    else if (estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getValorAdquisicion().getMax() != null)
                    { 
                        documentoObjetosIntervalos.append("max", estructuraPostDeclaraciones.getQuery().getBienesInmuebles().getValorAdquisicion().getMax());
                    } 

                    if (!documentoObjetosIntervalos.isEmpty())
                    { 
                        documentoObjetos.append("valorAdquisicion", documentoObjetosIntervalos);
                    } 
                } 

                if (!documentoObjetos.isEmpty())
                { 
                    arregloListaFiltrosDeBusqueda.add(new Document("filtro", "bienesInmuebles").append("valor", documentoObjetos));
                } 
               
                if (estructuraPostDeclaraciones.getQuery().getTotalIngresosNetos() != null)
                { 

                    documentoObjetosIntervalos = new Document();

                    if (estructuraPostDeclaraciones.getQuery().getTotalIngresosNetos().getMin() != null && estructuraPostDeclaraciones.getQuery().getTotalIngresosNetos().getMax() != null)
                    { 
                        documentoObjetosIntervalos
                                .append("min", estructuraPostDeclaraciones.getQuery().getTotalIngresosNetos().getMin())
                                .append("max", estructuraPostDeclaraciones.getQuery().getTotalIngresosNetos().getMax());
                    } 
                    else if (estructuraPostDeclaraciones.getQuery().getTotalIngresosNetos().getMin() != null)
                    { 
                        documentoObjetosIntervalos.append("min", estructuraPostDeclaraciones.getQuery().getTotalIngresosNetos().getMin());
                    } 
                    else if (estructuraPostDeclaraciones.getQuery().getTotalIngresosNetos().getMax() != null)
                    { 
                        documentoObjetosIntervalos.append("max", estructuraPostDeclaraciones.getQuery().getTotalIngresosNetos().getMax());
                    } 

                    if (!documentoObjetosIntervalos.isEmpty())
                    { 
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "totalIngresosNetos").append("valor", documentoObjetosIntervalos));
                    } 
                } 
                
                if (estructuraPostDeclaraciones.getQuery().getRfcSolicitante() != null)
                { 

                    documentoBitacora
                            .append("datosGeneralesDeConsulta",
                                    new Document()
                                            .append("fechaConsulta", formateador.format(fecha))
                                            .append("rfcSolicitante", estructuraPostDeclaraciones.getQuery().getRfcSolicitante())
                                            .append("privacidadConsulta", "PRV")
                                            .append("sistemaConsultado", "Declaraciones")
                            );
                } 
                else
                { 

                    documentoBitacora
                            .append("datosGeneralesDeConsulta",
                                    new Document()
                                            .append("fechaConsulta", formateador.format(fecha))
                                            .append("rfcSolicitante", (estructuraPostDeclaraciones.getQuery().getRfcSolicitante() != null ? estructuraPostDeclaraciones.getQuery().getRfcSolicitante() : ""))
                                            .append("privacidadConsulta", "PUB")
                                            .append("sistemaConsultado", "Declaraciones")
                            );
                } 
                
                if (valoresDePaginacion != null)
                { 

                    valoresDePaginacionDeclaraciones = (DeclaracionesPagination) valoresDePaginacion;

                    documentoBitacora
                            .append("resultadosDeConsulta",
                                    new Document()
                                            .append("filtrosDeBusqueda", arregloListaFiltrosDeBusqueda)
                                            .append("estatusConsulta", strEstatusConsulta)
                                            .append("numeroDeRegistrosConsultados", valoresDePaginacionDeclaraciones.getTotalRows())
                            );
                } 
                else
                { 

                    documentoBitacora
                            .append("resultadosDeConsulta",
                                    new Document()
                                            .append("filtrosDeBusqueda", arregloListaFiltrosDeBusqueda)
                                            .append("estatusConsulta", strEstatusConsulta)
                                            .append("numeroDeRegistrosConsultados", 0)
                            );
                } 
                
            } 
            coleccion.insertOne(documentoBitacora);
            conectarBaseDatos.close();
        } 
    } 

    /**
     * Obtener listado de usuarios token de acceso
     *
     * @throws Exception
     * @return arreglo de usuarios token de acceso
     */
    public static ArrayList<USUARIO_TOKEN_ACCESO> obtenerUsuariosTokenDeAcceso() throws Exception
    { 

        ArrayList<USUARIO_TOKEN_ACCESO> arregloUsuariosTokenAcceso;             
        MongoClient conectarBaseDatos;                                          
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        ArrayList<Document> query;                                              
        Collation collation = Collation.builder().locale("es").caseLevel(true).build();
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                collection = database.getCollection("usuarios_token_acceso");
                query = new ArrayList<>();
                query.add(new Document("$match", new Document("activo", new Document("$eq", 1))));
                arregloUsuariosTokenAcceso = collection.aggregate(query, USUARIO_TOKEN_ACCESO.class).collation(collation).into(new ArrayList<>());
                conectarBaseDatos.close();
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener usuarios token acceso: " + ex.toString());
        } 

        return arregloUsuariosTokenAcceso;
    } 

    
    /**
     * Obtener listado de declaraciones por alta, modificacion o conclusion
     *
     * @param estructuraPost Estructura para la peticion POST
     *
     * @return listado de declaraciones por alta, modificacion o conclusion
     *
     * @throws Exception
     */
    public static Declaraciones obtenerDeclaracionesPost(DeclaracionesPost estructuraPost) throws Exception
    { 

        LogicaDeNegocio logicaNegocio = new LogicaDeNegocioV01();                   
        ArrayList<DeclaracionesResults> listaDeclaraciones;                         
        Declaraciones resultadoConsulta;                                            
        MongoClient conectarBaseDatos;                                              
        MongoDatabase database;                                                     
        MongoCollection<Document> coleccion;                                        
        Collation collation;                                                        
        AggregateIterable<Document> iteradorDeResultados;                           
        MongoCursor<Document> cursorDeResultados;                                   
        Document where = new Document();                                            
        Document order = new Document();                                            
        int intNumeroDocumentosEncontrados = 0;                                     
        int intNumeroSaltos = 0;                                                    
        boolean hasNextPage = false;                                                
        DeclaracionesPagination declaracionesPagination;                            
        Document documentoCondicionSuperficieConstruccion = new Document();         
        List<Document> arregloSuperficieConstruccionCondicion;                      
        Document documentoCondicionSuperficieTerreno = new Document();              
        List<Document> arregloSuperficieTerrenoCondicion;                           
        Document documentoCondicionFormaAdquisicion = new Document();               
        Document documentoCondicionValorAdquisicion = new Document();               
        List<Document> arregloValorAdquisicionCondicion;                            
        List<Document> arregloTotalIngresosMensualesNetosCondicion;                 
        Document arregloOrdenamientoFiltrosMultiplesValores = new Document();       
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {
            
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                coleccion = database.getCollection("declaraciones_servidores");

                
                collation = Collation.builder()
                        .locale("es")
                        .caseLevel(true)
                        .build();
                

                
                if (estructuraPost.getQuery().getId() != null && !estructuraPost.getQuery().getId().isEmpty())
                { 
                    where.append("identificador", estructuraPost.getQuery().getId());
                } 
                else
                { 

                    if (estructuraPost.getQuery().getNombres() != null && !estructuraPost.getQuery().getNombres().isEmpty())
                    { 
                        where.append("filtros.nombres",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getNombres())).append("$options", "i"));
                    } 

                    if (estructuraPost.getQuery().getPrimerApellido() != null && !estructuraPost.getQuery().getPrimerApellido().isEmpty())
                    { 
                        where.append("filtros.primerApellido",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getPrimerApellido())).append("$options", "i"));
                    } 

                    if (estructuraPost.getQuery().getSegundoApellido() != null && !estructuraPost.getQuery().getSegundoApellido().isEmpty())
                    { 
                        where.append("filtros.segundoApellido",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getSegundoApellido())).append("$options", "i"));
                    } 

                    if (estructuraPost.getQuery().getEscolaridadNivel() != null && !estructuraPost.getQuery().getEscolaridadNivel().isEmpty())
                    { 
                        where.append("filtros.escolaridadNivel",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getEscolaridadNivel())).append("$options", "i"));
                    } 

                    if (estructuraPost.getQuery().getDatosEmpleoCargoComision() != null)
                    { 
                        if (estructuraPost.getQuery().getDatosEmpleoCargoComision().getNombreEntePublico() != null && !estructuraPost.getQuery().getDatosEmpleoCargoComision().getNombreEntePublico().isEmpty())
                        { 
                            where.append("filtros.datosEmpleoCargoComision.nombreEntePublico",
                                    new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getDatosEmpleoCargoComision().getNombreEntePublico())).append("$options", "i"));
                        } 

                        if (estructuraPost.getQuery().getDatosEmpleoCargoComision().getEntidadFederativa() != null && !estructuraPost.getQuery().getDatosEmpleoCargoComision().getEntidadFederativa().isEmpty())
                        { 
                            where.append("filtros.datosEmpleoCargoComision.entidadFederativa", estructuraPost.getQuery().getDatosEmpleoCargoComision().getEntidadFederativa());
                        } 

                        if (estructuraPost.getQuery().getDatosEmpleoCargoComision().getMunicipioAlcaldia() != null && !estructuraPost.getQuery().getDatosEmpleoCargoComision().getMunicipioAlcaldia().isEmpty())
                        { 
                            where.append("filtros.datosEmpleoCargoComision.municipioAlcaldia", estructuraPost.getQuery().getDatosEmpleoCargoComision().getMunicipioAlcaldia());
                        } 

                        if (estructuraPost.getQuery().getDatosEmpleoCargoComision().getEmpleoCargoComision() != null && !estructuraPost.getQuery().getDatosEmpleoCargoComision().getEmpleoCargoComision().isEmpty())
                        { 
                            where.append("filtros.datosEmpleoCargoComision.empleoCargoComision",
                                    new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getDatosEmpleoCargoComision().getEmpleoCargoComision())).append("$options", "i"));
                        } 

                        if (estructuraPost.getQuery().getDatosEmpleoCargoComision().getNivelOrdenGobierno() != null && !estructuraPost.getQuery().getDatosEmpleoCargoComision().getNivelOrdenGobierno().isEmpty())
                        { 
                            where.append("filtros.datosEmpleoCargoComision.nivelOrdenGobierno",
                                    new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getDatosEmpleoCargoComision().getNivelOrdenGobierno())).append("$options", "i"));
                        } 

                        if (estructuraPost.getQuery().getDatosEmpleoCargoComision().getNivelEmpleoCargoComision() != null && !estructuraPost.getQuery().getDatosEmpleoCargoComision().getNivelEmpleoCargoComision().isEmpty())
                        { 
                            where.append("filtros.datosEmpleoCargoComision.nivelEmpleoCargoComision",
                                    new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getDatosEmpleoCargoComision().getNivelEmpleoCargoComision())).append("$options", "i"));
                        } 
                    } 
                    
                    if (estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion() != null)
                    { 
                        if (estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMin() != null && estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMax() == null)
                        { 
                            documentoCondicionSuperficieConstruccion.append("$gte", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMin()));
                            where.append("tamanioArregloSuperficieConstruccionQuery", new Document("$gt", 0));
                        } 
                        else if (estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMax() != null && estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMin() == null)
                        { 
                            documentoCondicionSuperficieConstruccion.append("$lte", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMax()));
                            where.append("tamanioArregloSuperficieConstruccionQuery", new Document("$gt", 0));
                        } 
                        else if (estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMin() != null && estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMax() != null)
                        { 
                            if (estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMin().equals(estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMax()))
                            { 
                                documentoCondicionSuperficieConstruccion.append("$eq", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMin()));
                                where.append("tamanioArregloSuperficieConstruccionQuery", new Document("$gt", 0));
                            } 
                            else
                            { 
                                arregloSuperficieConstruccionCondicion = new ArrayList<>();
                                arregloSuperficieConstruccionCondicion.add(new Document("$gte", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMin())));
                                arregloSuperficieConstruccionCondicion.add(new Document("$lte", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getSuperficieConstruccion().getMax())));
                                documentoCondicionSuperficieConstruccion.append("$and", arregloSuperficieConstruccionCondicion);
                                where.append("tamanioArregloSuperficieConstruccionQuery", new Document("$gt", 0));
                            } 
                        } 
                    } 
                    
                    if (estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno() != null)
                    { 
                        if (estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMin() != null && estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMax() == null)
                        { 
                            documentoCondicionSuperficieTerreno.append("$gte", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMin()));
                            where.append("tamanioArregloSuperficieTerrenoQuery", new Document("$gt", 0));
                        } 
                        else if (estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMax() != null && estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMin() == null)
                        { 
                            documentoCondicionSuperficieTerreno.append("$lte", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMax()));
                            where.append("tamanioArregloSuperficieTerrenoQuery", new Document("$gt", 0));
                        } 
                        else if (estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMin() != null && estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMax() != null)
                        { 
                            if (estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMin().equals(estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMax()))
                            { 
                                documentoCondicionSuperficieTerreno.append("$eq", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMin()));
                                where.append("tamanioArregloSuperficieTerrenoQuery", new Document("$gt", 0));
                            } 
                            else
                            { 
                                arregloSuperficieTerrenoCondicion = new ArrayList<>();
                                arregloSuperficieTerrenoCondicion.add(new Document("$gte", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMin())));
                                arregloSuperficieTerrenoCondicion.add(new Document("$lte", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getSuperficieTerreno().getMax())));
                                documentoCondicionSuperficieTerreno.append("$and", arregloSuperficieTerrenoCondicion);
                                where.append("tamanioArregloSuperficieTerrenoQuery", new Document("$gt", 0));
                            } 
                        } 
                    } 
                    
                    if (estructuraPost.getQuery().getBienesInmuebles().getFormaAdquisicion() != null && !estructuraPost.getQuery().getBienesInmuebles().getFormaAdquisicion().isEmpty())
                    { 
                        documentoCondicionFormaAdquisicion.append("$regexMatch",
                                new Document("input", "$$d").append("regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getBienesInmuebles().getFormaAdquisicion())).append("options", "i"));
                        where.append("tamanioArregloFormaAdquisicionQuery", new Document("$gt", 0));
                    } 

                    if (estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion() != null)
                    { 
                        if (estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMin() != null && estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMax() == null)
                        { 
                            documentoCondicionValorAdquisicion.append("$gte", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMin()));
                            where.append("tamanioArregloValorAdquisicionQuery", new Document("$gt", 0));
                        } 
                        else if (estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMax() != null && estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMin() == null)
                        { 
                            documentoCondicionValorAdquisicion.append("$lte", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMax()));
                            where.append("tamanioArregloValorAdquisicionQuery", new Document("$gt", 0));
                        } 
                        else if (estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMin() != null && estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMax() != null)
                        { 
                            if (estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMin().equals(estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMax()))
                            { 
                                documentoCondicionValorAdquisicion.append("$eq", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMin()));
                                where.append("tamanioArregloValorAdquisicionQuery", new Document("$gt", 0));
                            } 
                            else
                            { 
                                arregloValorAdquisicionCondicion = new ArrayList<>();
                                arregloValorAdquisicionCondicion.add(new Document("$gte", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMin())));
                                arregloValorAdquisicionCondicion.add(new Document("$lte", Arrays.asList("$$d", estructuraPost.getQuery().getBienesInmuebles().getValorAdquisicion().getMax())));
                                documentoCondicionValorAdquisicion.append("$and", arregloValorAdquisicionCondicion);
                                where.append("tamanioArregloValorAdquisicionQuery", new Document("$gt", 0));
                            } 
                        } 
                    } 
                    
                    if (estructuraPost.getQuery().getTotalIngresosNetos() != null)
                    { 
                        if (estructuraPost.getQuery().getTotalIngresosNetos().getMin() != null && estructuraPost.getQuery().getTotalIngresosNetos().getMax() == null)
                        { 
                            where.append("filtros.totalIngresosNetos", new Document("$gte", estructuraPost.getQuery().getTotalIngresosNetos().getMin()));
                        } 
                        else if (estructuraPost.getQuery().getTotalIngresosNetos().getMax() != null && estructuraPost.getQuery().getTotalIngresosNetos().getMin() == null)
                        { 
                            where.append("filtros.totalIngresosNetos", new Document("$lte", estructuraPost.getQuery().getTotalIngresosNetos().getMax()));
                        } 
                        else if (estructuraPost.getQuery().getTotalIngresosNetos().getMin() != null && estructuraPost.getQuery().getTotalIngresosNetos().getMax() != null)
                        { 
                            if (estructuraPost.getQuery().getTotalIngresosNetos().getMin().equals(estructuraPost.getQuery().getTotalIngresosNetos().getMax()))
                            { 
                                where.append("filtros.totalIngresosNetos", new Document("$eq", estructuraPost.getQuery().getTotalIngresosNetos().getMin()));
                            } 
                            else
                            { 
                                arregloTotalIngresosMensualesNetosCondicion = new ArrayList<>();
                                arregloTotalIngresosMensualesNetosCondicion.add(new Document()
                                        .append("filtros.totalIngresosNetos",
                                                new Document("$gte", estructuraPost.getQuery().getTotalIngresosNetos().getMin())));
                                arregloTotalIngresosMensualesNetosCondicion.add(new Document()
                                        .append("filtros.totalIngresosNetos",
                                                new Document("$lte", estructuraPost.getQuery().getTotalIngresosNetos().getMax())));
                                where.append("$and", arregloTotalIngresosMensualesNetosCondicion);
                            } 
                        } 
                    } 
                    
                } 

                where.append("publicar", new Document("$ne", "0"));
                
                if (estructuraPost.getSort().getNombres() != null && !estructuraPost.getSort().getNombres().isEmpty())
                { 
                    if(estructuraPost.getSort().getNombres().toLowerCase().equals("desc"))
                    {
                        order.append("data.filtros.nombres", -1);
                    }
                    else if(estructuraPost.getSort().getNombres().toLowerCase().equals("asc"))
                    {
                        order.append("data.filtros.nombres", 1);                        
                    }
                } 

                if (estructuraPost.getSort().getPrimerApellido() != null && !estructuraPost.getSort().getPrimerApellido().isEmpty())
                { 
                    if(estructuraPost.getSort().getPrimerApellido().toLowerCase().equals("desc"))
                    {
                        order.append("data.filtros.primerApellido", -1); 
                    }
                    else if (estructuraPost.getSort().getPrimerApellido().toLowerCase().equals("asc"))
                    {
                        order.append("data.filtros.primerApellido", 1);
                    }
                } 

                if (estructuraPost.getSort().getSegundoApellido() != null && !estructuraPost.getSort().getSegundoApellido().isEmpty())
                { 
                    if(estructuraPost.getSort().getSegundoApellido().toLowerCase().equals("desc"))
                    {
                        order.append("data.filtros.segundoApellido", -1);
                    }
                    else if(estructuraPost.getSort().getSegundoApellido().toLowerCase().equals("asc"))
                    {
                        order.append("data.filtros.segundoApellido", 1);
                    }
                } 

                if (estructuraPost.getSort().getEscolaridadNivel() != null && !estructuraPost.getSort().getEscolaridadNivel().isEmpty())
                { 
                    if(estructuraPost.getSort().getEscolaridadNivel().toLowerCase().equals("desc"))
                    {
                        arregloOrdenamientoFiltrosMultiplesValores
                                .append("ordenEscolaridadNivel", new Document ()
                                        .append("$max", "$data.arregloEscolaridadNivel")
                                );
                        order.append("ordenEscolaridadNivel", -1 );    
                    }
                    else if(estructuraPost.getSort().getEscolaridadNivel().toLowerCase().equals("asc"))
                    {
                        arregloOrdenamientoFiltrosMultiplesValores
                                .append("ordenEscolaridadNivel", new Document ()
                                        .append("$min", "$data.arregloEscolaridadNivel")
                                );
                        order.append("ordenEscolaridadNivel", 1 );   
                    }
                } 

                
                if (estructuraPost.getSort().getDatosEmpleoCargoComision() != null)
                { 
                    if (estructuraPost.getSort().getDatosEmpleoCargoComision().getNombreEntePublico() != null && !estructuraPost.getSort().getDatosEmpleoCargoComision().getNombreEntePublico().isEmpty())
                    { 
                        if(estructuraPost.getSort().getDatosEmpleoCargoComision().getNombreEntePublico().toLowerCase().equals("desc"))
                        {
                            order.append("data.filtros.datosEmpleoCargoComision.nombreEntePublico", -1);                            
                        }
                        else if(estructuraPost.getSort().getDatosEmpleoCargoComision().getNombreEntePublico().toLowerCase().equals("asc"))
                        {
                            order.append("data.filtros.datosEmpleoCargoComision.nombreEntePublico", 1);
                        }
                    } 

                    if (estructuraPost.getSort().getDatosEmpleoCargoComision().getEntidadFederativa() != null && !estructuraPost.getSort().getDatosEmpleoCargoComision().getEntidadFederativa().isEmpty())
                    { 
                        if(estructuraPost.getSort().getDatosEmpleoCargoComision().getEntidadFederativa().toLowerCase().equals("desc"))
                        {
                            order.append("data.filtros.datosEmpleoCargoComision.entidadFederativa", -1);
                        }
                        else if(estructuraPost.getSort().getDatosEmpleoCargoComision().getEntidadFederativa().toLowerCase().equals("asc"))
                        {
                            order.append("data.filtros.datosEmpleoCargoComision.entidadFederativa", 1);                            
                        }
                    } 

                    if (estructuraPost.getSort().getDatosEmpleoCargoComision().getMunicipioAlcaldia() != null && !estructuraPost.getSort().getDatosEmpleoCargoComision().getMunicipioAlcaldia().isEmpty())
                    { 
                        if(estructuraPost.getSort().getDatosEmpleoCargoComision().getMunicipioAlcaldia().toLowerCase().equals("desc"))
                        {
                            order.append("data.filtros.datosEmpleoCargoComision.municipioAlcaldia", -1);
                        }
                        else if(estructuraPost.getSort().getDatosEmpleoCargoComision().getMunicipioAlcaldia().toLowerCase().equals("asc"))
                        {
                            order.append("data.filtros.datosEmpleoCargoComision.municipioAlcaldia", 1);                            
                        }
                    } 

                    if (estructuraPost.getSort().getDatosEmpleoCargoComision().getEmpleoCargoComision() != null && !estructuraPost.getSort().getDatosEmpleoCargoComision().getEmpleoCargoComision().isEmpty())
                    { 
                        if(estructuraPost.getSort().getDatosEmpleoCargoComision().getEmpleoCargoComision().toLowerCase().equals("desc"))
                        {
                            order.append("data.filtros.datosEmpleoCargoComision.empleoCargoComision", -1);
                        }
                        else if(estructuraPost.getSort().getDatosEmpleoCargoComision().getEmpleoCargoComision().toLowerCase().equals("asc"))
                        {
                            order.append("data.filtros.datosEmpleoCargoComision.empleoCargoComision", 1);
                        }
                    } 

                    if (estructuraPost.getSort().getDatosEmpleoCargoComision().getNivelEmpleoCargoComision() != null && !estructuraPost.getSort().getDatosEmpleoCargoComision().getNivelEmpleoCargoComision().isEmpty())
                    { 
                        if(estructuraPost.getSort().getDatosEmpleoCargoComision().getNivelEmpleoCargoComision().toLowerCase().equals("desc"))
                        {
                            order.append("data.filtros.datosEmpleoCargoComision.nivelEmpleoCargoComision", -1);
                        }
                        else if(estructuraPost.getSort().getDatosEmpleoCargoComision().getNivelEmpleoCargoComision().toLowerCase().equals("asc"))
                        {
                            order.append("data.filtros.datosEmpleoCargoComision.nivelEmpleoCargoComision", 1);                            
                        }
                    } 

                    if (estructuraPost.getSort().getDatosEmpleoCargoComision().getNivelOrdenGobierno() != null && !estructuraPost.getSort().getDatosEmpleoCargoComision().getNivelOrdenGobierno().isEmpty())
                    { 
                        if(estructuraPost.getSort().getDatosEmpleoCargoComision().getNivelOrdenGobierno().toLowerCase().equals("desc"))
                        {
                            order.append("data.filtros.datosEmpleoCargoComision.nivelOrdenGobierno", -1);
                        }
                        else if(estructuraPost.getSort().getDatosEmpleoCargoComision().getNivelOrdenGobierno().toLowerCase().equals("asc"))
                        {
                            order.append("data.filtros.datosEmpleoCargoComision.nivelOrdenGobierno", 1);                            
                        }
                    } 
                } 
                
                if (estructuraPost.getSort().getTotalIngresosNetos() != null && !estructuraPost.getSort().getTotalIngresosNetos().isEmpty())
                { 
                    if(estructuraPost.getSort().getTotalIngresosNetos().toLowerCase().equals("desc"))
                    {
                        order.append("data.filtros.totalIngresosNetos", -1);
                    }
                    else if(estructuraPost.getSort().getTotalIngresosNetos().toLowerCase().equals("asc"))
                    {
                        order.append("data.filtros.totalIngresosNetos", 1);
                    }
                } 

                if (estructuraPost.getSort().getBienesInmuebles() != null)
                { 
                    if (estructuraPost.getSort().getBienesInmuebles().getSuperficieConstruccion() != null && !estructuraPost.getSort().getBienesInmuebles().getSuperficieConstruccion().isEmpty())
                    { 
                        if(estructuraPost.getSort().getBienesInmuebles().getSuperficieConstruccion().toLowerCase().equals("desc"))
                        {
                            arregloOrdenamientoFiltrosMultiplesValores
                                    .append("ordenSuperficieConstruccion", new Document ()
                                            .append("$max", "$data.arregloSuperficieConstruccionQuery")
                                    );
                            order.append("ordenSuperficieConstruccion", -1 );    
                        }
                        else if(estructuraPost.getSort().getBienesInmuebles().getSuperficieConstruccion().toLowerCase().equals("asc"))
                        {
                            arregloOrdenamientoFiltrosMultiplesValores
                                    .append("ordenSuperficieConstruccion", new Document ()
                                            .append("$min", "$data.arregloSuperficieConstruccionQuery")
                                    );
                            order.append("ordenSuperficieConstruccion", 1 );   
                        }
                    } 

                    if (estructuraPost.getSort().getBienesInmuebles().getSuperficieTerreno() != null && !estructuraPost.getSort().getBienesInmuebles().getSuperficieTerreno().isEmpty())
                    { 
                        if(estructuraPost.getSort().getBienesInmuebles().getSuperficieTerreno().toLowerCase().equals("desc"))
                        {
                            arregloOrdenamientoFiltrosMultiplesValores
                                    .append("ordenSuperficieTerreno", new Document ()
                                            .append("$max", "$data.arregloSuperficieTerrenoQuery")
                                    );
                            order.append("ordenSuperficieTerreno", -1 );    
                        }
                        else if(estructuraPost.getSort().getBienesInmuebles().getSuperficieTerreno().toLowerCase().equals("asc"))
                        {
                            arregloOrdenamientoFiltrosMultiplesValores
                                    .append("ordenSuperficieTerreno", new Document ()
                                            .append("$min", "$data.arregloSuperficieTerrenoQuery")
                                    );
                            order.append("ordenSuperficieTerreno", 1 );   
                        }
                    } 

                    if (estructuraPost.getSort().getBienesInmuebles().getFormaAdquisicion() != null && !estructuraPost.getSort().getBienesInmuebles().getFormaAdquisicion().isEmpty())
                    { 
                        if(estructuraPost.getSort().getBienesInmuebles().getFormaAdquisicion().toLowerCase().equals("desc"))
                        {
                            arregloOrdenamientoFiltrosMultiplesValores
                                    .append("ordenFormaAdquisicion", new Document ()
                                            .append("$max", "$data.arregloFormaAdquisicionQuery")
                                    );
                            order.append("ordenFormaAdquisicion", -1 );    
                        }
                        else if(estructuraPost.getSort().getBienesInmuebles().getFormaAdquisicion().toLowerCase().equals("asc"))
                        {
                            arregloOrdenamientoFiltrosMultiplesValores
                                    .append("ordenFormaAdquisicion", new Document ()
                                            .append("$min", "$data.arregloFormaAdquisicionQuery")
                                    );
                            order.append("ordenFormaAdquisicion", 1 );   
                        }
                    } 

                    if (estructuraPost.getSort().getBienesInmuebles().getValorAdquisicion() != null && !estructuraPost.getSort().getBienesInmuebles().getValorAdquisicion().isEmpty())
                    { 
                        if(estructuraPost.getSort().getBienesInmuebles().getValorAdquisicion().toLowerCase().equals("desc"))
                        {
                            arregloOrdenamientoFiltrosMultiplesValores
                                    .append("ordenValorAdquisicion", new Document ()
                                            .append("$max", "$data.arregloValorAdquisicionQuery")
                                    );
                            order.append("ordenValorAdquisicion", -1 );    
                        }
                        else if(estructuraPost.getSort().getBienesInmuebles().getValorAdquisicion().toLowerCase().equals("asc"))
                        {
                            arregloOrdenamientoFiltrosMultiplesValores
                                    .append("ordenValorAdquisicion", new Document ()
                                            .append("$min", "$data.arregloValorAdquisicionQuery")
                                    );
                            order.append("ordenValorAdquisicion", 1 );   
                        }
                    } 
                } 
                
                if (arregloOrdenamientoFiltrosMultiplesValores.isEmpty())
                { 
                    arregloOrdenamientoFiltrosMultiplesValores.append("fechaActualizacion","$data.metadata.actualizacion");
                } 
                
                if (order.isEmpty())
                { 
                    order.append("data.metadata.actualizacion", 1);
                } 
                
                iteradorDeResultados = coleccion.aggregate(
                        Arrays.asList(
                                new Document("$addFields", new Document()
                                        .append("identificador", new Document()
                                                .append("$convert", new Document()
                                                        .append("input", "$_id")
                                                        .append("to", "string")
                                                        .append("onError", "")
                                                        .append("onNull", "")
                                                )
                                        )
                                        .append("arregloEscolaridadNivel", "$filtros.escolaridadNivel")
                                        .append("arregloSuperficieTerreno", "$filtros.bienesInmuebles.superficieTerreno")
                                        .append("arregloSuperficieConstruccion", "$filtros.bienesInmuebles.superficieConstruccion")
                                        .append("arregloValorAdquisicion", "$filtros.bienesInmuebles.valorAdquisicion")
                                        .append("arregloFormaAdquisicion", "$filtros.bienesInmuebles.formaAdquisicion")
                                ),
                                new Document("$addFields" , new Document()
                                        
                                        .append("arregloSuperficieConstruccionQuery", new Document()
                                                .append("$filter", new Document()
                                                        .append("input", "$arregloSuperficieConstruccion")
                                                        .append("as", "d")
                                                        .append("cond", documentoCondicionSuperficieConstruccion)
                                                )
                                        )
                                        .append("arregloSuperficieTerrenoQuery", new Document()
                                                .append("$filter", new Document()
                                                        .append("input", "$arregloSuperficieTerreno")
                                                        .append("as", "d")
                                                        .append("cond", documentoCondicionSuperficieTerreno)
                                                )
                                        )
                                        .append("arregloFormaAdquisicionQuery", new Document()
                                                .append("$filter", new Document()
                                                        .append("input", "$arregloFormaAdquisicion")
                                                        .append("as", "d")
                                                        .append("cond", documentoCondicionFormaAdquisicion)
                                                )
                                        )
                                        .append("arregloValorAdquisicionQuery", new Document()
                                                .append("$filter", new Document()
                                                        .append("input", "$arregloValorAdquisicion")
                                                        .append("as", "d")
                                                        .append("cond", documentoCondicionValorAdquisicion)
                                                )
                                        )
                                ),
                                new Document("$addFields", new Document()
                                
                                        .append("tamanioArregloSuperficieConstruccionQuery", new Document()
                                                .append("$size", "$arregloSuperficieConstruccionQuery")
                                        )
                                        .append("tamanioArregloSuperficieTerrenoQuery", new Document()
                                                .append("$size", "$arregloSuperficieTerrenoQuery")
                                        )
                                        .append("tamanioArregloFormaAdquisicionQuery", new Document()
                                                .append("$size", "$arregloFormaAdquisicionQuery")
                                        )
                                        .append("tamanioArregloValorAdquisicionQuery", new Document()
                                                .append("$size", "$arregloValorAdquisicionQuery")
                                        )
                                ),
                                new Document("$match",
                                        where
                                ),
                                new Document("$count", "totalCount")
                        )
                ).collation(collation).allowDiskUse(true);
                
                cursorDeResultados = iteradorDeResultados.iterator();
                if (cursorDeResultados.hasNext())
                { 
                    intNumeroDocumentosEncontrados = cursorDeResultados.next().getInteger("totalCount");
                } 
                cursorDeResultados.close();
                
                if (estructuraPost.getPage() > 1)
                { 
                    intNumeroSaltos = (int) ((estructuraPost.getPage() - 1) * estructuraPost.getPageSize());
                } 

                if ((estructuraPost.getPage() * estructuraPost.getPageSize()) < intNumeroDocumentosEncontrados)
                { 
                    hasNextPage = true;
                } 
                
                listaDeclaraciones = coleccion.aggregate(
                        Arrays.asList(
                                new Document("$addFields", new Document()
                                        .append("identificador", new Document()
                                                .append("$convert", new Document()
                                                        .append("input", "$_id")
                                                        .append("to", "string")
                                                        .append("onError", "")
                                                        .append("onNull", "")
                                                )
                                        )
                                        .append("arregloEscolaridadNivel", "$filtros.escolaridadNivel")
                                        .append("arregloSuperficieTerreno", "$filtros.bienesInmuebles.superficieTerreno")
                                        .append("arregloSuperficieConstruccion", "$filtros.bienesInmuebles.superficieConstruccion")
                                        .append("arregloValorAdquisicion", "$filtros.bienesInmuebles.valorAdquisicion")
                                        .append("arregloFormaAdquisicion", "$filtros.bienesInmuebles.formaAdquisicion")
                                ),
                                new Document("$addFields" , new Document()
                                        
                                        .append("arregloSuperficieConstruccionQuery", new Document()
                                                .append("$filter", new Document()
                                                        .append("input", "$arregloSuperficieConstruccion")
                                                        .append("as", "d")
                                                        .append("cond", documentoCondicionSuperficieConstruccion)
                                                )
                                        )
                                        .append("arregloSuperficieTerrenoQuery", new Document()
                                                .append("$filter", new Document()
                                                        .append("input", "$arregloSuperficieTerreno")
                                                        .append("as", "d")
                                                        .append("cond", documentoCondicionSuperficieTerreno)
                                                )
                                        )
                                        .append("arregloFormaAdquisicionQuery", new Document()
                                                .append("$filter", new Document()
                                                        .append("input", "$arregloFormaAdquisicion")
                                                        .append("as", "d")
                                                        .append("cond", documentoCondicionFormaAdquisicion)
                                                )
                                        )
                                        .append("arregloValorAdquisicionQuery", new Document()
                                                .append("$filter", new Document()
                                                        .append("input", "$arregloValorAdquisicion")
                                                        .append("as", "d")
                                                        .append("cond", documentoCondicionValorAdquisicion)
                                                )
                                        )
                                ),
                                new Document("$addFields", new Document()
                                
                                        .append("tamanioArregloSuperficieConstruccionQuery", new Document()
                                                .append("$size", "$arregloSuperficieConstruccionQuery")
                                        )
                                        .append("tamanioArregloSuperficieTerrenoQuery", new Document()
                                                .append("$size", "$arregloSuperficieTerrenoQuery")
                                        )
                                        .append("tamanioArregloFormaAdquisicionQuery", new Document()
                                                .append("$size", "$arregloFormaAdquisicionQuery")
                                        )
                                        .append("tamanioArregloValorAdquisicionQuery", new Document()
                                                .append("$size", "$arregloValorAdquisicionQuery")
                                        )
                                
                                ),
                                new Document("$match",
                                        where
                                ),
                                new Document("$project",
                                        new Document("data", "$$ROOT")
                                ),
                                
                                new Document("$addFields",
                                        arregloOrdenamientoFiltrosMultiplesValores
                                ),
                                
                                new Document("$sort",
                                        order
                                ),
                                new Document("$skip", intNumeroSaltos),
                                new Document("$limit", estructuraPost.getPageSize()),
                                new Document("$replaceRoot", new Document("newRoot", "$data"))
                        ), DeclaracionesResults.class).collation(collation).allowDiskUse(true).into(new ArrayList<>());
                
                conectarBaseDatos.close();
                
                declaracionesPagination = new DeclaracionesPagination();
                declaracionesPagination.setPage(estructuraPost.getPage());
                declaracionesPagination.setPageSize(estructuraPost.getPageSize());
                declaracionesPagination.setTotalRows(intNumeroDocumentosEncontrados);
                declaracionesPagination.setHasNextPage(hasNextPage);
                
                resultadoConsulta = new Declaraciones();
                resultadoConsulta.setPagination(declaracionesPagination);
                resultadoConsulta.setResults(listaDeclaraciones);
                
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener las declaraciones: " + ex.toString());
        } 

        return resultadoConsulta;
    } 

    /**
     * Valida RFC
     *
     * @param rfc RFC
     *
     * @return Falso o verdadero
     *
     * @throws Exception
     */
    public static boolean esValidoRFC(String rfc) throws Exception
    { 

        MongoClient conectarBaseDatos;                                          
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        FindIterable<Document> iteradorDeResultados;                            
        MongoCursor<Document> cursorDeResultados;                               
        boolean esValido = false;                                               
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                collection = database.getCollection("usuarios");

                iteradorDeResultados = collection.find(new Document("rfc", rfc));

                cursorDeResultados = iteradorDeResultados.iterator();
                if (cursorDeResultados.hasNext())
                { 
                    esValido = true;
                } 

                cursorDeResultados.close();
                conectarBaseDatos.close();
                
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener privacidad de campos: " + ex.toString());
        } 

        return esValido;
    } 
}
