/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.interfaces;

import java.util.ArrayList;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;
import tol.sesaemm.javabeans.s1declaraciones.Declaraciones;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesPost;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
public interface LogicaDeNegocio
{    
    
    public void guardarBitacoraDeEventos(Object estructuraPost, Object valoresDePaginacion ,String strEstatusConsulta) throws Exception;
    public ArrayList<USUARIO_TOKEN_ACCESO> obtenerUsuariosTokenDeAcceso() throws Exception;
    public DeclaracionesPost obtenerValoresPorDefaultFiltroDeclaraciones (DeclaracionesPost estructuraPost) throws Exception;    
    public Declaraciones obtenerDeclaracionesPost(DeclaracionesPost estructuraPost) throws Exception;
    public String generarJsonDeclaracionesPost(Declaraciones estructuraDeclaraciones, boolean bolMostrarDatosPublicosYPrivados) throws Exception;
    public String crearExpresionRegularDePalabra(String palabra);
    public boolean esValidoRFC(String rfc) throws Exception;    
    
}