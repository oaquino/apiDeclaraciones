/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.cvlg.oauth2;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.UserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import tol.sesaemm.ing.jidv.integracion.DAOWs;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;

/**
 *
 * @author I. en C. Cristian Luna <cristian.luna@sesaemm.org.mx>
 * Colaboracion: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter
{ 
    @Autowired
    @Qualifier("bcryptPasswordEncoder")
    private BCryptPasswordEncoder bcryptPasswordEncoder;

    /**
     * @return AuthenticationManager
     * @throws Exception
     */
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception
    {
        return super.authenticationManagerBean();
    }

    /**
     * @param auth
     * @throws Exception
     */
    @Autowired
    public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception
    { 
        ArrayList<USUARIO_TOKEN_ACCESO> arregloUsuariosTokenAcceso;     
        UserDetailsManagerConfigurer.UserDetailsBuilder usuarios;       
        int numeroRegistros;                                            
        
        arregloUsuariosTokenAcceso = DAOWs.obtenerUsuariosTokenDeAcceso();
        usuarios = null;
        numeroRegistros = 1;
        
        for (USUARIO_TOKEN_ACCESO usuario : arregloUsuariosTokenAcceso)
        {   
            if (numeroRegistros == 1)
            { 
                usuarios = auth.inMemoryAuthentication().withUser(usuario.getUsuario()).password(bcryptPasswordEncoder.encode(usuario.getContrasenia())).roles(usuario.getRol());
            } 
            else
            { 
                usuarios.and().withUser(usuario.getUsuario()).password(bcryptPasswordEncoder.encode(usuario.getContrasenia())).roles(usuario.getRol());
            } 

            numeroRegistros++;
        } 
    } 

    /**
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http
            .csrf().disable()
            .anonymous().disable()          
            .authorizeRequests()
            .antMatchers("/oauth/token").permitAll()    
            .anyRequest().authenticated()               
        ;
    }
} 