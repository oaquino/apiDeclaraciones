/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.cvlg.oauth2;

import com.google.gson.Gson;
import org.bson.Document;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;

/**
 *
 * @author I. en C. Cristian Luna <cristian.luna@sesaemm.org.mx>
 * Colaboracion: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ExceptionTranslatorWebResponse extends DefaultWebResponseExceptionTranslator
{ 
    /**
     * @param e
     * @return ResponseEntity
     * @throws Exception
     */
    @Override
    public ResponseEntity translate(Exception e) throws Exception
    {
        ResponseEntity<OAuth2Exception> responseEntity;         
        OAuth2Exception oauth2Exception;                        
        HttpStatus codigoEstadoHTTP;                            
        Document documentoMensajeError;                         
        
        responseEntity = super.translate(e);
        oauth2Exception = responseEntity.getBody();
        codigoEstadoHTTP = responseEntity.getStatusCode();
        documentoMensajeError = new Document();        

        documentoMensajeError.append("code", (oauth2Exception == null) ? "error" : String.valueOf(oauth2Exception.getHttpErrorCode()));
        documentoMensajeError.append("message", (oauth2Exception == null) ? "Petición inválida " : oauth2Exception.getMessage());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        return new ResponseEntity(new Gson().toJson(documentoMensajeError), headers, codigoEstadoHTTP);
    }
} 
