package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesDatosGenerales;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoDatosGenerales
{

    private DeclaracionesDatosGenerales datosGenerales;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoDeDatosGenerales;

    public DeclaracionesDocumentoDatosGenerales(DeclaracionesDatosGenerales datosGenerales, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosGenerales = datosGenerales;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoDeDatosGenerales()
    {

        documentoDeDatosGenerales = new Document();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {

            documentoDeDatosGenerales.append("nombre", datosGenerales.getNombre());

            documentoDeDatosGenerales.append("primerApellido", datosGenerales.getPrimerApellido());

            if (datosGenerales.getSegundoApellido() != null)
            {
                documentoDeDatosGenerales.append("segundoApellido", datosGenerales.getSegundoApellido());
            }

            if (datosGenerales.getCorreoElectronico() != null)
            {
                if (datosGenerales.getCorreoElectronico().getInstitucional() != null)
                {
                    documentoDeDatosGenerales.append("correoElectronico", new Document("institucional", datosGenerales.getCorreoElectronico().getInstitucional()));
                }
            }

        } else
        {

            documentoDeDatosGenerales.append("nombre", datosGenerales.getNombre());

            documentoDeDatosGenerales.append("primerApellido", datosGenerales.getPrimerApellido());

            if (datosGenerales.getSegundoApellido() != null)
            {
                documentoDeDatosGenerales.append("segundoApellido", datosGenerales.getSegundoApellido());
            }

            if (datosGenerales.getCurp() != null)
            {
                documentoDeDatosGenerales.append("curp", datosGenerales.getCurp());
            }

            if (datosGenerales.getRfc() != null)
            {
                Document documentoRfc = new Document();
                if (datosGenerales.getRfc().getRfc() != null)
                {
                    documentoRfc.append("rfc", datosGenerales.getRfc().getRfc());
                }
                if (datosGenerales.getRfc().getHomoClave() != null)
                {
                    documentoRfc.append("homoClave", datosGenerales.getRfc().getHomoClave());
                }
                if (documentoRfc.isEmpty() == false)
                {
                    documentoDeDatosGenerales.append("rfc", documentoRfc);
                }
            }

            if (datosGenerales.getCorreoElectronico() != null)
            {
                Document documentoCorreoElectronico = new Document();
                if (datosGenerales.getCorreoElectronico().getInstitucional() != null)
                {
                    documentoCorreoElectronico.append("institucional", datosGenerales.getCorreoElectronico().getInstitucional());
                }
                if (datosGenerales.getCorreoElectronico().getPersonal() != null)
                {
                    documentoCorreoElectronico.append("personal", datosGenerales.getCorreoElectronico().getPersonal());
                }
                if (documentoCorreoElectronico.isEmpty() == false)
                {
                    documentoDeDatosGenerales.append("correoElectronico", documentoCorreoElectronico);
                }
            }

            if (datosGenerales.getTelefono() != null)
            {
                Document documentoTelefono = new Document();
                if (datosGenerales.getTelefono().getCasa() != null)
                {
                    documentoTelefono.append("casa", datosGenerales.getTelefono().getCasa());
                }
                if (datosGenerales.getTelefono().getCelularPersonal() != null)
                {
                    documentoTelefono.append("celularPersonal", datosGenerales.getTelefono().getCelularPersonal());
                }
                if (documentoTelefono.isEmpty() == false)
                {
                    documentoDeDatosGenerales.append("telefono", documentoTelefono);
                }
            }

            if (datosGenerales.getSituacionPersonalEstadoCivil() != null)
            {
                Document documentoSituacionPersonalEstadoCivil = new Document();
                if (datosGenerales.getSituacionPersonalEstadoCivil().getClave() != null)
                {
                    documentoSituacionPersonalEstadoCivil.append("clave", datosGenerales.getSituacionPersonalEstadoCivil().getClave());
                }
                if (datosGenerales.getSituacionPersonalEstadoCivil().getValor() != null)
                {
                    documentoSituacionPersonalEstadoCivil.append("valor", datosGenerales.getSituacionPersonalEstadoCivil().getValor());
                }
                if (documentoSituacionPersonalEstadoCivil.isEmpty() == false)
                {
                    documentoDeDatosGenerales.append("situacionPersonalEstadoCivil", documentoSituacionPersonalEstadoCivil);
                }
            }

            if (datosGenerales.getRegimenMatrimonial() != null)
            {
                Document documentoRegimenMatrimonial = new Document();
                if (datosGenerales.getRegimenMatrimonial().getClave() != null)
                {
                    documentoRegimenMatrimonial.append("clave", datosGenerales.getRegimenMatrimonial().getClave());
                }
                if (datosGenerales.getRegimenMatrimonial().getValor() != null)
                {
                    documentoRegimenMatrimonial.append("valor", datosGenerales.getRegimenMatrimonial().getValor());
                }
                if (documentoRegimenMatrimonial.isEmpty() == false)
                {
                    documentoDeDatosGenerales.append("regimenMatrimonial", documentoRegimenMatrimonial);
                }
            }

            if (datosGenerales.getPaisNacimiento() != null)
            {
                documentoDeDatosGenerales.append("paisNacimiento", datosGenerales.getPaisNacimiento());
            }

            if (datosGenerales.getNacionalidad() != null)
            {
                documentoDeDatosGenerales.append("nacionalidad", datosGenerales.getNacionalidad());
            }

            if (datosGenerales.getAclaracionesObservaciones() != null)
            {
                documentoDeDatosGenerales.append("aclaracionesObservaciones", datosGenerales.getAclaracionesObservaciones());
            }

        }

        return documentoDeDatosGenerales;

    }

}
