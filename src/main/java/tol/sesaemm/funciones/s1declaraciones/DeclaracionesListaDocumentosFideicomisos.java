package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesFideicomiso;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosFideicomisos
{

    private ArrayList<DeclaracionesFideicomiso> fideicomiso;
    private boolean bolMostrarDatosPublicosYPrivados;
    private ArrayList<Document> arregloListaFideicomisos;

    public DeclaracionesListaDocumentosFideicomisos(ArrayList<DeclaracionesFideicomiso> fideicomiso, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.fideicomiso = fideicomiso;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public ArrayList<Document> obtenerArregloDocumentosFideicomisos()
    {

        arregloListaFideicomisos = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            for (int j = 0; j < fideicomiso.size(); j++)
            {
                Document documentoFideicomisos = new Document();
                if (fideicomiso.get(j).getTipoRelacion() != null)
                {
                    if (fideicomiso.get(j).getTipoRelacion().equals("DECLARANTE"))
                    {

                        if (fideicomiso.get(j).getTipoOperacion() != null)
                        {
                            documentoFideicomisos.append("tipoOperacion", fideicomiso.get(j).getTipoOperacion());
                        }

                        documentoFideicomisos.append("tipoRelacion", fideicomiso.get(j).getTipoRelacion());

                        if (fideicomiso.get(j).getTipoFideicomiso() != null)
                        {
                            documentoFideicomisos.append("tipoFideicomiso", fideicomiso.get(j).getTipoFideicomiso());
                        }

                        if (fideicomiso.get(j).getTipoParticipacion() != null)
                        {
                            documentoFideicomisos.append("tipoParticipacion", fideicomiso.get(j).getTipoParticipacion());
                        }

                        if (fideicomiso.get(j).getRfcFideicomiso() != null)
                        {
                            documentoFideicomisos.append("rfcFideicomiso", fideicomiso.get(j).getRfcFideicomiso());
                        }

                        if (fideicomiso.get(j).getFideicomitente() != null)
                        {
                            Document documentoFideicomitente = new Document();
                            if (fideicomiso.get(j).getFideicomitente().getTipoPersona() != null)
                            {
                                if (fideicomiso.get(j).getFideicomitente().getTipoPersona().equals("MORAL"))
                                {

                                    documentoFideicomitente.append("tipoPersona", fideicomiso.get(j).getFideicomitente().getTipoPersona());

                                    if (fideicomiso.get(j).getFideicomitente().getNombreRazonSocial() != null)
                                    {
                                        documentoFideicomitente.append("nombreRazonSocial", fideicomiso.get(j).getFideicomitente().getNombreRazonSocial());
                                    }

                                    if (fideicomiso.get(j).getFideicomitente().getRfc() != null)
                                    {
                                        documentoFideicomitente.append("rfc", fideicomiso.get(j).getFideicomitente().getRfc());
                                    }

                                }
                            }
                            if (documentoFideicomitente.isEmpty() == false)
                            {
                                documentoFideicomisos.append("fideicomitente", documentoFideicomitente);
                            }
                        }

                        if (fideicomiso.get(j).getFiduciario() != null)
                        {
                            Document documentoFiduciario = new Document();

                            if (fideicomiso.get(j).getFiduciario().getNombreRazonSocial() != null)
                            {
                                documentoFiduciario.append("nombreRazonSocial", fideicomiso.get(j).getFiduciario().getNombreRazonSocial());
                            }

                            if (fideicomiso.get(j).getFiduciario().getRfc() != null)
                            {
                                documentoFiduciario.append("rfc", fideicomiso.get(j).getFiduciario().getRfc());
                            }

                            if (documentoFiduciario.isEmpty() == false)
                            {
                                documentoFideicomisos.append("fiduciario", documentoFiduciario);
                            }
                        }

                        if (fideicomiso.get(j).getFideicomisario() != null)
                        {
                            Document documentoFideicomisario = new Document();
                            if (fideicomiso.get(j).getFideicomisario().getTipoPersona() != null)
                            {
                                if (fideicomiso.get(j).getFideicomisario().getTipoPersona().equals("MORAL"))
                                {

                                    documentoFideicomisario.append("tipoPersona", fideicomiso.get(j).getFideicomisario().getTipoPersona());

                                    if (fideicomiso.get(j).getFideicomisario().getNombreRazonSocial() != null)
                                    {
                                        documentoFideicomisario.append("nombreRazonSocial", fideicomiso.get(j).getFideicomisario().getNombreRazonSocial());
                                    }

                                    if (fideicomiso.get(j).getFideicomisario().getRfc() != null)
                                    {
                                        documentoFideicomisario.append("rfc", fideicomiso.get(j).getFideicomisario().getRfc());
                                    }

                                }
                            }
                            if (documentoFideicomisario.isEmpty() == false)
                            {
                                documentoFideicomisos.append("fideicomisario", documentoFideicomisario);
                            }
                        }

                        if (fideicomiso.get(j).getSector() != null)
                        {
                            Document documentoSector = new Document();

                            if (fideicomiso.get(j).getSector().getClave() != null)
                            {
                                documentoSector.append("clave", fideicomiso.get(j).getSector().getClave());
                            }

                            if (fideicomiso.get(j).getSector().getValor() != null)
                            {
                                documentoSector.append("valor", fideicomiso.get(j).getSector().getValor());
                            }

                            if (documentoSector.isEmpty() == false)
                            {
                                documentoFideicomisos.append("sector", documentoSector);
                            }
                        }

                        if (fideicomiso.get(j).getExtranjero() != null)
                        {
                            documentoFideicomisos.append("extranjero", fideicomiso.get(j).getExtranjero());
                        }

                    }
                }
                if (documentoFideicomisos.isEmpty() == false)
                {
                    arregloListaFideicomisos.add(documentoFideicomisos);
                }
            }
        } else
        {

            for (int j = 0; j < fideicomiso.size(); j++)
            {
                Document documentoFideicomisos = new Document();

                if (fideicomiso.get(j).getTipoOperacion() != null)
                {
                    documentoFideicomisos.append("tipoOperacion", fideicomiso.get(j).getTipoOperacion());
                }

                if (fideicomiso.get(j).getTipoRelacion() != null)
                {
                    documentoFideicomisos.append("tipoRelacion", fideicomiso.get(j).getTipoRelacion());
                }

                if (fideicomiso.get(j).getTipoFideicomiso() != null)
                {
                    documentoFideicomisos.append("tipoFideicomiso", fideicomiso.get(j).getTipoFideicomiso());
                }

                if (fideicomiso.get(j).getTipoParticipacion() != null)
                {
                    documentoFideicomisos.append("tipoParticipacion", fideicomiso.get(j).getTipoParticipacion());
                }

                if (fideicomiso.get(j).getRfcFideicomiso() != null)
                {
                    documentoFideicomisos.append("rfcFideicomiso", fideicomiso.get(j).getRfcFideicomiso());
                }

                if (fideicomiso.get(j).getFideicomitente() != null)
                {
                    Document documentoFideicomitente = new Document();

                    if (fideicomiso.get(j).getFideicomitente().getTipoPersona() != null)
                    {
                        documentoFideicomitente.append("tipoPersona", fideicomiso.get(j).getFideicomitente().getTipoPersona());
                    }

                    if (fideicomiso.get(j).getFideicomitente().getNombreRazonSocial() != null)
                    {
                        documentoFideicomitente.append("nombreRazonSocial", fideicomiso.get(j).getFideicomitente().getNombreRazonSocial());
                    }

                    if (fideicomiso.get(j).getFideicomitente().getRfc() != null)
                    {
                        documentoFideicomitente.append("rfc", fideicomiso.get(j).getFideicomitente().getRfc());
                    }

                    if (documentoFideicomitente.isEmpty() == false)
                    {
                        documentoFideicomisos.append("fideicomitente", documentoFideicomitente);
                    }
                }

                if (fideicomiso.get(j).getFiduciario() != null)
                {
                    Document documentoFiduciario = new Document();

                    if (fideicomiso.get(j).getFiduciario().getNombreRazonSocial() != null)
                    {
                        documentoFiduciario.append("nombreRazonSocial", fideicomiso.get(j).getFiduciario().getNombreRazonSocial());
                    }

                    if (fideicomiso.get(j).getFiduciario().getRfc() != null)
                    {
                        documentoFiduciario.append("rfc", fideicomiso.get(j).getFiduciario().getRfc());
                    }

                    if (documentoFiduciario.isEmpty() == false)
                    {
                        documentoFideicomisos.append("fiduciario", documentoFiduciario);
                    }
                }

                if (fideicomiso.get(j).getFideicomisario() != null)
                {
                    Document documentoFideicomisario = new Document();

                    if (fideicomiso.get(j).getFideicomisario().getTipoPersona() != null)
                    {
                        documentoFideicomisario.append("tipoPersona", fideicomiso.get(j).getFideicomisario().getTipoPersona());
                    }

                    if (fideicomiso.get(j).getFideicomisario().getNombreRazonSocial() != null)
                    {
                        documentoFideicomisario.append("nombreRazonSocial", fideicomiso.get(j).getFideicomisario().getNombreRazonSocial());
                    }

                    if (fideicomiso.get(j).getFideicomisario().getRfc() != null)
                    {
                        documentoFideicomisario.append("rfc", fideicomiso.get(j).getFideicomisario().getRfc());
                    }

                    if (documentoFideicomisario.isEmpty() == false)
                    {
                        documentoFideicomisos.append("fideicomisario", documentoFideicomisario);
                    }
                }

                if (fideicomiso.get(j).getSector() != null)
                {
                    Document documentoSector = new Document();

                    if (fideicomiso.get(j).getSector().getClave() != null)
                    {
                        documentoSector.append("clave", fideicomiso.get(j).getSector().getClave());
                    }

                    if (fideicomiso.get(j).getSector().getValor() != null)
                    {
                        documentoSector.append("valor", fideicomiso.get(j).getSector().getValor());
                    }

                    if (documentoSector.isEmpty() == false)
                    {
                        documentoFideicomisos.append("sector", documentoSector);
                    }
                }

                if (fideicomiso.get(j).getExtranjero() != null)
                {
                    documentoFideicomisos.append("extranjero", fideicomiso.get(j).getExtranjero());
                }

                if (documentoFideicomisos.isEmpty() == false)
                {
                    arregloListaFideicomisos.add(documentoFideicomisos);
                }
            }

        }

        return arregloListaFideicomisos;

    }

}
