package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesServiciosProfesionales;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoServiciosProfesionales
{

    private DeclaracionesServiciosProfesionales serviciosProfesionales;
    private Document documentoServiciosProfesionales;

    public DeclaracionesDocumentoServiciosProfesionales(DeclaracionesServiciosProfesionales serviciosProfesionales)
    {
        this.serviciosProfesionales = serviciosProfesionales;
    }

    public Document obtenerDocumentoServiciosProfesionales()
    {

        documentoServiciosProfesionales = new Document();
        List<Document> arregloListaServicios;

        if (serviciosProfesionales.getRemuneracionTotal() != null)
        {
            Document documentoRemuneracionTotal = new DeclaracionesDocumentoMonto(serviciosProfesionales.getRemuneracionTotal()).obtenerDocumentoMonto();
            if (documentoRemuneracionTotal.isEmpty() == false)
            {
                documentoServiciosProfesionales.append("remuneracionTotal", documentoRemuneracionTotal);
            }
        }

        if (serviciosProfesionales.getServicios() != null)
        {
            arregloListaServicios = new ArrayList<>();
            for (int i = 0; i < serviciosProfesionales.getServicios().size(); i++)
            {
                Document documentoServicio = new Document();

                if (serviciosProfesionales.getServicios().get(i).getRemuneracion() != null)
                {
                    Document documentoRemuneracion = new DeclaracionesDocumentoMonto(serviciosProfesionales.getServicios().get(i).getRemuneracion()).obtenerDocumentoMonto();
                    if (documentoRemuneracion.isEmpty() == false)
                    {
                        documentoServicio.append("remuneracion", documentoRemuneracion);
                    }
                }

                if (serviciosProfesionales.getServicios().get(i).getTipoServicio() != null)
                {
                    documentoServicio.append("tipoServicio", serviciosProfesionales.getServicios().get(i).getTipoServicio());
                }

                if (documentoServicio.isEmpty() == false)
                {
                    arregloListaServicios.add(documentoServicio);
                }
            }
            documentoServiciosProfesionales.append("servicios", arregloListaServicios);
        }

        return documentoServiciosProfesionales;

    }

}
