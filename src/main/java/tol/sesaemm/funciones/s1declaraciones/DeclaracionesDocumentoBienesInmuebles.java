package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesBienesInmuebles;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoBienesInmuebles
{

    private DeclaracionesBienesInmuebles datosBienesInmuebles;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoBienesInmuebles;

    public DeclaracionesDocumentoBienesInmuebles(DeclaracionesBienesInmuebles datosBienesInmuebles, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosBienesInmuebles = datosBienesInmuebles;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoBienesInmuebles()
    {

        documentoBienesInmuebles = new Document();

        if (datosBienesInmuebles != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (datosBienesInmuebles.getNinguno() != null)
                {
                    documentoBienesInmuebles.append("ninguno", datosBienesInmuebles.getNinguno());
                }

                if (datosBienesInmuebles.getBienInmueble() != null)
                {
                    ArrayList<Document> arregloDocumentosBienesInmuebles = new DeclaracionesListaDocumentosBienesInmuebles(datosBienesInmuebles.getBienInmueble(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaBienesInmuebles();
                    if (arregloDocumentosBienesInmuebles.isEmpty() == false)
                    {
                        documentoBienesInmuebles.append("bienInmueble", arregloDocumentosBienesInmuebles);
                    }
                }

            } else
            {

                if (datosBienesInmuebles.getNinguno() != null)
                {
                    documentoBienesInmuebles.append("ninguno", datosBienesInmuebles.getNinguno());
                }

                if (datosBienesInmuebles.getBienInmueble() != null)
                {
                    ArrayList<Document> arregloDocumentosBienesInmuebles = new DeclaracionesListaDocumentosBienesInmuebles(datosBienesInmuebles.getBienInmueble(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaBienesInmuebles();
                    if (arregloDocumentosBienesInmuebles.isEmpty() == false)
                    {
                        documentoBienesInmuebles.append("bienInmueble", arregloDocumentosBienesInmuebles);
                    }
                }

                if (datosBienesInmuebles.getAclaracionesObservaciones() != null)
                {
                    documentoBienesInmuebles.append("aclaracionesObservaciones", datosBienesInmuebles.getAclaracionesObservaciones());
                }

            }
        }

        return documentoBienesInmuebles;

    }

}
