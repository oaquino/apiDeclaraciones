package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesActividadIndustialComercialEmpresarial;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoActividadIndustialComercialEmpresarial
{

    private DeclaracionesActividadIndustialComercialEmpresarial actividadIndustrialComercialEmpresarial;
    private Document documentoActividadIndustialComercialEmpresarial;

    public DeclaracionesDocumentoActividadIndustialComercialEmpresarial(DeclaracionesActividadIndustialComercialEmpresarial actividadIndustrialComercialEmpresarial)
    {
        this.actividadIndustrialComercialEmpresarial = actividadIndustrialComercialEmpresarial;
    }

    public Document obtenerDocumentoActividadIndustialComercialEmpresarial()
    {

        documentoActividadIndustialComercialEmpresarial = new Document();
        List<Document> arregloListaActividades;

        if (actividadIndustrialComercialEmpresarial.getRemuneracionTotal() != null)
        {
            Document documentoRemuneracionTotal = new DeclaracionesDocumentoMonto(actividadIndustrialComercialEmpresarial.getRemuneracionTotal()).obtenerDocumentoMonto();
            if (documentoRemuneracionTotal.isEmpty() == false)
            {
                documentoActividadIndustialComercialEmpresarial.append("remuneracionTotal", documentoRemuneracionTotal);
            }
        }

        if (actividadIndustrialComercialEmpresarial.getActividades() != null)
        {
            arregloListaActividades = new ArrayList<>();
            for (int i = 0; i < actividadIndustrialComercialEmpresarial.getActividades().size(); i++)
            {
                Document documentoActividad = new Document();

                if (actividadIndustrialComercialEmpresarial.getActividades().get(i).getRemuneracion() != null)
                {
                    Document documentoRemuneracion = new DeclaracionesDocumentoMonto(actividadIndustrialComercialEmpresarial.getActividades().get(i).getRemuneracion()).obtenerDocumentoMonto();
                    if (documentoRemuneracion.isEmpty() == false)
                    {
                        documentoActividad.append("remuneracion", documentoRemuneracion);
                    }
                }

                if (actividadIndustrialComercialEmpresarial.getActividades().get(i).getNombreRazonSocial() != null)
                {
                    documentoActividad.append("nombreRazonSocial", actividadIndustrialComercialEmpresarial.getActividades().get(i).getNombreRazonSocial());
                }

                if (actividadIndustrialComercialEmpresarial.getActividades().get(i).getTipoNegocio() != null)
                {
                    documentoActividad.append("tipoNegocio", actividadIndustrialComercialEmpresarial.getActividades().get(i).getTipoNegocio());
                }

                if (documentoActividad.isEmpty() == false)
                {
                    arregloListaActividades.add(documentoActividad);
                }
            }
            documentoActividadIndustialComercialEmpresarial.append("actividades", arregloListaActividades);
        }

        return documentoActividadIndustialComercialEmpresarial;

    }

}
