/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesTipoInstrumento;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoTipoInstrumento
{

    private DeclaracionesTipoInstrumento tipoInstrumento;
    private Document documentoTipoInstrumento;

    public DeclaracionesDocumentoTipoInstrumento(DeclaracionesTipoInstrumento tipoInstrumento)
    {
        this.tipoInstrumento = tipoInstrumento;
    }

    public Document obtenerDocumentoTipoInstrumento()
    {

        documentoTipoInstrumento = new Document();

        if (tipoInstrumento.getClave() != null)
        {
            documentoTipoInstrumento.append("clave", tipoInstrumento.getClave());
        }

        if (tipoInstrumento.getValor() != null)
        {
            documentoTipoInstrumento.append("valor", tipoInstrumento.getValor());
        }

        return documentoTipoInstrumento;
    }

}
