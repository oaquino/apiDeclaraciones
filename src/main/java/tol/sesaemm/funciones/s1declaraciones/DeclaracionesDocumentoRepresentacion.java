package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesRepresentaciones;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoRepresentacion
{

    private DeclaracionesRepresentaciones datosRepresentacion;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoRepresentacion;

    public DeclaracionesDocumentoRepresentacion(DeclaracionesRepresentaciones datosRepresentacion, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosRepresentacion = datosRepresentacion;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoRepresentacion()
    {

        documentoRepresentacion = new Document();

        if (datosRepresentacion != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (datosRepresentacion.getNinguno() != null)
                {
                    documentoRepresentacion.append("ninguno", datosRepresentacion.getNinguno());
                }

                if (datosRepresentacion.getRepresentacion() != null)
                {
                    ArrayList<Document> listaRepresentacion = new DeclaracionesListaDocumentosRepresentacion(datosRepresentacion.getRepresentacion(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaRepresentacion();
                    if (listaRepresentacion.isEmpty() == false)
                    {
                        documentoRepresentacion.append("representacion", listaRepresentacion);
                    }
                }

            } else
            {

                if (datosRepresentacion.getNinguno() != null)
                {
                    documentoRepresentacion.append("ninguno", datosRepresentacion.getNinguno());
                }

                if (datosRepresentacion.getRepresentacion() != null)
                {
                    ArrayList<Document> listaRepresentacion = new DeclaracionesListaDocumentosRepresentacion(datosRepresentacion.getRepresentacion(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaRepresentacion();
                    if (listaRepresentacion.isEmpty() == false)
                    {
                        documentoRepresentacion.append("representacion", listaRepresentacion);
                    }
                }

                if (datosRepresentacion.getAclaracionesObservaciones() != null)
                {
                    documentoRepresentacion.append("aclaracionesObservaciones", datosRepresentacion.getAclaracionesObservaciones());
                }

            }
        }

        return documentoRepresentacion;

    }

}
