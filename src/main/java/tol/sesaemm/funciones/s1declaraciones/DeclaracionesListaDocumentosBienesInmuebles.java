package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesBienInmueble;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosBienesInmuebles
{

    private ArrayList<DeclaracionesBienInmueble> bienInmueble;
    private boolean bolMostrarDatosPublicosYPrivados;
    private ArrayList<Document> arregloListaBienesInmuebles;

    public DeclaracionesListaDocumentosBienesInmuebles(ArrayList<DeclaracionesBienInmueble> bienInmueble, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.bienInmueble = bienInmueble;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public ArrayList<Document> obtenerArregloDocumentosListaBienesInmuebles()
    {

        List<Document> arregloListaTitularBien;
        List<Document> arregloListaTercero;
        List<Document> arregloListaTransmisor;

        arregloListaBienesInmuebles = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            for (int j = 0; j < bienInmueble.size(); j++)
            {

                Document documentoBienInmueble = new Document();

                if (bienInmueble.get(j).getTitular() != null)
                {
                    if (bienInmueble.get(j).getTitular().size() == 1)
                    {
                        if (bienInmueble.get(j).getTitular().get(0).getClave() != null)
                        {
                            if (bienInmueble.get(j).getTitular().get(0).getClave().equals("DEC") == true)
                            {

                                if (bienInmueble.get(j).getTipoOperacion() != null)
                                {
                                    documentoBienInmueble.append("tipoOperacion", bienInmueble.get(j).getTipoOperacion());
                                }

                                if (bienInmueble.get(j).getTipoInmueble() != null)
                                {
                                    Document documentoTipoInmueble = new Document();

                                    if (bienInmueble.get(j).getTipoInmueble().getClave() != null)
                                    {
                                        documentoTipoInmueble.append("clave", bienInmueble.get(j).getTipoInmueble().getClave());
                                    }

                                    if (bienInmueble.get(j).getTipoInmueble().getValor() != null)
                                    {
                                        documentoTipoInmueble.append("valor", bienInmueble.get(j).getTipoInmueble().getValor());
                                    }

                                    if (documentoTipoInmueble.isEmpty() == false)
                                    {
                                        documentoBienInmueble.append("tipoInmueble", documentoTipoInmueble);
                                    }
                                }

                                arregloListaTitularBien = new ArrayList<>();
                                Document documentoTitularBien = new Document();

                                documentoTitularBien.append("clave", bienInmueble.get(j).getTitular().get(0).getClave());

                                if (bienInmueble.get(j).getTitular().get(0).getValor() != null)
                                {
                                    documentoTitularBien.append("valor", bienInmueble.get(j).getTitular().get(0).getValor());
                                }

                                if (documentoTitularBien.isEmpty() == false)
                                {
                                    arregloListaTitularBien.add(documentoTitularBien);
                                    documentoBienInmueble.append("titular", arregloListaTitularBien);
                                }

                                if (bienInmueble.get(j).getPorcentajePropiedad() != null)
                                {
                                    documentoBienInmueble.append("porcentajePropiedad", bienInmueble.get(j).getPorcentajePropiedad());
                                }

                                if (bienInmueble.get(j).getSuperficieTerreno() != null)
                                {
                                    Document documentoSuperficieTerreno = new Document();

                                    if (bienInmueble.get(j).getSuperficieTerreno().getValor() != null)
                                    {
                                        documentoSuperficieTerreno.append("valor", bienInmueble.get(j).getSuperficieTerreno().getValor());
                                    }

                                    if (bienInmueble.get(j).getSuperficieTerreno().getUnidad() != null)
                                    {
                                        documentoSuperficieTerreno.append("unidad", bienInmueble.get(j).getSuperficieTerreno().getUnidad());
                                    }

                                    if (documentoSuperficieTerreno.isEmpty() == false)
                                    {
                                        documentoBienInmueble.append("superficieTerreno", documentoSuperficieTerreno);
                                    }
                                }

                                if (bienInmueble.get(j).getSuperficieConstruccion() != null)
                                {
                                    Document documentoSuperficieConstruccion = new Document();

                                    if (bienInmueble.get(j).getSuperficieConstruccion().getValor() != null)
                                    {
                                        documentoSuperficieConstruccion.append("valor", bienInmueble.get(j).getSuperficieConstruccion().getValor());
                                    }

                                    if (bienInmueble.get(j).getSuperficieConstruccion().getUnidad() != null)
                                    {
                                        documentoSuperficieConstruccion.append("unidad", bienInmueble.get(j).getSuperficieConstruccion().getUnidad());
                                    }

                                    if (documentoSuperficieConstruccion.isEmpty() == false)
                                    {
                                        documentoBienInmueble.append("superficieConstruccion", documentoSuperficieConstruccion);
                                    }
                                }

                                if (bienInmueble.get(j).getTercero() != null)
                                {
                                    arregloListaTercero = new ArrayList<>();
                                    for (int l = 0; l < bienInmueble.get(j).getTercero().size(); l++)
                                    {
                                        Document documentoTercero = new Document();
                                        if (bienInmueble.get(j).getTercero().get(l).getTipoPersona() != null)
                                        {
                                            if (bienInmueble.get(j).getTercero().get(l).getTipoPersona().equals("MORAL"))
                                            {

                                                documentoTercero.append("tipoPersona", bienInmueble.get(j).getTercero().get(l).getTipoPersona());

                                                if (bienInmueble.get(j).getTercero().get(l).getNombreRazonSocial() != null)
                                                {
                                                    documentoTercero.append("nombreRazonSocial", bienInmueble.get(j).getTercero().get(l).getNombreRazonSocial());
                                                }

                                                if (bienInmueble.get(j).getTercero().get(l).getRfc() != null)
                                                {
                                                    documentoTercero.append("rfc", bienInmueble.get(j).getTercero().get(l).getRfc());
                                                }

                                            }
                                        }
                                        if (documentoTercero.isEmpty() == false)
                                        {
                                            arregloListaTercero.add(documentoTercero);
                                        }
                                    }
                                    if (arregloListaTercero.isEmpty() == false)
                                    {
                                        documentoBienInmueble.append("tercero", arregloListaTercero);
                                    }
                                }

                                if (bienInmueble.get(j).getTransmisor() != null)
                                {
                                    arregloListaTransmisor = new ArrayList<>();
                                    for (int m = 0; m < bienInmueble.get(j).getTransmisor().size(); m++)
                                    {
                                        Document documentoTransmisor = new Document();
                                        if (bienInmueble.get(j).getTransmisor().get(m).getTipoPersona() != null)
                                        {
                                            if (bienInmueble.get(j).getTransmisor().get(m).getTipoPersona().equals("MORAL"))
                                            {

                                                documentoTransmisor.append("tipoPersona", bienInmueble.get(j).getTransmisor().get(m).getTipoPersona());

                                                if (bienInmueble.get(j).getTransmisor().get(m).getNombreRazonSocial() != null)
                                                {
                                                    documentoTransmisor.append("nombreRazonSocial", bienInmueble.get(j).getTransmisor().get(m).getNombreRazonSocial());
                                                }

                                                if (bienInmueble.get(j).getTransmisor().get(m).getRfc() != null)
                                                {
                                                    documentoTransmisor.append("rfc", bienInmueble.get(j).getTransmisor().get(m).getRfc());
                                                }

                                                if (bienInmueble.get(j).getTransmisor().get(m).getRelacion() != null)
                                                {
                                                    Document documentoRelacion = new Document();
                                                    if (bienInmueble.get(j).getTransmisor().get(m).getRelacion().getClave() != null)
                                                    {
                                                        if (bienInmueble.get(j).getTransmisor().get(m).getRelacion().getClave().equals("NIN") || bienInmueble.get(j).getTransmisor().get(m).getRelacion().getClave().equals("OTRO"))
                                                        {

                                                            documentoRelacion.append("clave", bienInmueble.get(j).getTransmisor().get(m).getRelacion().getClave());

                                                            if (bienInmueble.get(j).getTransmisor().get(m).getRelacion().getValor() != null)
                                                            {
                                                                documentoRelacion.append("valor", bienInmueble.get(j).getTransmisor().get(m).getRelacion().getValor());
                                                            }

                                                        }
                                                    }
                                                    if (documentoRelacion.isEmpty() == false)
                                                    {
                                                        documentoTransmisor.append("relacion", documentoRelacion);
                                                    }
                                                }

                                            }
                                        }
                                        if (documentoTransmisor.isEmpty() == false)
                                        {
                                            arregloListaTransmisor.add(documentoTransmisor);
                                        }
                                    }
                                    if (arregloListaTransmisor.isEmpty() == false)
                                    {
                                        documentoBienInmueble.append("transmisor", arregloListaTransmisor);
                                    }
                                }

                                if (bienInmueble.get(j).getFormaAdquisicion() != null)
                                {
                                    Document documentoFormaAdquisicion = new Document();

                                    if (bienInmueble.get(j).getFormaAdquisicion().getClave() != null)
                                    {
                                        documentoFormaAdquisicion.append("clave", bienInmueble.get(j).getFormaAdquisicion().getClave());
                                    }

                                    if (bienInmueble.get(j).getFormaAdquisicion().getValor() != null)
                                    {
                                        documentoFormaAdquisicion.append("valor", bienInmueble.get(j).getFormaAdquisicion().getValor());
                                    }

                                    if (documentoFormaAdquisicion.isEmpty() == false)
                                    {
                                        documentoBienInmueble.append("formaAdquisicion", documentoFormaAdquisicion);
                                    }
                                }

                                if (bienInmueble.get(j).getFormaPago() != null)
                                {
                                    documentoBienInmueble.append("formaPago", bienInmueble.get(j).getFormaPago());
                                }

                                if (bienInmueble.get(j).getValorAdquisicion() != null)
                                {
                                    Document documentoValorAdquisicion = new Document();

                                    if (bienInmueble.get(j).getValorAdquisicion().getValor() != null)
                                    {
                                        documentoValorAdquisicion.append("valor", bienInmueble.get(j).getValorAdquisicion().getValor());
                                    }

                                    if (bienInmueble.get(j).getValorAdquisicion().getMoneda() != null)
                                    {
                                        documentoValorAdquisicion.append("moneda", bienInmueble.get(j).getValorAdquisicion().getMoneda());
                                    }

                                    if (documentoValorAdquisicion.isEmpty() == false)
                                    {
                                        documentoBienInmueble.append("valorAdquisicion", documentoValorAdquisicion);
                                    }
                                }

                                if (bienInmueble.get(j).getFechaAdquisicion() != null)
                                {
                                    documentoBienInmueble.append("fechaAdquisicion", bienInmueble.get(j).getFechaAdquisicion());
                                }

                                if (bienInmueble.get(j).getValorConformeA() != null)
                                {
                                    documentoBienInmueble.append("valorConformeA", bienInmueble.get(j).getValorConformeA());
                                }

                                if (bienInmueble.get(j).getMotivoBaja() != null)
                                {
                                    Document documentoMotivoBaja = new Document();

                                    if (bienInmueble.get(j).getMotivoBaja().getClave() != null)
                                    {
                                        documentoMotivoBaja.append("clave", bienInmueble.get(j).getMotivoBaja().getClave());
                                    }

                                    if (bienInmueble.get(j).getMotivoBaja().getValor() != null)
                                    {
                                        documentoMotivoBaja.append("valor", bienInmueble.get(j).getMotivoBaja().getValor());
                                    }

                                    if (documentoMotivoBaja.isEmpty() == false)
                                    {
                                        documentoBienInmueble.append("motivoBaja", documentoMotivoBaja);
                                    }
                                }

                            }
                        }
                    }
                }
                if (documentoBienInmueble.isEmpty() == false)
                {
                    arregloListaBienesInmuebles.add(documentoBienInmueble);
                }
            }
        } else
        {

            for (int j = 0; j < bienInmueble.size(); j++)
            {

                Document documentoBienInmueble = new Document();

                if (bienInmueble.get(j).getTipoOperacion() != null)
                {
                    documentoBienInmueble.append("tipoOperacion", bienInmueble.get(j).getTipoOperacion());
                }

                if (bienInmueble.get(j).getTipoInmueble() != null)
                {
                    Document documentoTipoInmueble = new Document();

                    if (bienInmueble.get(j).getTipoInmueble().getClave() != null)
                    {
                        documentoTipoInmueble.append("clave", bienInmueble.get(j).getTipoInmueble().getClave());
                    }

                    if (bienInmueble.get(j).getTipoInmueble().getValor() != null)
                    {
                        documentoTipoInmueble.append("valor", bienInmueble.get(j).getTipoInmueble().getValor());
                    }

                    if (documentoTipoInmueble.isEmpty() == false)
                    {
                        documentoBienInmueble.append("tipoInmueble", documentoTipoInmueble);
                    }
                }

                if (bienInmueble.get(j).getTitular() != null)
                {
                    arregloListaTitularBien = new ArrayList<>();
                    for (int k = 0; k < bienInmueble.get(j).getTitular().size(); k++)
                    {
                        Document documentoTitularBien = new Document();

                        if (bienInmueble.get(j).getTitular().get(k).getClave() != null)
                        {
                            documentoTitularBien.append("clave", bienInmueble.get(j).getTitular().get(k).getClave());
                        }

                        if (bienInmueble.get(j).getTitular().get(k).getValor() != null)
                        {
                            documentoTitularBien.append("valor", bienInmueble.get(j).getTitular().get(k).getValor());
                        }

                        if (documentoTitularBien.isEmpty() == false)
                        {
                            arregloListaTitularBien.add(documentoTitularBien);
                        }
                    }
                    if (arregloListaTitularBien.isEmpty() == false)
                    {
                        documentoBienInmueble.append("titular", arregloListaTitularBien);
                    }
                }

                if (bienInmueble.get(j).getPorcentajePropiedad() != null)
                {
                    documentoBienInmueble.append("porcentajePropiedad", bienInmueble.get(j).getPorcentajePropiedad());
                }

                if (bienInmueble.get(j).getSuperficieTerreno() != null)
                {
                    Document documentoSuperficieTerreno = new Document();

                    if (bienInmueble.get(j).getSuperficieTerreno().getValor() != null)
                    {
                        documentoSuperficieTerreno.append("valor", bienInmueble.get(j).getSuperficieTerreno().getValor());
                    }

                    if (bienInmueble.get(j).getSuperficieTerreno().getUnidad() != null)
                    {
                        documentoSuperficieTerreno.append("unidad", bienInmueble.get(j).getSuperficieTerreno().getUnidad());
                    }

                    if (documentoSuperficieTerreno.isEmpty() == false)
                    {
                        documentoBienInmueble.append("superficieTerreno", documentoSuperficieTerreno);
                    }
                }

                if (bienInmueble.get(j).getSuperficieConstruccion() != null)
                {
                    Document documentoSuperficieConstruccion = new Document();

                    if (bienInmueble.get(j).getSuperficieConstruccion().getValor() != null)
                    {
                        documentoSuperficieConstruccion.append("valor", bienInmueble.get(j).getSuperficieConstruccion().getValor());
                    }

                    if (bienInmueble.get(j).getSuperficieConstruccion().getUnidad() != null)
                    {
                        documentoSuperficieConstruccion.append("unidad", bienInmueble.get(j).getSuperficieConstruccion().getUnidad());
                    }

                    if (documentoSuperficieConstruccion.isEmpty() == false)
                    {
                        documentoBienInmueble.append("superficieConstruccion", documentoSuperficieConstruccion);
                    }
                }

                if (bienInmueble.get(j).getTercero() != null)
                {
                    arregloListaTercero = new ArrayList<>();
                    for (int l = 0; l < bienInmueble.get(j).getTercero().size(); l++)
                    {
                        Document documentoTercero = new Document();

                        if (bienInmueble.get(j).getTercero().get(l).getTipoPersona() != null)
                        {
                            documentoTercero.append("tipoPersona", bienInmueble.get(j).getTercero().get(l).getTipoPersona());
                        }

                        if (bienInmueble.get(j).getTercero().get(l).getNombreRazonSocial() != null)
                        {
                            documentoTercero.append("nombreRazonSocial", bienInmueble.get(j).getTercero().get(l).getNombreRazonSocial());
                        }

                        if (bienInmueble.get(j).getTercero().get(l).getRfc() != null)
                        {
                            documentoTercero.append("rfc", bienInmueble.get(j).getTercero().get(l).getRfc());
                        }

                        if (documentoTercero.isEmpty() == false)
                        {
                            arregloListaTercero.add(documentoTercero);
                        }
                    }
                    if (arregloListaTercero.isEmpty() == false)
                    {
                        documentoBienInmueble.append("tercero", arregloListaTercero);
                    }
                }

                if (bienInmueble.get(j).getTransmisor() != null)
                {
                    arregloListaTransmisor = new ArrayList<>();
                    for (int m = 0; m < bienInmueble.get(j).getTransmisor().size(); m++)
                    {
                        Document documentoTransmisor = new Document();

                        if (bienInmueble.get(j).getTransmisor().get(m).getTipoPersona() != null)
                        {
                            documentoTransmisor.append("tipoPersona", bienInmueble.get(j).getTransmisor().get(m).getTipoPersona());
                        }

                        if (bienInmueble.get(j).getTransmisor().get(m).getNombreRazonSocial() != null)
                        {
                            documentoTransmisor.append("nombreRazonSocial", bienInmueble.get(j).getTransmisor().get(m).getNombreRazonSocial());
                        }

                        if (bienInmueble.get(j).getTransmisor().get(m).getRfc() != null)
                        {
                            documentoTransmisor.append("rfc", bienInmueble.get(j).getTransmisor().get(m).getRfc());
                        }

                        if (bienInmueble.get(j).getTransmisor().get(m).getRelacion() != null)
                        {
                            Document documentoRelacion = new Document();

                            if (bienInmueble.get(j).getTransmisor().get(m).getRelacion().getClave() != null)
                            {
                                documentoRelacion.append("clave", bienInmueble.get(j).getTransmisor().get(m).getRelacion().getClave());
                            }

                            if (bienInmueble.get(j).getTransmisor().get(m).getRelacion().getValor() != null)
                            {
                                documentoRelacion.append("valor", bienInmueble.get(j).getTransmisor().get(m).getRelacion().getValor());
                            }

                            if (documentoRelacion.isEmpty() == false)
                            {
                                documentoTransmisor.append("relacion", documentoRelacion);
                            }
                        }

                        if (documentoTransmisor.isEmpty() == false)
                        {
                            arregloListaTransmisor.add(documentoTransmisor);
                        }
                    }
                    if (arregloListaTransmisor.isEmpty() == false)
                    {
                        documentoBienInmueble.append("transmisor", arregloListaTransmisor);
                    }
                }

                if (bienInmueble.get(j).getFormaAdquisicion() != null)
                {
                    Document documentoFormaAdquisicion = new Document();

                    if (bienInmueble.get(j).getFormaAdquisicion().getClave() != null)
                    {
                        documentoFormaAdquisicion.append("clave", bienInmueble.get(j).getFormaAdquisicion().getClave());
                    }

                    if (bienInmueble.get(j).getFormaAdquisicion().getValor() != null)
                    {
                        documentoFormaAdquisicion.append("valor", bienInmueble.get(j).getFormaAdquisicion().getValor());
                    }

                    if (documentoFormaAdquisicion.isEmpty() == false)
                    {
                        documentoBienInmueble.append("formaAdquisicion", documentoFormaAdquisicion);
                    }
                }

                if (bienInmueble.get(j).getFormaPago() != null)
                {
                    documentoBienInmueble.append("formaPago", bienInmueble.get(j).getFormaPago());
                }

                if (bienInmueble.get(j).getValorAdquisicion() != null)
                {
                    Document documentoValorAdquisicion = new Document();

                    if (bienInmueble.get(j).getValorAdquisicion().getValor() != null)
                    {
                        documentoValorAdquisicion.append("valor", bienInmueble.get(j).getValorAdquisicion().getValor());
                    }

                    if (bienInmueble.get(j).getValorAdquisicion().getMoneda() != null)
                    {
                        documentoValorAdquisicion.append("moneda", bienInmueble.get(j).getValorAdquisicion().getMoneda());
                    }

                    if (documentoValorAdquisicion.isEmpty() == false)
                    {
                        documentoBienInmueble.append("valorAdquisicion", documentoValorAdquisicion);
                    }
                }

                if (bienInmueble.get(j).getFechaAdquisicion() != null)
                {
                    documentoBienInmueble.append("fechaAdquisicion", bienInmueble.get(j).getFechaAdquisicion());
                }

                if (bienInmueble.get(j).getDatoIdentificacion() != null)
                {
                    documentoBienInmueble.append("datoIdentificacion", bienInmueble.get(j).getDatoIdentificacion());
                }

                if (bienInmueble.get(j).getValorConformeA() != null)
                {
                    documentoBienInmueble.append("valorConformeA", bienInmueble.get(j).getValorConformeA());
                }

                if (bienInmueble.get(j).getDomicilioMexico() != null)
                {
                    Document documentoDomicilioMexico = new DeclaracionesDocumentoDomicilioMexico(bienInmueble.get(j).getDomicilioMexico()).obtenerDocumentoDomicilioMexico();
                    if (documentoDomicilioMexico.isEmpty() == false)
                    {
                        documentoBienInmueble.append("domicilioMexico", documentoDomicilioMexico);
                    }
                }

                if (bienInmueble.get(j).getDomicilioExtranjero() != null)
                {
                    Document documentoDomicilioExtranjero = new DeclaracionesDocumentoDomicilioExtranjero(bienInmueble.get(j).getDomicilioExtranjero()).obtenerDocumentoDomicilioExtranjero();
                    if (documentoDomicilioExtranjero.isEmpty() == false)
                    {
                        documentoBienInmueble.append("domicilioExtranjero", documentoDomicilioExtranjero);
                    }
                }

                if (bienInmueble.get(j).getMotivoBaja() != null)
                {
                    Document documentoMotivoBaja = new Document();

                    if (bienInmueble.get(j).getMotivoBaja().getClave() != null)
                    {
                        documentoMotivoBaja.append("clave", bienInmueble.get(j).getMotivoBaja().getClave());
                    }

                    if (bienInmueble.get(j).getMotivoBaja().getValor() != null)
                    {
                        documentoMotivoBaja.append("valor", bienInmueble.get(j).getMotivoBaja().getValor());
                    }

                    if (documentoMotivoBaja.isEmpty() == false)
                    {
                        documentoBienInmueble.append("motivoBaja", documentoMotivoBaja);
                    }
                }

                if (documentoBienInmueble.isEmpty() == false)
                {
                    arregloListaBienesInmuebles.add(documentoBienInmueble);
                }
            }

        }

        return arregloListaBienesInmuebles;

    }

}
