package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesFideicomisos;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoFideicomisos
{

    private DeclaracionesFideicomisos datosFideicomiso;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoFideicomisos;

    public DeclaracionesDocumentoFideicomisos(DeclaracionesFideicomisos datosFideicomiso, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosFideicomiso = datosFideicomiso;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoFideicomisos()
    {

        documentoFideicomisos = new Document();

        if (datosFideicomiso != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (datosFideicomiso.getNinguno() != null)
                {
                    documentoFideicomisos.append("ninguno", datosFideicomiso.getNinguno());
                }

                if (datosFideicomiso.getFideicomiso() != null)
                {
                    ArrayList<Document> listaFideicomiso = new DeclaracionesListaDocumentosFideicomisos(datosFideicomiso.getFideicomiso(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosFideicomisos();
                    if (listaFideicomiso.isEmpty() == false)
                    {
                        documentoFideicomisos.append("fideicomiso", listaFideicomiso);
                    }
                }

            } else
            {

                if (datosFideicomiso.getNinguno() != null)
                {
                    documentoFideicomisos.append("ninguno", datosFideicomiso.getNinguno());
                }

                if (datosFideicomiso.getFideicomiso() != null)
                {
                    ArrayList<Document> listaFideicomiso = new DeclaracionesListaDocumentosFideicomisos(datosFideicomiso.getFideicomiso(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosFideicomisos();
                    if (listaFideicomiso.isEmpty() == false)
                    {
                        documentoFideicomisos.append("fideicomiso", listaFideicomiso);
                    }
                }

                if (datosFideicomiso.getAclaracionesObservaciones() != null)
                {
                    documentoFideicomisos.append("aclaracionesObservaciones", datosFideicomiso.getAclaracionesObservaciones());
                }

            }
        }

        return documentoFideicomisos;

    }

}
