package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesRepresentacionRepresentaciones;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosRepresentacion
{

    private ArrayList<DeclaracionesRepresentacionRepresentaciones> representacion;
    private boolean bolMostrarDatosPublicosYPrivados;
    private ArrayList<Document> arregloListaRepresentacion;

    public DeclaracionesListaDocumentosRepresentacion(ArrayList<DeclaracionesRepresentacionRepresentaciones> representacion, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.representacion = representacion;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public ArrayList<Document> obtenerArregloDocumentosListaRepresentacion()
    {

        arregloListaRepresentacion = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            for (int j = 0; j < representacion.size(); j++)
            {
                Document documentoRepresentacion = new Document();
                if (representacion.get(j).getTipoRelacion() != null)
                {
                    if (representacion.get(j).getTipoRelacion().equals("DECLARANTE"))
                    {

                        if (representacion.get(j).getTipoOperacion() != null)
                        {
                            documentoRepresentacion.append("tipoOperacion", representacion.get(j).getTipoOperacion());
                        }

                        documentoRepresentacion.append("tipoRelacion", representacion.get(j).getTipoRelacion());

                        if (representacion.get(j).getTipoRepresentacion() != null)
                        {
                            documentoRepresentacion.append("tipoRepresentacion", representacion.get(j).getTipoRepresentacion());
                        }

                        if (representacion.get(j).getFechaInicioRepresentacion() != null)
                        {
                            documentoRepresentacion.append("fechaInicioRepresentacion", representacion.get(j).getFechaInicioRepresentacion());
                        }

                        if (representacion.get(j).getTipoPersona() != null)
                        {
                            if (representacion.get(j).getTipoPersona().equals("MORAL"))
                            {

                                documentoRepresentacion.append("tipoPersona", representacion.get(j).getTipoPersona());

                                if (representacion.get(j).getNombreRazonSocial() != null)
                                {
                                    documentoRepresentacion.append("nombreRazonSocial", representacion.get(j).getNombreRazonSocial());
                                }

                                if (representacion.get(j).getRfc() != null)
                                {
                                    documentoRepresentacion.append("rfc", representacion.get(j).getRfc());
                                }
                            }
                        }

                        if (representacion.get(j).getRecibeRemuneracion() != null)
                        {
                            documentoRepresentacion.append("recibeRemuneracion", representacion.get(j).getRecibeRemuneracion());
                        }

                        if (representacion.get(j).getMontoMensual() != null)
                        {
                            Document documentoMontoMensual = new Document();

                            if (representacion.get(j).getMontoMensual().getValor() != null)
                            {
                                documentoMontoMensual.append("valor", representacion.get(j).getMontoMensual().getValor());
                            }

                            if (representacion.get(j).getMontoMensual().getMoneda() != null)
                            {
                                documentoMontoMensual.append("moneda", representacion.get(j).getMontoMensual().getMoneda());
                            }

                            if (documentoMontoMensual.isEmpty() == false)
                            {
                                documentoRepresentacion.append("montoMensual", documentoMontoMensual);
                            }
                        }

                        if (representacion.get(j).getUbicacion() != null)
                        {
                            Document documentoUbicacion = new Document();

                            if (representacion.get(j).getUbicacion().getPais() != null)
                            {
                                documentoUbicacion.append("pais", representacion.get(j).getUbicacion().getPais());
                            }

                            if (representacion.get(j).getUbicacion().getEntidadFederativa() != null)
                            {
                                Document documentoEntidadFederativa = new Document();

                                if (representacion.get(j).getUbicacion().getEntidadFederativa().getClave() != null)
                                {
                                    documentoEntidadFederativa.append("clave", representacion.get(j).getUbicacion().getEntidadFederativa().getClave());
                                }

                                if (representacion.get(j).getUbicacion().getEntidadFederativa().getValor() != null)
                                {
                                    documentoEntidadFederativa.append("valor", representacion.get(j).getUbicacion().getEntidadFederativa().getValor());
                                }

                                if (documentoEntidadFederativa.isEmpty() == false)
                                {
                                    documentoUbicacion.append("entidadFederativa", documentoEntidadFederativa);
                                }
                            }

                            if (documentoUbicacion.isEmpty() == false)
                            {
                                documentoRepresentacion.append("ubicacion", documentoUbicacion);
                            }
                        }

                        if (representacion.get(j).getSector() != null)
                        {
                            Document documentoSector = new Document();

                            if (representacion.get(j).getSector().getClave() != null)
                            {
                                documentoSector.append("clave", representacion.get(j).getSector().getClave());
                            }

                            if (representacion.get(j).getSector().getValor() != null)
                            {
                                documentoSector.append("valor", representacion.get(j).getSector().getValor());
                            }

                            if (documentoSector.isEmpty() == false)
                            {
                                documentoRepresentacion.append("sector", documentoSector);
                            }
                        }

                    }
                }
                if (documentoRepresentacion.isEmpty() == false)
                {
                    arregloListaRepresentacion.add(documentoRepresentacion);
                }
            }
        } else
        {

            for (int j = 0; j < representacion.size(); j++)
            {
                Document documentoRepresentacion = new Document();

                if (representacion.get(j).getTipoOperacion() != null)
                {
                    documentoRepresentacion.append("tipoOperacion", representacion.get(j).getTipoOperacion());
                }

                if (representacion.get(j).getTipoRelacion() != null)
                {
                    documentoRepresentacion.append("tipoRelacion", representacion.get(j).getTipoRelacion());
                }

                if (representacion.get(j).getTipoRepresentacion() != null)
                {
                    documentoRepresentacion.append("tipoRepresentacion", representacion.get(j).getTipoRepresentacion());
                }

                if (representacion.get(j).getFechaInicioRepresentacion() != null)
                {
                    documentoRepresentacion.append("fechaInicioRepresentacion", representacion.get(j).getFechaInicioRepresentacion());
                }

                if (representacion.get(j).getTipoPersona() != null)
                {
                    documentoRepresentacion.append("tipoPersona", representacion.get(j).getTipoPersona());
                }

                if (representacion.get(j).getNombreRazonSocial() != null)
                {
                    documentoRepresentacion.append("nombreRazonSocial", representacion.get(j).getNombreRazonSocial());
                }

                if (representacion.get(j).getRfc() != null)
                {
                    documentoRepresentacion.append("rfc", representacion.get(j).getRfc());
                }

                if (representacion.get(j).getRecibeRemuneracion() != null)
                {
                    documentoRepresentacion.append("recibeRemuneracion", representacion.get(j).getRecibeRemuneracion());
                }

                if (representacion.get(j).getMontoMensual() != null)
                {
                    Document documentoMontoMensual = new Document();

                    if (representacion.get(j).getMontoMensual().getValor() != null)
                    {
                        documentoMontoMensual.append("valor", representacion.get(j).getMontoMensual().getValor());
                    }

                    if (representacion.get(j).getMontoMensual().getMoneda() != null)
                    {
                        documentoMontoMensual.append("moneda", representacion.get(j).getMontoMensual().getMoneda());
                    }

                    if (documentoMontoMensual.isEmpty() == false)
                    {
                        documentoRepresentacion.append("montoMensual", documentoMontoMensual);
                    }
                }

                if (representacion.get(j).getUbicacion() != null)
                {
                    Document documentoUbicacion = new Document();

                    if (representacion.get(j).getUbicacion().getPais() != null)
                    {
                        documentoUbicacion.append("pais", representacion.get(j).getUbicacion().getPais());
                    }

                    if (representacion.get(j).getUbicacion().getEntidadFederativa() != null)
                    {
                        Document documentoEntidadFederativa = new Document();

                        if (representacion.get(j).getUbicacion().getEntidadFederativa().getClave() != null)
                        {
                            documentoEntidadFederativa.append("clave", representacion.get(j).getUbicacion().getEntidadFederativa().getClave());
                        }

                        if (representacion.get(j).getUbicacion().getEntidadFederativa().getValor() != null)
                        {
                            documentoEntidadFederativa.append("valor", representacion.get(j).getUbicacion().getEntidadFederativa().getValor());
                        }

                        if (documentoEntidadFederativa.isEmpty() == false)
                        {
                            documentoUbicacion.append("entidadFederativa", documentoEntidadFederativa);
                        }
                    }

                    if (documentoUbicacion.isEmpty() == false)
                    {
                        documentoRepresentacion.append("ubicacion", documentoUbicacion);
                    }
                }

                if (representacion.get(j).getSector() != null)
                {
                    Document documentoSector = new Document();

                    if (representacion.get(j).getSector().getClave() != null)
                    {
                        documentoSector.append("clave", representacion.get(j).getSector().getClave());
                    }

                    if (representacion.get(j).getSector().getValor() != null)
                    {
                        documentoSector.append("valor", representacion.get(j).getSector().getValor());
                    }

                    if (documentoSector.isEmpty() == false)
                    {
                        documentoRepresentacion.append("sector", documentoSector);
                    }
                }

                if (documentoRepresentacion.isEmpty() == false)
                {
                    arregloListaRepresentacion.add(documentoRepresentacion);
                }
            }

        }

        return arregloListaRepresentacion;

    }
}
