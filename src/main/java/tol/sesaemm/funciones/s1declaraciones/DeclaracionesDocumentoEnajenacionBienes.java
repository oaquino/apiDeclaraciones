package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesEnajenacionBienes;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoEnajenacionBienes
{

    private DeclaracionesEnajenacionBienes enajenacionBienes;
    private Document documentoEnajenacionBienes;

    public DeclaracionesDocumentoEnajenacionBienes(DeclaracionesEnajenacionBienes enajenacionBienes, String strTipoDeclaracion)
    {
        this.enajenacionBienes = enajenacionBienes;
    }

    public Document obtenerDocumentoEnajenacionBienes()
    {

        documentoEnajenacionBienes = new Document();
        List<Document> arregloListaBienes;

        if (enajenacionBienes.getRemuneracionTotal() != null)
        {
            Document documentoRemuneracionTotal = new DeclaracionesDocumentoMonto(enajenacionBienes.getRemuneracionTotal()).obtenerDocumentoMonto();
            if (documentoRemuneracionTotal.isEmpty() == false)
            {
                documentoEnajenacionBienes.append("remuneracionTotal", documentoRemuneracionTotal);
            }
        }

        if (enajenacionBienes.getBienes() != null)
        {
            arregloListaBienes = new ArrayList<>();
            for (int i = 0; i < enajenacionBienes.getBienes().size(); i++)
            {
                Document documentoBien = new Document();

                if (enajenacionBienes.getBienes().get(i).getRemuneracion() != null)
                {
                    Document documentoRemuneracion = new DeclaracionesDocumentoMonto(enajenacionBienes.getBienes().get(i).getRemuneracion()).obtenerDocumentoMonto();
                    if (documentoRemuneracion.isEmpty() == false)
                    {
                        documentoBien.append("remuneracion", documentoRemuneracion);
                    }
                }

                if (enajenacionBienes.getBienes().get(i).getTipoBienEnajenado() != null)
                {
                    documentoBien.append("tipoBienEnajenado", enajenacionBienes.getBienes().get(i).getTipoBienEnajenado());
                }

                if (documentoBien.isEmpty() == false)
                {
                    arregloListaBienes.add(documentoBien);
                }
            }
            documentoEnajenacionBienes.append("bienes", arregloListaBienes);
        }

        return documentoEnajenacionBienes;

    }

}
