package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DelaracionesDependienteEconomico;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosDependientesEconomicos
{

    private ArrayList<DelaracionesDependienteEconomico> dependienteEconomico;
    private ArrayList<Document> arregloListaDependientesEconomicos;

    public DeclaracionesListaDocumentosDependientesEconomicos(ArrayList<DelaracionesDependienteEconomico> dependienteEconomico)
    {
        this.dependienteEconomico = dependienteEconomico;
    }

    public ArrayList<Document> obtenerArregloDocumentosListaDependientesEconomicos()
    {

        arregloListaDependientesEconomicos = new ArrayList<>();

        for (int j = 0; j < dependienteEconomico.size(); j++)
        {
            Document documentoDependienteEconomico = new Document();

            if (dependienteEconomico.get(j).getTipoOperacion() != null)
            {
                documentoDependienteEconomico.append("tipoOperacion", dependienteEconomico.get(j).getTipoOperacion());
            }

            if (dependienteEconomico.get(j).getNombre() != null)
            {
                documentoDependienteEconomico.append("nombre", dependienteEconomico.get(j).getNombre());
            }

            if (dependienteEconomico.get(j).getPrimerApellido() != null)
            {
                documentoDependienteEconomico.append("primerApellido", dependienteEconomico.get(j).getPrimerApellido());
            }

            if (dependienteEconomico.get(j).getSegundoApellido() != null)
            {
                documentoDependienteEconomico.append("segundoApellido", dependienteEconomico.get(j).getSegundoApellido());
            }

            if (dependienteEconomico.get(j).getFechaNacimiento() != null)
            {
                documentoDependienteEconomico.append("fechaNacimiento", dependienteEconomico.get(j).getFechaNacimiento());
            }

            if (dependienteEconomico.get(j).getRfc() != null)
            {
                documentoDependienteEconomico.append("rfc", dependienteEconomico.get(j).getRfc());
            }

            if (dependienteEconomico.get(j).getParentescoRelacion() != null)
            {
                Document documentoParentescoRelacion = new Document();
                if (dependienteEconomico.get(j).getParentescoRelacion().getClave() != null)
                {
                    documentoParentescoRelacion.append("clave", dependienteEconomico.get(j).getParentescoRelacion().getClave());
                }
                if (dependienteEconomico.get(j).getParentescoRelacion().getValor() != null)
                {
                    documentoParentescoRelacion.append("valor", dependienteEconomico.get(j).getParentescoRelacion().getValor());
                }
                if (documentoParentescoRelacion.isEmpty() == false)
                {
                    documentoDependienteEconomico.append("parentescoRelacion", documentoParentescoRelacion);
                }
            }

            if (dependienteEconomico.get(j).getExtranjero() != null)
            {
                documentoDependienteEconomico.append("extranjero", dependienteEconomico.get(j).getExtranjero());
            }

            if (dependienteEconomico.get(j).getCurp() != null)
            {
                documentoDependienteEconomico.append("curp", dependienteEconomico.get(j).getCurp());
            }

            if (dependienteEconomico.get(j).getHabitaDomicilioDeclarante() != null)
            {
                documentoDependienteEconomico.append("habitaDomicilioDeclarante", dependienteEconomico.get(j).getHabitaDomicilioDeclarante());
            }

            if (dependienteEconomico.get(j).getLugarDondeReside() != null)
            {
                documentoDependienteEconomico.append("lugarDondeReside", dependienteEconomico.get(j).getLugarDondeReside());
            }

            if (dependienteEconomico.get(j).getDomicilioMexico() != null)
            {
                Document documentoDomicilioMexico = new DeclaracionesDocumentoDomicilioMexico(dependienteEconomico.get(j).getDomicilioMexico()).obtenerDocumentoDomicilioMexico();
                if (documentoDomicilioMexico.isEmpty() == false)
                {
                    documentoDependienteEconomico.append("domicilioMexico", documentoDomicilioMexico);
                }
            }

            if (dependienteEconomico.get(j).getDomicilioExtranjero() != null)
            {
                Document documentoDomicilioExtranjero = new DeclaracionesDocumentoDomicilioExtranjero(dependienteEconomico.get(j).getDomicilioExtranjero()).obtenerDocumentoDomicilioExtranjero();
                if (documentoDomicilioExtranjero.isEmpty() == false)
                {
                    documentoDependienteEconomico.append("domicilioExtranjero", documentoDomicilioExtranjero);
                }
            }

            if (dependienteEconomico.get(j).getActividadLaboral() != null)
            {
                Document documentoActividadLaboral = new DeclaracionesDocumentoActividadLaboral(dependienteEconomico.get(j).getActividadLaboral()).obtenerDocumentoActividadLaboral();
                if (documentoActividadLaboral.isEmpty() == false)
                {
                    documentoDependienteEconomico.append("actividadLaboral", documentoActividadLaboral);
                }
            }

            if (dependienteEconomico.get(j).getActividadLaboralSectorPublico() != null)
            {
                Document documentoActividadLaboralSectorPublico = new DeclaracionesDocumentoActividadLaboralSectorPublico(null, dependienteEconomico.get(j).getActividadLaboralSectorPublico(), "datosDependienteEconomico").obtenerDocumentoActividadLaboralSectorPublico();
                if (documentoActividadLaboralSectorPublico.isEmpty() == false)
                {
                    documentoDependienteEconomico.append("actividadLaboralSectorPublico", documentoActividadLaboralSectorPublico);
                }
            }

            if (dependienteEconomico.get(j).getActividadLaboralSectorPrivadoOtro() != null)
            {
                Document documentoActividadLaboralSectorPrivadoOtro = new DeclaracionesDocumentoActividadLaboralSectorPrivadoOtro(null, dependienteEconomico.get(j).getActividadLaboralSectorPrivadoOtro(), "datosDependienteEconomico").obtenerDocumentoActividadLaboralSectorPrivadoOtro();
                if (documentoActividadLaboralSectorPrivadoOtro.isEmpty() == false)
                {
                    documentoDependienteEconomico.append("actividadLaboralSectorPrivadoOtro", documentoActividadLaboralSectorPrivadoOtro);
                }
            }

            if (dependienteEconomico.get(j).getProveedorContratistaGobierno() != null)
            {
                documentoDependienteEconomico.append("proveedorContratistaGobierno", dependienteEconomico.get(j).getProveedorContratistaGobierno());
            }

            if (dependienteEconomico.get(j).getSector() != null)
            {
                Document documentoSector = new Document();
                if (dependienteEconomico.get(j).getSector().getClave() != null)
                {
                    documentoSector.append("clave", dependienteEconomico.get(j).getSector().getClave());
                }
                if (dependienteEconomico.get(j).getSector().getValor() != null)
                {
                    documentoSector.append("valor", dependienteEconomico.get(j).getSector().getValor());
                }
                if (documentoSector.isEmpty() == false)
                {
                    documentoDependienteEconomico.append("sector", documentoSector);
                }
            }

            if (documentoDependienteEconomico.isEmpty() == false)
            {
                arregloListaDependientesEconomicos.add(documentoDependienteEconomico);
            }
        }

        return arregloListaDependientesEconomicos;
    }

}
