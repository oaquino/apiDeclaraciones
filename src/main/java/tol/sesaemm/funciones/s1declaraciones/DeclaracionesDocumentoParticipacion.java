package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesParticipacion;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoParticipacion
{

    private DeclaracionesParticipacion datosParticipacion;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoParticipacion;

    public DeclaracionesDocumentoParticipacion(DeclaracionesParticipacion datosParticipacion, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosParticipacion = datosParticipacion;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoParticipacion()
    {

        documentoParticipacion = new Document();

        if (datosParticipacion != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (datosParticipacion.getNinguno() != null)
                {
                    documentoParticipacion.append("ninguno", datosParticipacion.getNinguno());
                }

                if (datosParticipacion.getParticipacion() != null)
                {
                    ArrayList<Document> listaParticipacion = new DeclaracionesListaDocumentosParticipacion(datosParticipacion.getParticipacion(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaParticipacion();
                    if (listaParticipacion.isEmpty() == false)
                    {
                        documentoParticipacion.append("participacion", listaParticipacion);
                    }
                }

            } else
            {

                if (datosParticipacion.getNinguno() != null)
                {
                    documentoParticipacion.append("ninguno", datosParticipacion.getNinguno());
                }

                if (datosParticipacion.getParticipacion() != null)
                {
                    ArrayList<Document> listaParticipacion = new DeclaracionesListaDocumentosParticipacion(datosParticipacion.getParticipacion(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaParticipacion();
                    if (listaParticipacion.isEmpty() == false)
                    {
                        documentoParticipacion.append("participacion", listaParticipacion);
                    }
                }

                if (datosParticipacion.getAclaracionesObservaciones() != null)
                {
                    documentoParticipacion.append("aclaracionesObservaciones", datosParticipacion.getAclaracionesObservaciones());
                }

            }
        }

        return documentoParticipacion;

    }

}
