package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesActividadFinanciera;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoActividadFinanciera
{

    private DeclaracionesActividadFinanciera actividadFinanciera;
    private Document documentoActividadFinanciera;

    public DeclaracionesDocumentoActividadFinanciera(DeclaracionesActividadFinanciera actividadFinanciera)
    {
        this.actividadFinanciera = actividadFinanciera;
    }

    public Document obtenerDocumentoActividadFinanciera()
    {

        documentoActividadFinanciera = new Document();
        List<Document> arregloListaActividades;

        if (actividadFinanciera.getRemuneracionTotal() != null)
        {
            Document documentoRemuneracionTotal = new DeclaracionesDocumentoMonto(actividadFinanciera.getRemuneracionTotal()).obtenerDocumentoMonto();
            if (documentoRemuneracionTotal.isEmpty() == false)
            {
                documentoActividadFinanciera.append("remuneracionTotal", documentoRemuneracionTotal);
            }
        }

        if (actividadFinanciera.getActividades() != null)
        {
            arregloListaActividades = new ArrayList<>();
            for (int i = 0; i < actividadFinanciera.getActividades().size(); i++)
            {
                Document documentoActividad = new Document();

                if (actividadFinanciera.getActividades().get(i).getRemuneracion() != null)
                {
                    Document documentoRemuneracion = new DeclaracionesDocumentoMonto(actividadFinanciera.getActividades().get(i).getRemuneracion()).obtenerDocumentoMonto();
                    if (documentoRemuneracion.isEmpty() == false)
                    {
                        documentoActividad.append("remuneracion", documentoRemuneracion);
                    }
                }

                if (actividadFinanciera.getActividades().get(i).getTipoInstrumento() != null)
                {
                    Document documentoTipoInstrumento = new DeclaracionesDocumentoTipoInstrumento(actividadFinanciera.getActividades().get(i).getTipoInstrumento()).obtenerDocumentoTipoInstrumento();
                    if (documentoTipoInstrumento.isEmpty() == false)
                    {
                        documentoActividad.append("tipoInstrumento", documentoTipoInstrumento);
                    }
                }

                if (documentoActividad.isEmpty() == false)
                {
                    arregloListaActividades.add(documentoActividad);
                }
            }
            documentoActividadFinanciera.append("actividades", arregloListaActividades);
        }

        return documentoActividadFinanciera;

    }

}
