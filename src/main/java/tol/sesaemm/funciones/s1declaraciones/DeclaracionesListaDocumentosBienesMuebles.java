package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesBienMueble;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosBienesMuebles
{

    private ArrayList<DeclaracionesBienMueble> bienMueble;
    private boolean bolMostrarDatosPublicosYPrivados;
    private ArrayList<Document> arregloListaBienesMuebles;

    public DeclaracionesListaDocumentosBienesMuebles(ArrayList<DeclaracionesBienMueble> bienMueble, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.bienMueble = bienMueble;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public ArrayList<Document> obtenerArregloDocumentosBienesMuebles()
    {

        List<Document> arregloListaTitularBien;
        List<Document> arregloListaTercero;
        List<Document> arregloListaTransmisor;

        arregloListaBienesMuebles = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            for (int j = 0; j < bienMueble.size(); j++)
            {
                Document documentoBienMueble = new Document();

                if (bienMueble.get(j).getTitular() != null)
                {
                    if (bienMueble.get(j).getTitular().size() == 1)
                    {
                        if (bienMueble.get(j).getTitular().get(0).getClave() != null)
                        {
                            if (bienMueble.get(j).getTitular().get(0).getClave().equals("DEC") == true)
                            {

                                if (bienMueble.get(j).getTipoOperacion() != null)
                                {
                                    documentoBienMueble.append("tipoOperacion", bienMueble.get(j).getTipoOperacion());
                                }

                                arregloListaTitularBien = new ArrayList<>();
                                Document documentoTitularBien = new Document();

                                documentoTitularBien.append("clave", bienMueble.get(j).getTitular().get(0).getClave());

                                if (bienMueble.get(j).getTitular().get(0).getValor() != null)
                                {
                                    documentoTitularBien.append("valor", bienMueble.get(j).getTitular().get(0).getValor());
                                }

                                if (documentoTitularBien.isEmpty() == false)
                                {
                                    arregloListaTitularBien.add(documentoTitularBien);
                                    documentoBienMueble.append("titular", arregloListaTitularBien);
                                }

                                if (bienMueble.get(j).getTipoBien() != null)
                                {
                                    Document documentoTipoBien = new Document();

                                    if (bienMueble.get(j).getTipoBien().getClave() != null)
                                    {
                                        documentoTipoBien.append("clave", bienMueble.get(j).getTipoBien().getClave());
                                    }

                                    if (bienMueble.get(j).getTipoBien().getValor() != null)
                                    {
                                        documentoTipoBien.append("valor", bienMueble.get(j).getTipoBien().getValor());
                                    }

                                    if (documentoTipoBien.isEmpty() == false)
                                    {
                                        documentoBienMueble.append("tipoBien", documentoTipoBien);
                                    }
                                }

                                if (bienMueble.get(j).getTransmisor() != null)
                                {
                                    arregloListaTransmisor = new ArrayList<>();
                                    for (int m = 0; m < bienMueble.get(j).getTransmisor().size(); m++)
                                    {
                                        Document documentoTransmisor = new Document();
                                        if (bienMueble.get(j).getTransmisor().get(m).getTipoPersona() != null)
                                        {
                                            if (bienMueble.get(j).getTransmisor().get(m).getTipoPersona().equals("MORAL"))
                                            {

                                                documentoTransmisor.append("tipoPersona", bienMueble.get(j).getTransmisor().get(m).getTipoPersona());

                                                if (bienMueble.get(j).getTransmisor().get(m).getNombreRazonSocial() != null)
                                                {
                                                    documentoTransmisor.append("nombreRazonSocial", bienMueble.get(j).getTransmisor().get(m).getNombreRazonSocial());
                                                }

                                                if (bienMueble.get(j).getTransmisor().get(m).getRfc() != null)
                                                {
                                                    documentoTransmisor.append("rfc", bienMueble.get(j).getTransmisor().get(m).getRfc());
                                                }

                                                if (bienMueble.get(j).getTransmisor().get(m).getRelacion() != null)
                                                {
                                                    Document documentoRelacion = new Document();
                                                    if (bienMueble.get(j).getTransmisor().get(m).getRelacion().getClave() != null)
                                                    {
                                                        if (bienMueble.get(j).getTransmisor().get(m).getRelacion().getClave().equals("NIN") || bienMueble.get(j).getTransmisor().get(m).getRelacion().getClave().equals("OTRO"))
                                                        {

                                                            documentoRelacion.append("clave", bienMueble.get(j).getTransmisor().get(m).getRelacion().getClave());

                                                            if (bienMueble.get(j).getTransmisor().get(m).getRelacion().getValor() != null)
                                                            {
                                                                documentoRelacion.append("valor", bienMueble.get(j).getTransmisor().get(m).getRelacion().getValor());
                                                            }

                                                        }
                                                    }
                                                    if (documentoRelacion.isEmpty() == false)
                                                    {
                                                        documentoTransmisor.append("relacion", documentoRelacion);
                                                    }
                                                }

                                            }
                                        }
                                        if (documentoTransmisor.isEmpty() == false)
                                        {
                                            arregloListaTransmisor.add(documentoTransmisor);
                                        }
                                    }
                                    if (arregloListaTransmisor.isEmpty() == false)
                                    {
                                        documentoBienMueble.append("transmisor", arregloListaTransmisor);
                                    }
                                }

                                if (bienMueble.get(j).getTercero() != null)
                                {
                                    arregloListaTercero = new ArrayList<>();
                                    for (int l = 0; l < bienMueble.get(j).getTercero().size(); l++)
                                    {
                                        Document documentoTercero = new Document();
                                        if (bienMueble.get(j).getTercero().get(l).getTipoPersona() != null)
                                        {
                                            if (bienMueble.get(j).getTercero().get(l).getTipoPersona().equals("MORAL"))
                                            {

                                                documentoTercero.append("tipoPersona", bienMueble.get(j).getTercero().get(l).getTipoPersona());

                                                if (bienMueble.get(j).getTercero().get(l).getNombreRazonSocial() != null)
                                                {
                                                    documentoTercero.append("nombreRazonSocial", bienMueble.get(j).getTercero().get(l).getNombreRazonSocial());
                                                }

                                                if (bienMueble.get(j).getTercero().get(l).getRfc() != null)
                                                {
                                                    documentoTercero.append("rfc", bienMueble.get(j).getTercero().get(l).getRfc());
                                                }

                                            }
                                        }
                                        if (documentoTercero.isEmpty() == false)
                                        {
                                            arregloListaTercero.add(documentoTercero);
                                        }
                                    }
                                    if (arregloListaTercero.isEmpty() == false)
                                    {
                                        documentoBienMueble.append("tercero", arregloListaTercero);
                                    }
                                }

                                if (bienMueble.get(j).getDescripcionGeneralBien() != null)
                                {
                                    documentoBienMueble.append("descripcionGeneralBien", bienMueble.get(j).getDescripcionGeneralBien());
                                }

                                if (bienMueble.get(j).getFormaAdquisicion() != null)
                                {
                                    Document documentoFormaAdquisicion = new Document();

                                    if (bienMueble.get(j).getFormaAdquisicion().getClave() != null)
                                    {
                                        documentoFormaAdquisicion.append("clave", bienMueble.get(j).getFormaAdquisicion().getClave());
                                    }

                                    if (bienMueble.get(j).getFormaAdquisicion().getValor() != null)
                                    {
                                        documentoFormaAdquisicion.append("valor", bienMueble.get(j).getFormaAdquisicion().getValor());
                                    }

                                    if (documentoFormaAdquisicion.isEmpty() == false)
                                    {
                                        documentoBienMueble.append("formaAdquisicion", documentoFormaAdquisicion);
                                    }
                                }

                                if (bienMueble.get(j).getFormaPago() != null)
                                {
                                    documentoBienMueble.append("formaPago", bienMueble.get(j).getFormaPago());
                                }

                                if (bienMueble.get(j).getValorAdquisicion() != null)
                                {
                                    Document documentoValorAdquisicion = new Document();

                                    if (bienMueble.get(j).getValorAdquisicion().getValor() != null)
                                    {
                                        documentoValorAdquisicion.append("valor", bienMueble.get(j).getValorAdquisicion().getValor());
                                    }

                                    if (bienMueble.get(j).getValorAdquisicion().getMoneda() != null)
                                    {
                                        documentoValorAdquisicion.append("moneda", bienMueble.get(j).getValorAdquisicion().getMoneda());
                                    }

                                    if (documentoValorAdquisicion.isEmpty() == false)
                                    {
                                        documentoBienMueble.append("valorAdquisicion", documentoValorAdquisicion);
                                    }
                                }

                                if (bienMueble.get(j).getFechaAdquisicion() != null)
                                {
                                    documentoBienMueble.append("fechaAdquisicion", bienMueble.get(j).getFechaAdquisicion());
                                }

                                if (bienMueble.get(j).getMotivoBaja() != null)
                                {
                                    Document documentoMotivoBaja = new Document();

                                    if (bienMueble.get(j).getMotivoBaja().getClave() != null)
                                    {
                                        documentoMotivoBaja.append("clave", bienMueble.get(j).getMotivoBaja().getClave());
                                    }

                                    if (bienMueble.get(j).getMotivoBaja().getValor() != null)
                                    {
                                        documentoMotivoBaja.append("valor", bienMueble.get(j).getMotivoBaja().getValor());
                                    }

                                    if (documentoMotivoBaja.isEmpty() == false)
                                    {
                                        documentoBienMueble.append("motivoBaja", documentoMotivoBaja);
                                    }
                                }

                            }
                        }
                    }
                }
                if (documentoBienMueble.isEmpty() == false)
                {
                    arregloListaBienesMuebles.add(documentoBienMueble);
                }
            }
        } else
        {

            for (int j = 0; j < bienMueble.size(); j++)
            {
                Document documentoBienMueble = new Document();

                if (bienMueble.get(j).getTipoOperacion() != null)
                {
                    documentoBienMueble.append("tipoOperacion", bienMueble.get(j).getTipoOperacion());
                }

                if (bienMueble.get(j).getTitular() != null)
                {
                    arregloListaTitularBien = new ArrayList<>();
                    for (int k = 0; k < bienMueble.get(j).getTitular().size(); k++)
                    {
                        Document documentoTitularBien = new Document();

                        if (bienMueble.get(j).getTitular().get(k).getClave() != null)
                        {
                            documentoTitularBien.append("clave", bienMueble.get(j).getTitular().get(k).getClave());
                        }

                        if (bienMueble.get(j).getTitular().get(k).getValor() != null)
                        {
                            documentoTitularBien.append("valor", bienMueble.get(j).getTitular().get(k).getValor());
                        }

                        if (documentoTitularBien.isEmpty() == false)
                        {
                            arregloListaTitularBien.add(documentoTitularBien);
                        }
                    }
                    if (arregloListaTitularBien.isEmpty() == false)
                    {
                        documentoBienMueble.append("titular", arregloListaTitularBien);
                    }
                }

                if (bienMueble.get(j).getTipoBien() != null)
                {
                    Document documentoTipoBien = new Document();

                    if (bienMueble.get(j).getTipoBien().getClave() != null)
                    {
                        documentoTipoBien.append("clave", bienMueble.get(j).getTipoBien().getClave());
                    }

                    if (bienMueble.get(j).getTipoBien().getValor() != null)
                    {
                        documentoTipoBien.append("valor", bienMueble.get(j).getTipoBien().getValor());
                    }

                    if (documentoTipoBien.isEmpty() == false)
                    {
                        documentoBienMueble.append("tipoBien", documentoTipoBien);
                    }
                }

                if (bienMueble.get(j).getTransmisor() != null)
                {
                    arregloListaTransmisor = new ArrayList<>();
                    for (int m = 0; m < bienMueble.get(j).getTransmisor().size(); m++)
                    {
                        Document documentoTransmisor = new Document();

                        if (bienMueble.get(j).getTransmisor().get(m).getTipoPersona() != null)
                        {
                            documentoTransmisor.append("tipoPersona", bienMueble.get(j).getTransmisor().get(m).getTipoPersona());
                        }

                        if (bienMueble.get(j).getTransmisor().get(m).getNombreRazonSocial() != null)
                        {
                            documentoTransmisor.append("nombreRazonSocial", bienMueble.get(j).getTransmisor().get(m).getNombreRazonSocial());
                        }

                        if (bienMueble.get(j).getTransmisor().get(m).getRfc() != null)
                        {
                            documentoTransmisor.append("rfc", bienMueble.get(j).getTransmisor().get(m).getRfc());
                        }

                        if (bienMueble.get(j).getTransmisor().get(m).getRelacion() != null)
                        {
                            Document documentoRelacion = new Document();

                            if (bienMueble.get(j).getTransmisor().get(m).getRelacion().getClave() != null)
                            {
                                documentoRelacion.append("clave", bienMueble.get(j).getTransmisor().get(m).getRelacion().getClave());
                            }

                            if (bienMueble.get(j).getTransmisor().get(m).getRelacion().getValor() != null)
                            {
                                documentoRelacion.append("valor", bienMueble.get(j).getTransmisor().get(m).getRelacion().getValor());
                            }

                            if (documentoRelacion.isEmpty() == false)
                            {
                                documentoTransmisor.append("relacion", documentoRelacion);
                            }
                        }

                        if (documentoTransmisor.isEmpty() == false)
                        {
                            arregloListaTransmisor.add(documentoTransmisor);
                        }
                    }
                    if (arregloListaTransmisor.isEmpty() == false)
                    {
                        documentoBienMueble.append("transmisor", arregloListaTransmisor);
                    }
                }

                if (bienMueble.get(j).getTercero() != null)
                {
                    arregloListaTercero = new ArrayList<>();
                    for (int l = 0; l < bienMueble.get(j).getTercero().size(); l++)
                    {
                        Document documentoTercero = new Document();

                        if (bienMueble.get(j).getTercero().get(l).getTipoPersona() != null)
                        {
                            documentoTercero.append("tipoPersona", bienMueble.get(j).getTercero().get(l).getTipoPersona());
                        }

                        if (bienMueble.get(j).getTercero().get(l).getNombreRazonSocial() != null)
                        {
                            documentoTercero.append("nombreRazonSocial", bienMueble.get(j).getTercero().get(l).getNombreRazonSocial());
                        }

                        if (bienMueble.get(j).getTercero().get(l).getRfc() != null)
                        {
                            documentoTercero.append("rfc", bienMueble.get(j).getTercero().get(l).getRfc());
                        }

                        if (documentoTercero.isEmpty() == false)
                        {
                            arregloListaTercero.add(documentoTercero);
                        }
                    }
                    if (arregloListaTercero.isEmpty() == false)
                    {
                        documentoBienMueble.append("tercero", arregloListaTercero);
                    }
                }

                if (bienMueble.get(j).getDescripcionGeneralBien() != null)
                {
                    documentoBienMueble.append("descripcionGeneralBien", bienMueble.get(j).getDescripcionGeneralBien());
                }

                if (bienMueble.get(j).getFormaAdquisicion() != null)
                {
                    Document documentoFormaAdquisicion = new Document();

                    if (bienMueble.get(j).getFormaAdquisicion().getClave() != null)
                    {
                        documentoFormaAdquisicion.append("clave", bienMueble.get(j).getFormaAdquisicion().getClave());
                    }

                    if (bienMueble.get(j).getFormaAdquisicion().getValor() != null)
                    {
                        documentoFormaAdquisicion.append("valor", bienMueble.get(j).getFormaAdquisicion().getValor());
                    }

                    if (documentoFormaAdquisicion.isEmpty() == false)
                    {
                        documentoBienMueble.append("formaAdquisicion", documentoFormaAdquisicion);
                    }
                }

                if (bienMueble.get(j).getFormaPago() != null)
                {
                    documentoBienMueble.append("formaPago", bienMueble.get(j).getFormaPago());
                }

                if (bienMueble.get(j).getValorAdquisicion() != null)
                {
                    Document documentoValorAdquisicion = new Document();

                    if (bienMueble.get(j).getValorAdquisicion().getValor() != null)
                    {
                        documentoValorAdquisicion.append("valor", bienMueble.get(j).getValorAdquisicion().getValor());
                    }

                    if (bienMueble.get(j).getValorAdquisicion().getMoneda() != null)
                    {
                        documentoValorAdquisicion.append("moneda", bienMueble.get(j).getValorAdquisicion().getMoneda());
                    }

                    if (documentoValorAdquisicion.isEmpty() == false)
                    {
                        documentoBienMueble.append("valorAdquisicion", documentoValorAdquisicion);
                    }
                }

                if (bienMueble.get(j).getFechaAdquisicion() != null)
                {
                    documentoBienMueble.append("fechaAdquisicion", bienMueble.get(j).getFechaAdquisicion());
                }

                if (bienMueble.get(j).getMotivoBaja() != null)
                {
                    Document documentoMotivoBaja = new Document();

                    if (bienMueble.get(j).getMotivoBaja().getClave() != null)
                    {
                        documentoMotivoBaja.append("clave", bienMueble.get(j).getMotivoBaja().getClave());
                    }

                    if (bienMueble.get(j).getMotivoBaja().getValor() != null)
                    {
                        documentoMotivoBaja.append("valor", bienMueble.get(j).getMotivoBaja().getValor());
                    }

                    if (documentoMotivoBaja.isEmpty() == false)
                    {
                        documentoBienMueble.append("motivoBaja", documentoMotivoBaja);
                    }
                }

                if (documentoBienMueble.isEmpty() == false)
                {
                    arregloListaBienesMuebles.add(documentoBienMueble);
                }
            }

        }

        return arregloListaBienesMuebles;

    }

}
