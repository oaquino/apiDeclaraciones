package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesDatosCurricularesDeclarante;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoDatosCurricularesDeclarante
{

    private DeclaracionesDatosCurricularesDeclarante datosCurriculares;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoDatosCurricularesDeclarante;

    public DeclaracionesDocumentoDatosCurricularesDeclarante(DeclaracionesDatosCurricularesDeclarante datosCurriculares, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosCurriculares = datosCurriculares;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoDatosCurricularesDeclarante()
    {

        documentoDatosCurricularesDeclarante = new Document();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {

            if (datosCurriculares.getEscolaridad() != null)
            {
                ArrayList<Document> arregloEscolaridad = new DeclaracionesListaDocumentosEscolaridad(datosCurriculares.getEscolaridad()).obtenerArregloDocumentosListaEscolaridad();
                documentoDatosCurricularesDeclarante.append("escolaridad", arregloEscolaridad);
            } else
            {
                documentoDatosCurricularesDeclarante.append("escolaridad", new ArrayList<Document>());
            }

        } else
        {

            if (datosCurriculares.getEscolaridad() != null)
            {
                ArrayList<Document> arregloEscolaridad = new DeclaracionesListaDocumentosEscolaridad(datosCurriculares.getEscolaridad()).obtenerArregloDocumentosListaEscolaridad();
                documentoDatosCurricularesDeclarante.append("escolaridad", arregloEscolaridad);
            } else
            {
                documentoDatosCurricularesDeclarante.append("escolaridad", new ArrayList<Document>());
            }

            if (datosCurriculares.getAclaracionesObservaciones() != null)
            {
                documentoDatosCurricularesDeclarante.append("aclaracionesObservaciones", datosCurriculares.getAclaracionesObservaciones());
            }

        }

        return documentoDatosCurricularesDeclarante;
    }

}
