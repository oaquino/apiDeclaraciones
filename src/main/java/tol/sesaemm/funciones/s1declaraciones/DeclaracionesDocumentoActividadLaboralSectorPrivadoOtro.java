package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesActividadLaboralSectorPrivadoOtroDatosPareja;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesActividadLaboralSectorPrivadoOtroDependienteEconomico;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoActividadLaboralSectorPrivadoOtro
{

    private DeclaracionesActividadLaboralSectorPrivadoOtroDatosPareja sectorPrivadoDatosPareja;
    private DeclaracionesActividadLaboralSectorPrivadoOtroDependienteEconomico sectorPrivadoDependienteEconomico;
    private Document documentoActividadLaboralSectorPrivadoOtro;
    private String strSeccion;

    public DeclaracionesDocumentoActividadLaboralSectorPrivadoOtro(DeclaracionesActividadLaboralSectorPrivadoOtroDatosPareja sectorPrivadoDatosPareja, DeclaracionesActividadLaboralSectorPrivadoOtroDependienteEconomico sectorPrivadoDependienteEconomico, String strSeccion)
    {
        this.sectorPrivadoDatosPareja = sectorPrivadoDatosPareja;
        this.sectorPrivadoDependienteEconomico = sectorPrivadoDependienteEconomico;
        this.strSeccion = strSeccion;
    }

    public Document obtenerDocumentoActividadLaboralSectorPrivadoOtro()
    {

        documentoActividadLaboralSectorPrivadoOtro = new Document();

        if (strSeccion.equals("datosPareja"))
        {

            if (sectorPrivadoDatosPareja.getNombreEmpresaSociedadAsociacion() != null)
            {
                documentoActividadLaboralSectorPrivadoOtro.append("nombreEmpresaSociedadAsociacion", sectorPrivadoDatosPareja.getNombreEmpresaSociedadAsociacion());
            }

            if (sectorPrivadoDatosPareja.getEmpleoCargoComision() != null)
            {
                documentoActividadLaboralSectorPrivadoOtro.append("empleoCargoComision", sectorPrivadoDatosPareja.getEmpleoCargoComision());
            }

            if (sectorPrivadoDatosPareja.getRfc() != null)
            {
                documentoActividadLaboralSectorPrivadoOtro.append("rfc", sectorPrivadoDatosPareja.getRfc());
            }

            if (sectorPrivadoDatosPareja.getFechaIngreso() != null)
            {
                documentoActividadLaboralSectorPrivadoOtro.append("fechaIngreso", sectorPrivadoDatosPareja.getFechaIngreso());
            }

            if (sectorPrivadoDatosPareja.getSector() != null)
            {
                Document documentoSector = new Document();
                if (sectorPrivadoDatosPareja.getSector().getClave() != null)
                {
                    documentoSector.append("clave", sectorPrivadoDatosPareja.getSector().getClave());
                }
                if (sectorPrivadoDatosPareja.getSector().getValor() != null)
                {
                    documentoSector.append("valor", sectorPrivadoDatosPareja.getSector().getValor());
                }
                if (documentoSector.isEmpty() == false)
                {
                    documentoActividadLaboralSectorPrivadoOtro.append("sector", documentoSector);
                }
            }

            if (sectorPrivadoDatosPareja.getSalarioMensualNeto() != null)
            {
                Document documentoSalarioMensualNeto = new Document();
                if (sectorPrivadoDatosPareja.getSalarioMensualNeto().getValor() != null)
                {
                    documentoSalarioMensualNeto.append("valor", sectorPrivadoDatosPareja.getSalarioMensualNeto().getValor());
                }
                if (sectorPrivadoDatosPareja.getSalarioMensualNeto().getMoneda() != null)
                {
                    documentoSalarioMensualNeto.append("moneda", sectorPrivadoDatosPareja.getSalarioMensualNeto().getMoneda());
                }
                if (documentoSalarioMensualNeto.isEmpty() == false)
                {
                    documentoActividadLaboralSectorPrivadoOtro.append("salarioMensualNeto", documentoSalarioMensualNeto);
                }
            }

            if (sectorPrivadoDatosPareja.getProveedorContratistaGobierno() != null)
            {
                documentoActividadLaboralSectorPrivadoOtro.append("proveedorContratistaGobierno", sectorPrivadoDatosPareja.getProveedorContratistaGobierno());
            }

        } else if (strSeccion.equals("datosDependienteEconomico"))
        {

            if (sectorPrivadoDependienteEconomico.getNombreEmpresaSociedadAsociacion() != null)
            {
                documentoActividadLaboralSectorPrivadoOtro.append("nombreEmpresaSociedadAsociacion", sectorPrivadoDependienteEconomico.getNombreEmpresaSociedadAsociacion());
            }

            if (sectorPrivadoDependienteEconomico.getRfc() != null)
            {
                documentoActividadLaboralSectorPrivadoOtro.append("rfc", sectorPrivadoDependienteEconomico.getRfc());
            }

            if (sectorPrivadoDependienteEconomico.getEmpleoCargo() != null)
            {
                documentoActividadLaboralSectorPrivadoOtro.append("empleoCargo", sectorPrivadoDependienteEconomico.getEmpleoCargo());
            }

            if (sectorPrivadoDependienteEconomico.getFechaIngreso() != null)
            {
                documentoActividadLaboralSectorPrivadoOtro.append("fechaIngreso", sectorPrivadoDependienteEconomico.getFechaIngreso());
            }

            if (sectorPrivadoDependienteEconomico.getSalarioMensualNeto() != null)
            {
                Document documentoSalarioMensualNeto = new Document();
                if (sectorPrivadoDependienteEconomico.getSalarioMensualNeto().getValor() != null)
                {
                    documentoSalarioMensualNeto.append("valor", sectorPrivadoDependienteEconomico.getSalarioMensualNeto().getValor());
                }
                if (sectorPrivadoDependienteEconomico.getSalarioMensualNeto().getMoneda() != null)
                {
                    documentoSalarioMensualNeto.append("moneda", sectorPrivadoDependienteEconomico.getSalarioMensualNeto().getMoneda());
                }
                if (documentoSalarioMensualNeto.isEmpty() == false)
                {
                    documentoActividadLaboralSectorPrivadoOtro.append("salarioMensualNeto", documentoSalarioMensualNeto);
                }
            }

        }

        return documentoActividadLaboralSectorPrivadoOtro;

    }

}
