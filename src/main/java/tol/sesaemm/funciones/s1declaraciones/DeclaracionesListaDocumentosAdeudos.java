package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesAdeudo;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosAdeudos
{

    private ArrayList<DeclaracionesAdeudo> adeudo;
    private String strTipoDeclaracion;
    private boolean bolMostrarDatosPublicosYPrivados;
    private ArrayList<Document> arregloListaAdeudos;

    public DeclaracionesListaDocumentosAdeudos(ArrayList<DeclaracionesAdeudo> adeudo, String strTipoDeclaracion, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.adeudo = adeudo;
        this.strTipoDeclaracion = strTipoDeclaracion;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public ArrayList<Document> obtenerArregloDocumentosListaAdeudos()
    {

        List<Document> arregloListaTitularBien;
        List<Document> arregloListaTercero;

        arregloListaAdeudos = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            for (int j = 0; j < adeudo.size(); j++)
            {
                Document documentoAdeudo = new Document();

                if (adeudo.get(j).getTitular() != null)
                {
                    if (adeudo.get(j).getTitular().size() == 1)
                    {
                        if (adeudo.get(j).getTitular().get(0).getClave() != null)
                        {
                            if (adeudo.get(j).getTitular().get(0).getClave().equals("DEC") == true)
                            {

                                if (adeudo.get(j).getTipoOperacion() != null)
                                {
                                    documentoAdeudo.append("tipoOperacion", adeudo.get(j).getTipoOperacion());
                                }

                                arregloListaTitularBien = new ArrayList<>();
                                Document documentoTitularBien = new Document();

                                documentoTitularBien.append("clave", adeudo.get(j).getTitular().get(0).getClave());

                                if (adeudo.get(j).getTitular().get(0).getValor() != null)
                                {
                                    documentoTitularBien.append("valor", adeudo.get(j).getTitular().get(0).getValor());
                                }

                                if (documentoTitularBien.isEmpty() == false)
                                {
                                    arregloListaTitularBien.add(documentoTitularBien);
                                    documentoAdeudo.append("titular", arregloListaTitularBien);
                                }

                                if (adeudo.get(j).getTipoAdeudo() != null)
                                {
                                    Document documentoTipoAdeudo = new Document();

                                    if (adeudo.get(j).getTipoAdeudo().getClave() != null)
                                    {
                                        documentoTipoAdeudo.append("clave", adeudo.get(j).getTipoAdeudo().getClave());
                                    }

                                    if (adeudo.get(j).getTipoAdeudo().getValor() != null)
                                    {
                                        documentoTipoAdeudo.append("valor", adeudo.get(j).getTipoAdeudo().getValor());
                                    }

                                    if (documentoTipoAdeudo.isEmpty() == false)
                                    {
                                        documentoAdeudo.append("tipoAdeudo", documentoTipoAdeudo);
                                    }
                                }

                                if (adeudo.get(j).getFechaAdquisicion() != null)
                                {
                                    documentoAdeudo.append("fechaAdquisicion", adeudo.get(j).getFechaAdquisicion());
                                }

                                if (adeudo.get(j).getMontoOriginal() != null)
                                {
                                    Document documentoMontoOriginal = new Document();

                                    if (adeudo.get(j).getMontoOriginal().getValor() != null)
                                    {
                                        documentoMontoOriginal.append("valor", adeudo.get(j).getMontoOriginal().getValor());
                                    }

                                    if (adeudo.get(j).getMontoOriginal().getMoneda() != null)
                                    {
                                        documentoMontoOriginal.append("moneda", adeudo.get(j).getMontoOriginal().getMoneda());
                                    }

                                    if (documentoMontoOriginal.isEmpty() == false)
                                    {
                                        documentoAdeudo.append("montoOriginal", documentoMontoOriginal);
                                    }
                                }

                                if (adeudo.get(j).getPorcentajeIncrementoDecremento() != null)
                                {
                                    documentoAdeudo.append("porcentajeIncrementoDecremento", adeudo.get(j).getPorcentajeIncrementoDecremento());
                                }

                                if (adeudo.get(j).getTercero() != null)
                                {
                                    arregloListaTercero = new ArrayList<>();
                                    for (int l = 0; l < adeudo.get(j).getTercero().size(); l++)
                                    {
                                        Document documentoTercero = new Document();
                                        if (adeudo.get(j).getTercero().get(l).getTipoPersona() != null)
                                        {
                                            if (adeudo.get(j).getTercero().get(l).getTipoPersona().equals("MORAL"))
                                            {

                                                documentoTercero.append("tipoPersona", adeudo.get(j).getTercero().get(l).getTipoPersona());

                                                if (adeudo.get(j).getTercero().get(l).getNombreRazonSocial() != null)
                                                {
                                                    documentoTercero.append("nombreRazonSocial", adeudo.get(j).getTercero().get(l).getNombreRazonSocial());
                                                }

                                                if (adeudo.get(j).getTercero().get(l).getRfc() != null)
                                                {
                                                    documentoTercero.append("rfc", adeudo.get(j).getTercero().get(l).getRfc());
                                                }

                                            }
                                        }
                                        if (documentoTercero.isEmpty() == false)
                                        {
                                            arregloListaTercero.add(documentoTercero);
                                        }
                                    }
                                    if (arregloListaTercero.isEmpty() == false)
                                    {
                                        documentoAdeudo.append("tercero", arregloListaTercero);
                                    }
                                }

                                if (adeudo.get(j).getOtorganteCredito() != null)
                                {
                                    Document documentoOtorganteCredito = new Document();
                                    if (adeudo.get(j).getOtorganteCredito().getTipoPersona() != null)
                                    {
                                        if (adeudo.get(j).getOtorganteCredito().getTipoPersona().equals("MORAL"))
                                        {

                                            documentoOtorganteCredito.append("tipoPersona", adeudo.get(j).getOtorganteCredito().getTipoPersona());

                                            if (adeudo.get(j).getOtorganteCredito().getNombreInstitucion() != null)
                                            {
                                                documentoOtorganteCredito.append("nombreInstitucion", adeudo.get(j).getOtorganteCredito().getNombreInstitucion());
                                            }

                                            if (adeudo.get(j).getOtorganteCredito().getRfc() != null)
                                            {
                                                documentoOtorganteCredito.append("rfc", adeudo.get(j).getOtorganteCredito().getRfc());
                                            }

                                        }
                                    }
                                    if (documentoOtorganteCredito.isEmpty() == false)
                                    {
                                        documentoAdeudo.append("otorganteCredito", documentoOtorganteCredito);
                                    }
                                }

                                if (adeudo.get(j).getLocalizacionAdeudo() != null)
                                {
                                    Document documentoLocalizacionAdeudo = new Document();

                                    if (adeudo.get(j).getLocalizacionAdeudo().getPais() != null)
                                    {
                                        documentoLocalizacionAdeudo.append("pais", adeudo.get(j).getLocalizacionAdeudo().getPais());
                                    }

                                    if (documentoLocalizacionAdeudo.isEmpty() == false)
                                    {
                                        documentoAdeudo.append("localizacionAdeudo", documentoLocalizacionAdeudo);
                                    }
                                }

                            }
                        }
                    }
                }
                if (documentoAdeudo.isEmpty() == false)
                {
                    arregloListaAdeudos.add(documentoAdeudo);
                }
            }
        } else
        {

            for (int j = 0; j < adeudo.size(); j++)
            {
                Document documentoAdeudo = new Document();

                if (adeudo.get(j).getTipoOperacion() != null)
                {
                    documentoAdeudo.append("tipoOperacion", adeudo.get(j).getTipoOperacion());
                }

                if (adeudo.get(j).getTitular() != null)
                {
                    arregloListaTitularBien = new ArrayList<>();
                    for (int k = 0; k < adeudo.get(j).getTitular().size(); k++)
                    {
                        Document documentoTitularBien = new Document();

                        if (adeudo.get(j).getTitular().get(k).getClave() != null)
                        {
                            documentoTitularBien.append("clave", adeudo.get(j).getTitular().get(k).getClave());
                        }

                        if (adeudo.get(j).getTitular().get(k).getValor() != null)
                        {
                            documentoTitularBien.append("valor", adeudo.get(j).getTitular().get(k).getValor());
                        }

                        if (documentoTitularBien.isEmpty() == false)
                        {
                            arregloListaTitularBien.add(documentoTitularBien);
                        }
                    }
                    if (arregloListaTitularBien.isEmpty() == false)
                    {
                        documentoAdeudo.append("titular", arregloListaTitularBien);
                    }
                }

                if (adeudo.get(j).getTipoAdeudo() != null)
                {
                    Document documentoTipoAdeudo = new Document();

                    if (adeudo.get(j).getTipoAdeudo().getClave() != null)
                    {
                        documentoTipoAdeudo.append("clave", adeudo.get(j).getTipoAdeudo().getClave());
                    }

                    if (adeudo.get(j).getTipoAdeudo().getValor() != null)
                    {
                        documentoTipoAdeudo.append("valor", adeudo.get(j).getTipoAdeudo().getValor());
                    }

                    if (documentoTipoAdeudo.isEmpty() == false)
                    {
                        documentoAdeudo.append("tipoAdeudo", documentoTipoAdeudo);
                    }
                }

                if (adeudo.get(j).getNumeroCuentaContrato() != null)
                {
                    documentoAdeudo.append("numeroCuentaContrato", adeudo.get(j).getNumeroCuentaContrato());
                }

                if (adeudo.get(j).getFechaAdquisicion() != null)
                {
                    documentoAdeudo.append("fechaAdquisicion", adeudo.get(j).getFechaAdquisicion());
                }

                if (adeudo.get(j).getMontoOriginal() != null)
                {
                    Document documentoMontoOriginal = new Document();

                    if (adeudo.get(j).getMontoOriginal().getValor() != null)
                    {
                        documentoMontoOriginal.append("valor", adeudo.get(j).getMontoOriginal().getValor());
                    }

                    if (adeudo.get(j).getMontoOriginal().getMoneda() != null)
                    {
                        documentoMontoOriginal.append("moneda", adeudo.get(j).getMontoOriginal().getMoneda());
                    }

                    if (documentoMontoOriginal.isEmpty() == false)
                    {
                        documentoAdeudo.append("montoOriginal", documentoMontoOriginal);
                    }
                }

                switch (strTipoDeclaracion)
                {
                    case "INICIAL":

                        if (adeudo.get(j).getSaldoInsolutoSituacionActual() != null)
                        {
                            Document documentoSaldoInsolutoSituacionActual = new Document();

                            if (adeudo.get(j).getSaldoInsolutoSituacionActual().getValor() != null)
                            {
                                documentoSaldoInsolutoSituacionActual.append("valor", adeudo.get(j).getSaldoInsolutoSituacionActual().getValor());
                            }

                            if (adeudo.get(j).getSaldoInsolutoSituacionActual().getMoneda() != null)
                            {
                                documentoSaldoInsolutoSituacionActual.append("moneda", adeudo.get(j).getSaldoInsolutoSituacionActual().getMoneda());
                            }

                            if (documentoSaldoInsolutoSituacionActual.isEmpty() == false)
                            {
                                documentoAdeudo.append("saldoInsolutoSituacionActual", documentoSaldoInsolutoSituacionActual);
                            }
                        }

                        break;
                    case "MODIFICACIÓN":

                        if (adeudo.get(j).getSaldoInsolutoDiciembreAnterior() != null)
                        {
                            Document documentoSaldoInsolutoDiciembreAnterior = new Document();

                            if (adeudo.get(j).getSaldoInsolutoDiciembreAnterior().getValor() != null)
                            {
                                documentoSaldoInsolutoDiciembreAnterior.append("valor", adeudo.get(j).getSaldoInsolutoDiciembreAnterior().getValor());
                            }

                            if (adeudo.get(j).getSaldoInsolutoDiciembreAnterior().getMoneda() != null)
                            {
                                documentoSaldoInsolutoDiciembreAnterior.append("moneda", adeudo.get(j).getSaldoInsolutoDiciembreAnterior().getMoneda());
                            }

                            if (documentoSaldoInsolutoDiciembreAnterior.isEmpty() == false)
                            {
                                documentoAdeudo.append("saldoInsolutoDiciembreAnterior", documentoSaldoInsolutoDiciembreAnterior);
                            }
                        }

                        break;
                    case "CONCLUSIÓN":

                        if (adeudo.get(j).getSaldoInsolutoFechaConclusion() != null)
                        {
                            Document documentoSaldoInsolutoFechaConclusion = new Document();

                            if (adeudo.get(j).getSaldoInsolutoFechaConclusion().getValor() != null)
                            {
                                documentoSaldoInsolutoFechaConclusion.append("valor", adeudo.get(j).getSaldoInsolutoFechaConclusion().getValor());
                            }

                            if (adeudo.get(j).getSaldoInsolutoFechaConclusion().getMoneda() != null)
                            {
                                documentoSaldoInsolutoFechaConclusion.append("moneda", adeudo.get(j).getSaldoInsolutoFechaConclusion().getMoneda());
                            }

                            if (documentoSaldoInsolutoFechaConclusion.isEmpty() == false)
                            {
                                documentoAdeudo.append("saldoInsolutoFechaConclusion", documentoSaldoInsolutoFechaConclusion);
                            }
                        }

                        break;
                    default:
                        break;
                }

                if (adeudo.get(j).getPorcentajeIncrementoDecremento() != null)
                {
                    documentoAdeudo.append("porcentajeIncrementoDecremento", adeudo.get(j).getPorcentajeIncrementoDecremento());
                }

                if (adeudo.get(j).getTercero() != null)
                {
                    arregloListaTercero = new ArrayList<>();
                    for (int l = 0; l < adeudo.get(j).getTercero().size(); l++)
                    {
                        Document documentoTercero = new Document();

                        if (adeudo.get(j).getTercero().get(l).getTipoPersona() != null)
                        {
                            documentoTercero.append("tipoPersona", adeudo.get(j).getTercero().get(l).getTipoPersona());
                        }

                        if (adeudo.get(j).getTercero().get(l).getNombreRazonSocial() != null)
                        {
                            documentoTercero.append("nombreRazonSocial", adeudo.get(j).getTercero().get(l).getNombreRazonSocial());
                        }

                        if (adeudo.get(j).getTercero().get(l).getRfc() != null)
                        {
                            documentoTercero.append("rfc", adeudo.get(j).getTercero().get(l).getRfc());
                        }

                        if (documentoTercero.isEmpty() == false)
                        {
                            arregloListaTercero.add(documentoTercero);
                        }
                    }
                    if (arregloListaTercero.isEmpty() == false)
                    {
                        documentoAdeudo.append("tercero", arregloListaTercero);
                    }
                }

                if (adeudo.get(j).getOtorganteCredito() != null)
                {
                    Document documentoOtorganteCredito = new Document();

                    if (adeudo.get(j).getOtorganteCredito().getTipoPersona() != null)
                    {
                        documentoOtorganteCredito.append("tipoPersona", adeudo.get(j).getOtorganteCredito().getTipoPersona());
                    }

                    if (adeudo.get(j).getOtorganteCredito().getNombreInstitucion() != null)
                    {
                        documentoOtorganteCredito.append("nombreInstitucion", adeudo.get(j).getOtorganteCredito().getNombreInstitucion());
                    }

                    if (adeudo.get(j).getOtorganteCredito().getRfc() != null)
                    {
                        documentoOtorganteCredito.append("rfc", adeudo.get(j).getOtorganteCredito().getRfc());
                    }

                    if (documentoOtorganteCredito.isEmpty() == false)
                    {
                        documentoAdeudo.append("otorganteCredito", documentoOtorganteCredito);
                    }
                }

                if (adeudo.get(j).getLocalizacionAdeudo() != null)
                {
                    Document documentoLocalizacionAdeudo = new Document();

                    if (adeudo.get(j).getLocalizacionAdeudo().getPais() != null)
                    {
                        documentoLocalizacionAdeudo.append("pais", adeudo.get(j).getLocalizacionAdeudo().getPais());
                    }

                    if (documentoLocalizacionAdeudo.isEmpty() == false)
                    {
                        documentoAdeudo.append("localizacionAdeudo", documentoLocalizacionAdeudo);
                    }
                }

                if (documentoAdeudo.isEmpty() == false)
                {
                    arregloListaAdeudos.add(documentoAdeudo);
                }
            }

        }

        return arregloListaAdeudos;

    }

}
