package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesIngresos;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoIngresos
{

    private DeclaracionesIngresos ingresos;
    private boolean bolMostrarDatosPublicosYPrivados;
    private String strTipoDeclaracion;
    private Document documentoIngresos;

    public DeclaracionesDocumentoIngresos(DeclaracionesIngresos ingresos, String strTipoDeclaracion, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.ingresos = ingresos;
        this.strTipoDeclaracion = strTipoDeclaracion;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoIngresos()
    {

        documentoIngresos = new Document();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            switch (this.strTipoDeclaracion)
            {
                case "INICIAL":

                    Document documentoMontoRemuneracion = new DeclaracionesDocumentoMonto(ingresos.getRemuneracionMensualCargoPublico()).obtenerDocumentoMonto();
                    documentoIngresos.append("remuneracionMensualCargoPublico", documentoMontoRemuneracion);

                    if (ingresos.getOtrosIngresosMensualesTotal() != null)
                    {
                        Document documentoOtrosIngresosMensualesTotal = new DeclaracionesDocumentoMonto(ingresos.getOtrosIngresosMensualesTotal()).obtenerDocumentoMonto();
                        if (documentoOtrosIngresosMensualesTotal.isEmpty() == false)
                        {
                            documentoIngresos.append("otrosIngresosMensualesTotal", documentoOtrosIngresosMensualesTotal);
                        }
                    }

                    if (ingresos.getActividadIndustialComercialEmpresarial() != null)
                    {
                        Document documentoActividadIndustialComercialEmpresarial = new DeclaracionesDocumentoActividadIndustialComercialEmpresarial(ingresos.getActividadIndustialComercialEmpresarial()).obtenerDocumentoActividadIndustialComercialEmpresarial();
                        if (documentoActividadIndustialComercialEmpresarial.isEmpty() == false)
                        {
                            documentoIngresos.append("actividadIndustialComercialEmpresarial", documentoActividadIndustialComercialEmpresarial);
                        }
                    }

                    if (ingresos.getActividadFinanciera() != null)
                    {
                        Document documentoActividadFinanciera = new DeclaracionesDocumentoActividadFinanciera(ingresos.getActividadFinanciera()).obtenerDocumentoActividadFinanciera();
                        if (documentoActividadFinanciera.isEmpty() == false)
                        {
                            documentoIngresos.append("actividadFinanciera", documentoActividadFinanciera);
                        }
                    }

                    if (ingresos.getServiciosProfesionales() != null)
                    {
                        Document documentoServiciosProfesionales = new DeclaracionesDocumentoServiciosProfesionales(ingresos.getServiciosProfesionales()).obtenerDocumentoServiciosProfesionales();
                        if (documentoServiciosProfesionales.isEmpty() == false)
                        {
                            documentoIngresos.append("serviciosProfesionales", documentoServiciosProfesionales);
                        }
                    }

                    if (ingresos.getOtrosIngresos() != null)
                    {
                        Document documentoOtrosIngresos = new DeclaracionesDocumentoOtrosIngresos(ingresos.getOtrosIngresos()).obtenerDocumentoOtrosIngresos();
                        if (documentoOtrosIngresos.isEmpty() == false)
                        {
                            documentoIngresos.append("otrosIngresos", documentoOtrosIngresos);
                        }
                    }

                    Document documentoMontoIngresoMensualNetoDeclarante = new DeclaracionesDocumentoMonto(ingresos.getIngresoMensualNetoDeclarante()).obtenerDocumentoMonto();
                    documentoIngresos.append("ingresoMensualNetoDeclarante", documentoMontoIngresoMensualNetoDeclarante);

                    Document documentoTemporal = new Document();
                    documentoTemporal.append("valor", -1);
                    documentoTemporal.append("moneda", "");
                    documentoIngresos.append("totalIngresosMensualesNetos", documentoTemporal);

                    break;
                case "MODIFICACIÓN":

                    Document documentoMontoRemuneracionAnualCargoPublico = new DeclaracionesDocumentoMonto(ingresos.getRemuneracionAnualCargoPublico()).obtenerDocumentoMonto();
                    documentoIngresos.append("remuneracionAnualCargoPublico", documentoMontoRemuneracionAnualCargoPublico);

                    if (ingresos.getOtrosIngresosAnualesTotal() != null)
                    {
                        Document documentoOtrosIngresosAnualesTotal = new DeclaracionesDocumentoMonto(ingresos.getOtrosIngresosAnualesTotal()).obtenerDocumentoMonto();
                        if (documentoOtrosIngresosAnualesTotal.isEmpty() == false)
                        {
                            documentoIngresos.append("otrosIngresosAnualesTotal", documentoOtrosIngresosAnualesTotal);
                        }
                    }

                    if (ingresos.getActividadIndustialComercialEmpresarial() != null)
                    {
                        Document documentoActividadIndustialComercialEmpresarial = new DeclaracionesDocumentoActividadIndustialComercialEmpresarial(ingresos.getActividadIndustialComercialEmpresarial()).obtenerDocumentoActividadIndustialComercialEmpresarial();
                        if (documentoActividadIndustialComercialEmpresarial.isEmpty() == false)
                        {
                            documentoIngresos.append("actividadIndustialComercialEmpresarial", documentoActividadIndustialComercialEmpresarial);
                        }
                    }

                    if (ingresos.getActividadFinanciera() != null)
                    {
                        Document documentoActividadFinanciera = new DeclaracionesDocumentoActividadFinanciera(ingresos.getActividadFinanciera()).obtenerDocumentoActividadFinanciera();
                        if (documentoActividadFinanciera.isEmpty() == false)
                        {
                            documentoIngresos.append("actividadFinanciera", documentoActividadFinanciera);
                        }
                    }

                    if (ingresos.getServiciosProfesionales() != null)
                    {
                        Document documentoServiciosProfesionales = new DeclaracionesDocumentoServiciosProfesionales(ingresos.getServiciosProfesionales()).obtenerDocumentoServiciosProfesionales();
                        if (documentoServiciosProfesionales.isEmpty() == false)
                        {
                            documentoIngresos.append("serviciosProfesionales", documentoServiciosProfesionales);
                        }
                    }

                    if (ingresos.getEnajenacionBienes() != null)
                    {
                        Document documentoEnajenacionBienes = new DeclaracionesDocumentoEnajenacionBienes(ingresos.getEnajenacionBienes(), this.strTipoDeclaracion).obtenerDocumentoEnajenacionBienes();
                        if (documentoEnajenacionBienes.isEmpty() == false)
                        {
                            documentoIngresos.append("enajenacionBienes", documentoEnajenacionBienes);
                        }
                    }

                    if (ingresos.getOtrosIngresos() != null)
                    {
                        Document documentoOtrosIngresos = new DeclaracionesDocumentoOtrosIngresos(ingresos.getOtrosIngresos()).obtenerDocumentoOtrosIngresos();
                        if (documentoOtrosIngresos.isEmpty() == false)
                        {
                            documentoIngresos.append("otrosIngresos", documentoOtrosIngresos);
                        }
                    }

                    Document documentoMontoIngresoAnualNetoDeclarante = new DeclaracionesDocumentoMonto(ingresos.getIngresoAnualNetoDeclarante()).obtenerDocumentoMonto();
                    documentoIngresos.append("ingresoAnualNetoDeclarante", documentoMontoIngresoAnualNetoDeclarante);

                    Document documentoTemporal2 = new Document();
                    documentoTemporal2.append("valor", -1);
                    documentoTemporal2.append("moneda", "");
                    documentoIngresos.append("totalIngresosAnualesNetos", documentoTemporal2);

                    break;
                case "CONCLUSIÓN":

                    Document documetnoMontoRemuneracionConclusionCargoPublico = new DeclaracionesDocumentoMonto(ingresos.getRemuneracionConclusionCargoPublico()).obtenerDocumentoMonto();
                    documentoIngresos.append("remuneracionConclusionCargoPublico", documetnoMontoRemuneracionConclusionCargoPublico);

                    if (ingresos.getOtrosIngresosConclusionTotal() != null)
                    {
                        Document documentoOtrosIngresosConclusionTotal = new DeclaracionesDocumentoMonto(ingresos.getOtrosIngresosConclusionTotal()).obtenerDocumentoMonto();
                        if (documentoOtrosIngresosConclusionTotal.isEmpty() == false)
                        {
                            documentoIngresos.append("otrosIngresosConclusionTotal", documentoOtrosIngresosConclusionTotal);
                        }
                    }

                    if (ingresos.getActividadIndustialComercialEmpresarial() != null)
                    {
                        Document documentoActividadIndustialComercialEmpresarial = new DeclaracionesDocumentoActividadIndustialComercialEmpresarial(ingresos.getActividadIndustialComercialEmpresarial()).obtenerDocumentoActividadIndustialComercialEmpresarial();
                        if (documentoActividadIndustialComercialEmpresarial.isEmpty() == false)
                        {
                            documentoIngresos.append("actividadIndustialComercialEmpresarial", documentoActividadIndustialComercialEmpresarial);
                        }
                    }

                    if (ingresos.getActividadFinanciera() != null)
                    {
                        Document documentoActividadFinanciera = new DeclaracionesDocumentoActividadFinanciera(ingresos.getActividadFinanciera()).obtenerDocumentoActividadFinanciera();
                        if (documentoActividadFinanciera.isEmpty() == false)
                        {
                            documentoIngresos.append("actividadFinanciera", documentoActividadFinanciera);
                        }
                    }

                    if (ingresos.getServiciosProfesionales() != null)
                    {
                        Document documentoServiciosProfesionales = new DeclaracionesDocumentoServiciosProfesionales(ingresos.getServiciosProfesionales()).obtenerDocumentoServiciosProfesionales();
                        if (documentoServiciosProfesionales.isEmpty() == false)
                        {
                            documentoIngresos.append("serviciosProfesionales", documentoServiciosProfesionales);
                        }
                    }

                    if (ingresos.getEnajenacionBienes() != null)
                    {
                        Document documentoEnajenacionBienes = new DeclaracionesDocumentoEnajenacionBienes(ingresos.getEnajenacionBienes(), this.strTipoDeclaracion).obtenerDocumentoEnajenacionBienes();
                        if (documentoEnajenacionBienes.isEmpty() == false)
                        {
                            documentoIngresos.append("enajenacionBienes", documentoEnajenacionBienes);
                        }
                    }

                    if (ingresos.getOtrosIngresos() != null)
                    {
                        Document documentoOtrosIngresos = new DeclaracionesDocumentoOtrosIngresos(ingresos.getOtrosIngresos()).obtenerDocumentoOtrosIngresos();
                        if (documentoOtrosIngresos.isEmpty() == false)
                        {
                            documentoIngresos.append("otrosIngresos", documentoOtrosIngresos);
                        }
                    }

                    Document documentoMontoIngresoConclusionNetoDeclarante = new DeclaracionesDocumentoMonto(ingresos.getIngresoConclusionNetoDeclarante()).obtenerDocumentoMonto();
                    documentoIngresos.append("ingresoConclusionNetoDeclarante", documentoMontoIngresoConclusionNetoDeclarante);

                    Document documentoTemporal3 = new Document();
                    documentoTemporal3.append("valor", -1);
                    documentoTemporal3.append("moneda", "");
                    documentoIngresos.append("totalIngresosConclusionNetos", documentoTemporal3);

                    break;
                default:
                    break;
            }
        } else
        {
            switch (this.strTipoDeclaracion)
            {
                case "INICIAL":

                    Document documentoMontoRemuneracion = new DeclaracionesDocumentoMonto(ingresos.getRemuneracionMensualCargoPublico()).obtenerDocumentoMonto();
                    documentoIngresos.append("remuneracionMensualCargoPublico", documentoMontoRemuneracion);

                    if (ingresos.getOtrosIngresosMensualesTotal() != null)
                    {
                        Document documentoOtrosIngresosMensualesTotal = new DeclaracionesDocumentoMonto(ingresos.getOtrosIngresosMensualesTotal()).obtenerDocumentoMonto();
                        if (documentoOtrosIngresosMensualesTotal.isEmpty() == false)
                        {
                            documentoIngresos.append("otrosIngresosMensualesTotal", documentoOtrosIngresosMensualesTotal);
                        }
                    }

                    if (ingresos.getActividadIndustialComercialEmpresarial() != null)
                    {
                        Document documentoActividadIndustialComercialEmpresarial = new DeclaracionesDocumentoActividadIndustialComercialEmpresarial(ingresos.getActividadIndustialComercialEmpresarial()).obtenerDocumentoActividadIndustialComercialEmpresarial();
                        if (documentoActividadIndustialComercialEmpresarial.isEmpty() == false)
                        {
                            documentoIngresos.append("actividadIndustialComercialEmpresarial", documentoActividadIndustialComercialEmpresarial);
                        }
                    }

                    if (ingresos.getActividadFinanciera() != null)
                    {
                        Document documentoActividadFinanciera = new DeclaracionesDocumentoActividadFinanciera(ingresos.getActividadFinanciera()).obtenerDocumentoActividadFinanciera();
                        if (documentoActividadFinanciera.isEmpty() == false)
                        {
                            documentoIngresos.append("actividadFinanciera", documentoActividadFinanciera);
                        }
                    }

                    if (ingresos.getServiciosProfesionales() != null)
                    {
                        Document documentoServiciosProfesionales = new DeclaracionesDocumentoServiciosProfesionales(ingresos.getServiciosProfesionales()).obtenerDocumentoServiciosProfesionales();
                        if (documentoServiciosProfesionales.isEmpty() == false)
                        {
                            documentoIngresos.append("serviciosProfesionales", documentoServiciosProfesionales);
                        }
                    }

                    if (ingresos.getOtrosIngresos() != null)
                    {
                        Document documentoOtrosIngresos = new DeclaracionesDocumentoOtrosIngresos(ingresos.getOtrosIngresos()).obtenerDocumentoOtrosIngresos();
                        if (documentoOtrosIngresos.isEmpty() == false)
                        {
                            documentoIngresos.append("otrosIngresos", documentoOtrosIngresos);
                        }
                    }

                    Document documentoMontoIngresoMensualNetoDeclarante = new DeclaracionesDocumentoMonto(ingresos.getIngresoMensualNetoDeclarante()).obtenerDocumentoMonto();
                    documentoIngresos.append("ingresoMensualNetoDeclarante", documentoMontoIngresoMensualNetoDeclarante);

                    if (ingresos.getIngresoMensualNetoParejaDependiente() != null)
                    {
                        Document documentoIngresoMensualNetoParejaDependiente = new DeclaracionesDocumentoMonto(ingresos.getIngresoMensualNetoParejaDependiente()).obtenerDocumentoMonto();
                        if (documentoIngresoMensualNetoParejaDependiente.isEmpty() == false)
                        {
                            documentoIngresos.append("ingresoMensualNetoParejaDependiente", documentoIngresoMensualNetoParejaDependiente);
                        }
                    }

                    Document documentoMontoTotalIngresosMensualesNetos = new DeclaracionesDocumentoMonto(ingresos.getTotalIngresosMensualesNetos()).obtenerDocumentoMonto();
                    documentoIngresos.append("totalIngresosMensualesNetos", documentoMontoTotalIngresosMensualesNetos);

                    if (ingresos.getAclaracionesObservaciones() != null)
                    {
                        documentoIngresos.append("aclaracionesObservaciones", ingresos.getAclaracionesObservaciones());
                    }

                    break;
                case "MODIFICACIÓN":

                    Document documentoMontoRemuneracionAnualCargoPublico = new DeclaracionesDocumentoMonto(ingresos.getRemuneracionAnualCargoPublico()).obtenerDocumentoMonto();
                    documentoIngresos.append("remuneracionAnualCargoPublico", documentoMontoRemuneracionAnualCargoPublico);

                    if (ingresos.getOtrosIngresosAnualesTotal() != null)
                    {
                        Document documentoOtrosIngresosAnualesTotal = new DeclaracionesDocumentoMonto(ingresos.getOtrosIngresosAnualesTotal()).obtenerDocumentoMonto();
                        if (documentoOtrosIngresosAnualesTotal.isEmpty() == false)
                        {
                            documentoIngresos.append("otrosIngresosAnualesTotal", documentoOtrosIngresosAnualesTotal);
                        }
                    }

                    if (ingresos.getActividadIndustialComercialEmpresarial() != null)
                    {
                        Document documentoActividadIndustialComercialEmpresarial = new DeclaracionesDocumentoActividadIndustialComercialEmpresarial(ingresos.getActividadIndustialComercialEmpresarial()).obtenerDocumentoActividadIndustialComercialEmpresarial();
                        if (documentoActividadIndustialComercialEmpresarial.isEmpty() == false)
                        {
                            documentoIngresos.append("actividadIndustialComercialEmpresarial", documentoActividadIndustialComercialEmpresarial);
                        }
                    }

                    if (ingresos.getActividadFinanciera() != null)
                    {
                        Document documentoActividadFinanciera = new DeclaracionesDocumentoActividadFinanciera(ingresos.getActividadFinanciera()).obtenerDocumentoActividadFinanciera();
                        if (documentoActividadFinanciera.isEmpty() == false)
                        {
                            documentoIngresos.append("actividadFinanciera", documentoActividadFinanciera);
                        }
                    }

                    if (ingresos.getServiciosProfesionales() != null)
                    {
                        Document documentoServiciosProfesionales = new DeclaracionesDocumentoServiciosProfesionales(ingresos.getServiciosProfesionales()).obtenerDocumentoServiciosProfesionales();
                        if (documentoServiciosProfesionales.isEmpty() == false)
                        {
                            documentoIngresos.append("serviciosProfesionales", documentoServiciosProfesionales);
                        }
                    }

                    if (ingresos.getEnajenacionBienes() != null)
                    {
                        Document documentoEnajenacionBienes = new DeclaracionesDocumentoEnajenacionBienes(ingresos.getEnajenacionBienes(), this.strTipoDeclaracion).obtenerDocumentoEnajenacionBienes();
                        if (documentoEnajenacionBienes.isEmpty() == false)
                        {
                            documentoIngresos.append("enajenacionBienes", documentoEnajenacionBienes);
                        }
                    }

                    if (ingresos.getOtrosIngresos() != null)
                    {
                        Document documentoOtrosIngresos = new DeclaracionesDocumentoOtrosIngresos(ingresos.getOtrosIngresos()).obtenerDocumentoOtrosIngresos();
                        if (documentoOtrosIngresos.isEmpty() == false)
                        {
                            documentoIngresos.append("otrosIngresos", documentoOtrosIngresos);
                        }
                    }

                    Document documentoMontoIngresoAnualNetoDeclarante = new DeclaracionesDocumentoMonto(ingresos.getIngresoAnualNetoDeclarante()).obtenerDocumentoMonto();
                    documentoIngresos.append("ingresoAnualNetoDeclarante", documentoMontoIngresoAnualNetoDeclarante);

                    if (ingresos.getIngresoAnualNetoParejaDependiente() != null)
                    {
                        Document documentoIngresoAnualNetoParejaDependiente = new DeclaracionesDocumentoMonto(ingresos.getIngresoAnualNetoParejaDependiente()).obtenerDocumentoMonto();
                        if (documentoIngresoAnualNetoParejaDependiente.isEmpty() == false)
                        {
                            documentoIngresos.append("ingresoAnualNetoParejaDependiente", documentoIngresoAnualNetoParejaDependiente);
                        }
                    }

                    Document documentoMontoTotalIngresosAnualesNetos = new DeclaracionesDocumentoMonto(ingresos.getTotalIngresosAnualesNetos()).obtenerDocumentoMonto();
                    documentoIngresos.append("totalIngresosAnualesNetos", documentoMontoTotalIngresosAnualesNetos);

                    if (ingresos.getAclaracionesObservaciones() != null)
                    {
                        documentoIngresos.append("aclaracionesObservaciones", ingresos.getAclaracionesObservaciones());
                    }

                    break;
                case "CONCLUSIÓN":

                    Document documetnoMontoRemuneracionConclusionCargoPublico = new DeclaracionesDocumentoMonto(ingresos.getRemuneracionConclusionCargoPublico()).obtenerDocumentoMonto();
                    documentoIngresos.append("remuneracionConclusionCargoPublico", documetnoMontoRemuneracionConclusionCargoPublico);

                    if (ingresos.getOtrosIngresosConclusionTotal() != null)
                    {
                        Document documentoOtrosIngresosConclusionTotal = new DeclaracionesDocumentoMonto(ingresos.getOtrosIngresosConclusionTotal()).obtenerDocumentoMonto();
                        if (documentoOtrosIngresosConclusionTotal.isEmpty() == false)
                        {
                            documentoIngresos.append("otrosIngresosConclusionTotal", documentoOtrosIngresosConclusionTotal);
                        }
                    }

                    if (ingresos.getActividadIndustialComercialEmpresarial() != null)
                    {
                        Document documentoActividadIndustialComercialEmpresarial = new DeclaracionesDocumentoActividadIndustialComercialEmpresarial(ingresos.getActividadIndustialComercialEmpresarial()).obtenerDocumentoActividadIndustialComercialEmpresarial();
                        if (documentoActividadIndustialComercialEmpresarial.isEmpty() == false)
                        {
                            documentoIngresos.append("actividadIndustialComercialEmpresarial", documentoActividadIndustialComercialEmpresarial);
                        }
                    }

                    if (ingresos.getActividadFinanciera() != null)
                    {
                        Document documentoActividadFinanciera = new DeclaracionesDocumentoActividadFinanciera(ingresos.getActividadFinanciera()).obtenerDocumentoActividadFinanciera();
                        if (documentoActividadFinanciera.isEmpty() == false)
                        {
                            documentoIngresos.append("actividadFinanciera", documentoActividadFinanciera);
                        }
                    }

                    if (ingresos.getServiciosProfesionales() != null)
                    {
                        Document documentoServiciosProfesionales = new DeclaracionesDocumentoServiciosProfesionales(ingresos.getServiciosProfesionales()).obtenerDocumentoServiciosProfesionales();
                        if (documentoServiciosProfesionales.isEmpty() == false)
                        {
                            documentoIngresos.append("serviciosProfesionales", documentoServiciosProfesionales);
                        }
                    }

                    if (ingresos.getEnajenacionBienes() != null)
                    {
                        Document documentoEnajenacionBienes = new DeclaracionesDocumentoEnajenacionBienes(ingresos.getEnajenacionBienes(), this.strTipoDeclaracion).obtenerDocumentoEnajenacionBienes();
                        if (documentoEnajenacionBienes.isEmpty() == false)
                        {
                            documentoIngresos.append("enajenacionBienes", documentoEnajenacionBienes);
                        }
                    }

                    if (ingresos.getOtrosIngresos() != null)
                    {
                        Document documentoOtrosIngresos = new DeclaracionesDocumentoOtrosIngresos(ingresos.getOtrosIngresos()).obtenerDocumentoOtrosIngresos();
                        if (documentoOtrosIngresos.isEmpty() == false)
                        {
                            documentoIngresos.append("otrosIngresos", documentoOtrosIngresos);
                        }
                    }

                    Document documentoMontoIngresoConclusionNetoDeclarante = new DeclaracionesDocumentoMonto(ingresos.getIngresoConclusionNetoDeclarante()).obtenerDocumentoMonto();
                    documentoIngresos.append("ingresoConclusionNetoDeclarante", documentoMontoIngresoConclusionNetoDeclarante);

                    if (ingresos.getIngresoConclusionNetoParejaDependiente() != null)
                    {
                        Document documentoIngresoConclusionNetoParejaDependiente = new DeclaracionesDocumentoMonto(ingresos.getIngresoConclusionNetoParejaDependiente()).obtenerDocumentoMonto();
                        if (documentoIngresoConclusionNetoParejaDependiente.isEmpty() == false)
                        {
                            documentoIngresos.append("ingresoConclusionNetoParejaDependiente", documentoIngresoConclusionNetoParejaDependiente);
                        }
                    }

                    Document documentoMontoTotalIngresosConclusionNetos = new DeclaracionesDocumentoMonto(ingresos.getTotalIngresosConclusionNetos()).obtenerDocumentoMonto();
                    documentoIngresos.append("totalIngresosConclusionNetos", documentoMontoTotalIngresosConclusionNetos);

                    if (ingresos.getAclaracionesObservaciones() != null)
                    {
                        documentoIngresos.append("aclaracionesObservaciones", ingresos.getAclaracionesObservaciones());
                    }

                    break;
                default:
                    break;
            }
        }

        return documentoIngresos;

    }

}
