package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesVehiculos;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoVehiculos
{

    private DeclaracionesVehiculos datosVehiculos;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoVehiculos;

    public DeclaracionesDocumentoVehiculos(DeclaracionesVehiculos datosVehiculos, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosVehiculos = datosVehiculos;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoVehiculos()
    {

        documentoVehiculos = new Document();

        if (datosVehiculos != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (datosVehiculos.getNinguno() != null)
                {
                    documentoVehiculos.append("ninguno", datosVehiculos.getNinguno());
                }

                if (datosVehiculos.getVehiculo() != null)
                {
                    ArrayList<Document> arregloVehiculos = new DeclaracionesListaDocumentosVehiculos(datosVehiculos.getVehiculo(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaVehiculos();
                    if (arregloVehiculos.isEmpty() == false)
                    {
                        documentoVehiculos.append("vehiculo", arregloVehiculos);
                    }
                }

            } else
            {

                if (datosVehiculos.getNinguno() != null)
                {
                    documentoVehiculos.append("ninguno", datosVehiculos.getNinguno());
                }

                if (datosVehiculos.getVehiculo() != null)
                {
                    ArrayList<Document> arregloVehiculos = new DeclaracionesListaDocumentosVehiculos(datosVehiculos.getVehiculo(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaVehiculos();
                    if (arregloVehiculos.isEmpty() == false)
                    {
                        documentoVehiculos.append("vehiculo", arregloVehiculos);
                    }
                }

                if (datosVehiculos.getAclaracionesObservaciones() != null)
                {
                    documentoVehiculos.append("aclaracionesObservaciones", datosVehiculos.getAclaracionesObservaciones());
                }

            }
        }

        return documentoVehiculos;

    }

}
