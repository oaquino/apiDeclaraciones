package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesBeneficio;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosBeneficiosPrivados
{

    private ArrayList<DeclaracionesBeneficio> beneficio;
    private boolean bolMostrarDatosPublicosYPrivados;
    private ArrayList<Document> arregloListaBeneficiosPrivados;

    public DeclaracionesListaDocumentosBeneficiosPrivados(ArrayList<DeclaracionesBeneficio> beneficio, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.beneficio = beneficio;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public ArrayList<Document> obtenerArregloDocumentosListaBeneficiosPrivados()
    {

        List<Document> arregloListaBeneficiario;

        arregloListaBeneficiosPrivados = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            for (int j = 0; j < beneficio.size(); j++)
            {
                Document documentoBeneficios = new Document();

                if (beneficio.get(j).getTipoOperacion() != null)
                {
                    documentoBeneficios.append("tipoOperacion", beneficio.get(j).getTipoOperacion());
                }

                if (beneficio.get(j).getTipoPersona() != null)
                {
                    documentoBeneficios.append("tipoPersona", beneficio.get(j).getTipoPersona());
                }

                if (beneficio.get(j).getTipoBeneficio() != null)
                {
                    Document documentoTipoBeneficio = new Document();

                    if (beneficio.get(j).getTipoBeneficio().getClave() != null)
                    {
                        documentoTipoBeneficio.append("clave", beneficio.get(j).getTipoBeneficio().getClave());
                    }

                    if (beneficio.get(j).getTipoBeneficio().getValor() != null)
                    {
                        documentoTipoBeneficio.append("valor", beneficio.get(j).getTipoBeneficio().getValor());
                    }

                    if (documentoTipoBeneficio.isEmpty() == false)
                    {
                        documentoBeneficios.append("tipoBeneficio", documentoTipoBeneficio);
                    }
                }

                arregloListaBeneficiario = new ArrayList<>();
                if (beneficio.get(j).getBeneficiario() != null)
                {
                    for (int k = 0; k < beneficio.get(j).getBeneficiario().size(); k++)
                    {
                        Document documentoBeneficio = new Document();
                        if (beneficio.get(j).getBeneficiario().get(k).getClave() != null)
                        {
                            if (beneficio.get(j).getBeneficiario().get(k).getClave().equals("DC") || beneficio.get(j).getBeneficiario().get(k).getClave().equals("OTRO"))
                            {

                                documentoBeneficio.append("clave", beneficio.get(j).getBeneficiario().get(k).getClave());

                                if (beneficio.get(j).getBeneficiario().get(k).getValor() != null)
                                {
                                    documentoBeneficio.append("valor", beneficio.get(j).getBeneficiario().get(k).getValor());
                                }

                            }
                        }
                        if (documentoBeneficio.isEmpty() == false)
                        {
                            arregloListaBeneficiario.add(documentoBeneficio);
                        }
                    }
                }
                if (arregloListaBeneficiario.isEmpty() == false)
                {
                    documentoBeneficios.append("beneficiario", arregloListaBeneficiario);
                }

                if (beneficio.get(j).getOtorgante() != null)
                {
                    Document documentoOtorgante = new Document();
                    if (beneficio.get(j).getOtorgante().getTipoPersona() != null)
                    {
                        if (beneficio.get(j).getOtorgante().getTipoPersona().equals("MORAL"))
                        {

                            documentoOtorgante.append("tipoPersona", beneficio.get(j).getOtorgante().getTipoPersona());

                            if (beneficio.get(j).getOtorgante().getNombreRazonSocial() != null)
                            {
                                documentoOtorgante.append("nombreRazonSocial", beneficio.get(j).getOtorgante().getNombreRazonSocial());
                            }

                            if (beneficio.get(j).getOtorgante().getRfc() != null)
                            {
                                documentoOtorgante.append("rfc", beneficio.get(j).getOtorgante().getRfc());
                            }

                        }
                    }
                    if (documentoOtorgante.isEmpty() == false)
                    {
                        documentoBeneficios.append("otorgante", documentoOtorgante);
                    }
                }

                if (beneficio.get(j).getFormaRecepcion() != null)
                {
                    documentoBeneficios.append("formaRecepcion", beneficio.get(j).getFormaRecepcion());
                }

                if (beneficio.get(j).getEspecifiqueBeneficio() != null)
                {
                    documentoBeneficios.append("especifiqueBeneficio", beneficio.get(j).getEspecifiqueBeneficio());
                }

                if (beneficio.get(j).getMontoMensualAproximado() != null)
                {
                    Document documentoMontoMensualAproximado = new Document();

                    if (beneficio.get(j).getMontoMensualAproximado().getValor() != null)
                    {
                        documentoMontoMensualAproximado.append("valor", beneficio.get(j).getMontoMensualAproximado().getValor());
                    }

                    if (beneficio.get(j).getMontoMensualAproximado().getMoneda() != null)
                    {
                        documentoMontoMensualAproximado.append("moneda", beneficio.get(j).getMontoMensualAproximado().getMoneda());
                    }

                    if (documentoMontoMensualAproximado.isEmpty() == false)
                    {
                        documentoBeneficios.append("montoMensualAproximado", documentoMontoMensualAproximado);
                    }
                }

                if (beneficio.get(j).getSector() != null)
                {
                    Document documentoSector = new Document();

                    if (beneficio.get(j).getSector().getClave() != null)
                    {
                        documentoSector.append("clave", beneficio.get(j).getSector().getClave());
                    }

                    if (beneficio.get(j).getSector().getValor() != null)
                    {
                        documentoSector.append("valor", beneficio.get(j).getSector().getValor());
                    }

                    if (documentoSector.isEmpty() == false)
                    {
                        documentoBeneficios.append("sector", documentoSector);
                    }
                }

                if (documentoBeneficios.isEmpty() == false)
                {
                    arregloListaBeneficiosPrivados.add(documentoBeneficios);
                }
            }
        } else
        {

            for (int j = 0; j < beneficio.size(); j++)
            {
                Document documentoBeneficios = new Document();

                if (beneficio.get(j).getTipoOperacion() != null)
                {
                    documentoBeneficios.append("tipoOperacion", beneficio.get(j).getTipoOperacion());
                }

                if (beneficio.get(j).getTipoPersona() != null)
                {
                    documentoBeneficios.append("tipoPersona", beneficio.get(j).getTipoPersona());
                }

                if (beneficio.get(j).getTipoBeneficio() != null)
                {
                    Document documentoTipoBeneficio = new Document();

                    if (beneficio.get(j).getTipoBeneficio().getClave() != null)
                    {
                        documentoTipoBeneficio.append("clave", beneficio.get(j).getTipoBeneficio().getClave());
                    }

                    if (beneficio.get(j).getTipoBeneficio().getValor() != null)
                    {
                        documentoTipoBeneficio.append("valor", beneficio.get(j).getTipoBeneficio().getValor());
                    }

                    if (documentoTipoBeneficio.isEmpty() == false)
                    {
                        documentoBeneficios.append("tipoBeneficio", documentoTipoBeneficio);
                    }
                }

                arregloListaBeneficiario = new ArrayList<>();
                if (beneficio.get(j).getBeneficiario() != null)
                {
                    for (int k = 0; k < beneficio.get(j).getBeneficiario().size(); k++)
                    {
                        Document documentoBeneficio = new Document();

                        if (beneficio.get(j).getBeneficiario().get(k).getClave() != null)
                        {
                            documentoBeneficio.append("clave", beneficio.get(j).getBeneficiario().get(k).getClave());
                        }

                        if (beneficio.get(j).getBeneficiario().get(k).getValor() != null)
                        {
                            documentoBeneficio.append("valor", beneficio.get(j).getBeneficiario().get(k).getValor());
                        }

                        if (documentoBeneficio.isEmpty() == false)
                        {
                            arregloListaBeneficiario.add(documentoBeneficio);
                        }
                    }
                }
                if (arregloListaBeneficiario.isEmpty() == false)
                {
                    documentoBeneficios.append("beneficiario", arregloListaBeneficiario);
                }

                if (beneficio.get(j).getOtorgante() != null)
                {
                    Document documentoOtorgante = new Document();

                    if (beneficio.get(j).getOtorgante().getTipoPersona() != null)
                    {
                        documentoOtorgante.append("tipoPersona", beneficio.get(j).getOtorgante().getTipoPersona());
                    }

                    if (beneficio.get(j).getOtorgante().getNombreRazonSocial() != null)
                    {
                        documentoOtorgante.append("nombreRazonSocial", beneficio.get(j).getOtorgante().getNombreRazonSocial());
                    }

                    if (beneficio.get(j).getOtorgante().getRfc() != null)
                    {
                        documentoOtorgante.append("rfc", beneficio.get(j).getOtorgante().getRfc());
                    }

                    if (documentoOtorgante.isEmpty() == false)
                    {
                        documentoBeneficios.append("otorgante", documentoOtorgante);
                    }
                }

                if (beneficio.get(j).getFormaRecepcion() != null)
                {
                    documentoBeneficios.append("formaRecepcion", beneficio.get(j).getFormaRecepcion());
                }

                if (beneficio.get(j).getEspecifiqueBeneficio() != null)
                {
                    documentoBeneficios.append("especifiqueBeneficio", beneficio.get(j).getEspecifiqueBeneficio());
                }

                if (beneficio.get(j).getMontoMensualAproximado() != null)
                {
                    Document documentoMontoMensualAproximado = new Document();

                    if (beneficio.get(j).getMontoMensualAproximado().getValor() != null)
                    {
                        documentoMontoMensualAproximado.append("valor", beneficio.get(j).getMontoMensualAproximado().getValor());
                    }

                    if (beneficio.get(j).getMontoMensualAproximado().getMoneda() != null)
                    {
                        documentoMontoMensualAproximado.append("moneda", beneficio.get(j).getMontoMensualAproximado().getMoneda());
                    }

                    if (documentoMontoMensualAproximado.isEmpty() == false)
                    {
                        documentoBeneficios.append("montoMensualAproximado", documentoMontoMensualAproximado);
                    }
                }

                if (beneficio.get(j).getSector() != null)
                {
                    Document documentoSector = new Document();

                    if (beneficio.get(j).getSector().getClave() != null)
                    {
                        documentoSector.append("clave", beneficio.get(j).getSector().getClave());
                    }

                    if (beneficio.get(j).getSector().getValor() != null)
                    {
                        documentoSector.append("valor", beneficio.get(j).getSector().getValor());
                    }

                    if (documentoSector.isEmpty() == false)
                    {
                        documentoBeneficios.append("sector", documentoSector);
                    }
                }

                if (documentoBeneficios.isEmpty() == false)
                {
                    arregloListaBeneficiosPrivados.add(documentoBeneficios);
                }
            }

        }

        return arregloListaBeneficiosPrivados;

    }

}
