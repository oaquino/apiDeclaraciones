package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesVehiculo;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosVehiculos
{

    private ArrayList<DeclaracionesVehiculo> vehiculo;
    private boolean bolMostrarDatosPublicosYPrivados;
    private ArrayList<Document> arregloListaVehiculos;

    public DeclaracionesListaDocumentosVehiculos(ArrayList<DeclaracionesVehiculo> vehiculo, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.vehiculo = vehiculo;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public ArrayList<Document> obtenerArregloDocumentosListaVehiculos()
    {

        List<Document> arregloListaTitularBien;
        List<Document> arregloListaTercero;
        List<Document> arregloListaTransmisor;

        arregloListaVehiculos = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            for (int j = 0; j < vehiculo.size(); j++)
            {

                Document documentoVehiculo = new Document();

                if (vehiculo.get(j).getTitular() != null)
                {
                    if (vehiculo.get(j).getTitular().size() == 1)
                    {
                        if (vehiculo.get(j).getTitular().get(0).getClave() != null)
                        {
                            if (vehiculo.get(j).getTitular().get(0).getClave().equals("DEC") == true)
                            {

                                if (vehiculo.get(j).getTipoOperacion() != null)
                                {
                                    documentoVehiculo.append("tipoOperacion", vehiculo.get(j).getTipoOperacion());
                                }

                                if (vehiculo.get(j).getTipoVehiculo() != null)
                                {
                                    Document documentoTipoVehiculo = new Document();

                                    if (vehiculo.get(j).getTipoVehiculo().getClave() != null)
                                    {
                                        documentoTipoVehiculo.append("clave", vehiculo.get(j).getTipoVehiculo().getClave());
                                    }

                                    if (vehiculo.get(j).getTipoVehiculo().getValor() != null)
                                    {
                                        documentoTipoVehiculo.append("valor", vehiculo.get(j).getTipoVehiculo().getValor());
                                    }

                                    if (documentoTipoVehiculo.isEmpty() == false)
                                    {
                                        documentoVehiculo.append("tipoVehiculo", documentoTipoVehiculo);
                                    }
                                }

                                arregloListaTitularBien = new ArrayList<>();
                                Document documentoTitularBien = new Document();

                                documentoTitularBien.append("clave", vehiculo.get(j).getTitular().get(0).getClave());

                                if (vehiculo.get(j).getTitular().get(0).getValor() != null)
                                {
                                    documentoTitularBien.append("valor", vehiculo.get(j).getTitular().get(0).getValor());
                                }

                                if (documentoTitularBien.isEmpty() == false)
                                {
                                    arregloListaTitularBien.add(documentoTitularBien);
                                    documentoVehiculo.append("titular", arregloListaTitularBien);
                                }

                                if (vehiculo.get(j).getTransmisor() != null)
                                {
                                    arregloListaTransmisor = new ArrayList<>();
                                    for (int m = 0; m < vehiculo.get(j).getTransmisor().size(); m++)
                                    {
                                        Document documentoTransmisor = new Document();
                                        if (vehiculo.get(j).getTransmisor().get(m).getTipoPersona() != null)
                                        {
                                            if (vehiculo.get(j).getTransmisor().get(m).getTipoPersona().equals("MORAL"))
                                            {

                                                documentoTransmisor.append("tipoPersona", vehiculo.get(j).getTransmisor().get(m).getTipoPersona());

                                                if (vehiculo.get(j).getTransmisor().get(m).getNombreRazonSocial() != null)
                                                {
                                                    documentoTransmisor.append("nombreRazonSocial", vehiculo.get(j).getTransmisor().get(m).getNombreRazonSocial());
                                                }

                                                if (vehiculo.get(j).getTransmisor().get(m).getRfc() != null)
                                                {
                                                    documentoTransmisor.append("rfc", vehiculo.get(j).getTransmisor().get(m).getRfc());
                                                }

                                                if (vehiculo.get(j).getTransmisor().get(m).getRelacion() != null)
                                                {
                                                    Document documentoRelacion = new Document();
                                                    if (vehiculo.get(j).getTransmisor().get(m).getRelacion().getClave() != null)
                                                    {
                                                        if (vehiculo.get(j).getTransmisor().get(m).getRelacion().getClave().equals("NIN") || vehiculo.get(j).getTransmisor().get(m).getRelacion().getClave().equals("OTRO"))
                                                        {

                                                            documentoRelacion.append("clave", vehiculo.get(j).getTransmisor().get(m).getRelacion().getClave());

                                                            if (vehiculo.get(j).getTransmisor().get(m).getRelacion().getValor() != null)
                                                            {
                                                                documentoRelacion.append("valor", vehiculo.get(j).getTransmisor().get(m).getRelacion().getValor());
                                                            }

                                                        }
                                                    }
                                                    if (documentoRelacion.isEmpty() == false)
                                                    {
                                                        documentoTransmisor.append("relacion", documentoRelacion);
                                                    }
                                                }

                                            }
                                        }
                                        if (documentoTransmisor.isEmpty() == false)
                                        {
                                            arregloListaTransmisor.add(documentoTransmisor);
                                        }
                                    }
                                    if (arregloListaTransmisor.isEmpty() == false)
                                    {
                                        documentoVehiculo.append("transmisor", arregloListaTransmisor);
                                    }
                                }

                                if (vehiculo.get(j).getMarca() != null)
                                {
                                    documentoVehiculo.append("marca", vehiculo.get(j).getMarca());
                                }

                                if (vehiculo.get(j).getModelo() != null)
                                {
                                    documentoVehiculo.append("modelo", vehiculo.get(j).getModelo());
                                }

                                if (vehiculo.get(j).getAnio() != null)
                                {
                                    documentoVehiculo.append("anio", vehiculo.get(j).getAnio());
                                }

                                if (vehiculo.get(j).getTercero() != null)
                                {
                                    arregloListaTercero = new ArrayList<>();
                                    for (int l = 0; l < vehiculo.get(j).getTercero().size(); l++)
                                    {
                                        Document documentoTercero = new Document();
                                        if (vehiculo.get(j).getTercero().get(l).getTipoPersona() != null)
                                        {
                                            if (vehiculo.get(j).getTercero().get(l).getTipoPersona().equals("MORAL"))
                                            {

                                                documentoTercero.append("tipoPersona", vehiculo.get(j).getTercero().get(l).getTipoPersona());

                                                if (vehiculo.get(j).getTercero().get(l).getNombreRazonSocial() != null)
                                                {
                                                    documentoTercero.append("nombreRazonSocial", vehiculo.get(j).getTercero().get(l).getNombreRazonSocial());
                                                }

                                                if (vehiculo.get(j).getTercero().get(l).getRfc() != null)
                                                {
                                                    documentoTercero.append("rfc", vehiculo.get(j).getTercero().get(l).getRfc());
                                                }

                                            }
                                        }
                                        if (documentoTercero.isEmpty() == false)
                                        {
                                            arregloListaTercero.add(documentoTercero);
                                        }
                                    }
                                    if (arregloListaTercero.isEmpty() == false)
                                    {
                                        documentoVehiculo.append("tercero", arregloListaTercero);
                                    }
                                }

                                if (vehiculo.get(j).getFormaAdquisicion() != null)
                                {
                                    Document documentoFormaAdquisicion = new Document();

                                    if (vehiculo.get(j).getFormaAdquisicion().getClave() != null)
                                    {
                                        documentoFormaAdquisicion.append("clave", vehiculo.get(j).getFormaAdquisicion().getClave());
                                    }

                                    if (vehiculo.get(j).getFormaAdquisicion().getValor() != null)
                                    {
                                        documentoFormaAdquisicion.append("valor", vehiculo.get(j).getFormaAdquisicion().getValor());
                                    }

                                    if (documentoFormaAdquisicion.isEmpty() == false)
                                    {
                                        documentoVehiculo.append("formaAdquisicion", documentoFormaAdquisicion);
                                    }
                                }

                                if (vehiculo.get(j).getFormaPago() != null)
                                {
                                    documentoVehiculo.append("formaPago", vehiculo.get(j).getFormaPago());
                                }

                                if (vehiculo.get(j).getValorAdquisicion() != null)
                                {
                                    Document documentoValorAdquisicion = new Document();

                                    if (vehiculo.get(j).getValorAdquisicion().getValor() != null)
                                    {
                                        documentoValorAdquisicion.append("valor", vehiculo.get(j).getValorAdquisicion().getValor());
                                    }

                                    if (vehiculo.get(j).getValorAdquisicion().getMoneda() != null)
                                    {
                                        documentoValorAdquisicion.append("moneda", vehiculo.get(j).getValorAdquisicion().getMoneda());
                                    }

                                    if (documentoValorAdquisicion.isEmpty() == false)
                                    {
                                        documentoVehiculo.append("valorAdquisicion", documentoValorAdquisicion);
                                    }
                                }

                                if (vehiculo.get(j).getFechaAdquisicion() != null)
                                {
                                    documentoVehiculo.append("fechaAdquisicion", vehiculo.get(j).getFechaAdquisicion());
                                }

                                if (vehiculo.get(j).getMotivoBaja() != null)
                                {
                                    Document documentoMotivoBaja = new Document();

                                    if (vehiculo.get(j).getMotivoBaja().getClave() != null)
                                    {
                                        documentoMotivoBaja.append("clave", vehiculo.get(j).getMotivoBaja().getClave());
                                    }

                                    if (vehiculo.get(j).getMotivoBaja().getValor() != null)
                                    {
                                        documentoMotivoBaja.append("valor", vehiculo.get(j).getMotivoBaja().getValor());
                                    }

                                    if (documentoMotivoBaja.isEmpty() == false)
                                    {
                                        documentoVehiculo.append("motivoBaja", documentoMotivoBaja);
                                    }
                                }

                            }
                        }
                    }
                }
                if (documentoVehiculo.isEmpty() == false)
                {
                    arregloListaVehiculos.add(documentoVehiculo);
                }
            }
        } else
        {

            for (int j = 0; j < vehiculo.size(); j++)
            {

                Document documentoVehiculo = new Document();

                if (vehiculo.get(j).getTipoOperacion() != null)
                {
                    documentoVehiculo.append("tipoOperacion", vehiculo.get(j).getTipoOperacion());
                }

                if (vehiculo.get(j).getTipoVehiculo() != null)
                {
                    Document documentoTipoVehiculo = new Document();

                    if (vehiculo.get(j).getTipoVehiculo().getClave() != null)
                    {
                        documentoTipoVehiculo.append("clave", vehiculo.get(j).getTipoVehiculo().getClave());
                    }

                    if (vehiculo.get(j).getTipoVehiculo().getValor() != null)
                    {
                        documentoTipoVehiculo.append("valor", vehiculo.get(j).getTipoVehiculo().getValor());
                    }

                    if (documentoTipoVehiculo.isEmpty() == false)
                    {
                        documentoVehiculo.append("tipoVehiculo", documentoTipoVehiculo);
                    }
                }

                if (vehiculo.get(j).getTitular() != null)
                {
                    arregloListaTitularBien = new ArrayList<>();
                    for (int k = 0; k < vehiculo.get(j).getTitular().size(); k++)
                    {
                        Document documentoTitularBien = new Document();

                        if (vehiculo.get(j).getTitular().get(k).getClave() != null)
                        {
                            documentoTitularBien.append("clave", vehiculo.get(j).getTitular().get(k).getClave());
                        }

                        if (vehiculo.get(j).getTitular().get(k).getValor() != null)
                        {
                            documentoTitularBien.append("valor", vehiculo.get(j).getTitular().get(k).getValor());
                        }

                        if (documentoTitularBien.isEmpty() == false)
                        {
                            arregloListaTitularBien.add(documentoTitularBien);
                        }
                    }
                    if (arregloListaTitularBien.isEmpty() == false)
                    {
                        documentoVehiculo.append("titular", arregloListaTitularBien);
                    }
                }

                if (vehiculo.get(j).getTransmisor() != null)
                {
                    arregloListaTransmisor = new ArrayList<>();
                    for (int m = 0; m < vehiculo.get(j).getTransmisor().size(); m++)
                    {
                        Document documentoTransmisor = new Document();

                        if (vehiculo.get(j).getTransmisor().get(m).getTipoPersona() != null)
                        {
                            documentoTransmisor.append("tipoPersona", vehiculo.get(j).getTransmisor().get(m).getTipoPersona());
                        }

                        if (vehiculo.get(j).getTransmisor().get(m).getNombreRazonSocial() != null)
                        {
                            documentoTransmisor.append("nombreRazonSocial", vehiculo.get(j).getTransmisor().get(m).getNombreRazonSocial());
                        }

                        if (vehiculo.get(j).getTransmisor().get(m).getRfc() != null)
                        {
                            documentoTransmisor.append("rfc", vehiculo.get(j).getTransmisor().get(m).getRfc());
                        }

                        if (vehiculo.get(j).getTransmisor().get(m).getRelacion() != null)
                        {
                            Document documentoRelacion = new Document();

                            if (vehiculo.get(j).getTransmisor().get(m).getRelacion().getClave() != null)
                            {
                                documentoRelacion.append("clave", vehiculo.get(j).getTransmisor().get(m).getRelacion().getClave());
                            }

                            if (vehiculo.get(j).getTransmisor().get(m).getRelacion().getValor() != null)
                            {
                                documentoRelacion.append("valor", vehiculo.get(j).getTransmisor().get(m).getRelacion().getValor());
                            }

                            if (documentoRelacion.isEmpty() == false)
                            {
                                documentoTransmisor.append("relacion", documentoRelacion);
                            }
                        }

                        if (documentoTransmisor.isEmpty() == false)
                        {
                            arregloListaTransmisor.add(documentoTransmisor);
                        }
                    }
                    if (arregloListaTransmisor.isEmpty() == false)
                    {
                        documentoVehiculo.append("transmisor", arregloListaTransmisor);
                    }
                }

                if (vehiculo.get(j).getMarca() != null)
                {
                    documentoVehiculo.append("marca", vehiculo.get(j).getMarca());
                }

                if (vehiculo.get(j).getModelo() != null)
                {
                    documentoVehiculo.append("modelo", vehiculo.get(j).getModelo());
                }

                if (vehiculo.get(j).getAnio() != null)
                {
                    documentoVehiculo.append("anio", vehiculo.get(j).getAnio());
                }

                if (vehiculo.get(j).getNumeroSerieRegistro() != null)
                {
                    documentoVehiculo.append("numeroSerieRegistro", vehiculo.get(j).getNumeroSerieRegistro());
                }

                if (vehiculo.get(j).getTercero() != null)
                {
                    arregloListaTercero = new ArrayList<>();
                    for (int l = 0; l < vehiculo.get(j).getTercero().size(); l++)
                    {
                        Document documentoTercero = new Document();

                        if (vehiculo.get(j).getTercero().get(l).getTipoPersona() != null)
                        {
                            documentoTercero.append("tipoPersona", vehiculo.get(j).getTercero().get(l).getTipoPersona());
                        }

                        if (vehiculo.get(j).getTercero().get(l).getNombreRazonSocial() != null)
                        {
                            documentoTercero.append("nombreRazonSocial", vehiculo.get(j).getTercero().get(l).getNombreRazonSocial());
                        }

                        if (vehiculo.get(j).getTercero().get(l).getRfc() != null)
                        {
                            documentoTercero.append("rfc", vehiculo.get(j).getTercero().get(l).getRfc());
                        }

                        if (documentoTercero.isEmpty() == false)
                        {
                            arregloListaTercero.add(documentoTercero);
                        }
                    }
                    if (arregloListaTercero.isEmpty() == false)
                    {
                        documentoVehiculo.append("tercero", arregloListaTercero);
                    }
                }

                if (vehiculo.get(j).getLugarRegistro() != null)
                {
                    Document documentoLugarRegistro = new Document();

                    if (vehiculo.get(j).getLugarRegistro().getPais() != null)
                    {
                        documentoLugarRegistro.append("pais", vehiculo.get(j).getLugarRegistro().getPais());
                    }

                    if (vehiculo.get(j).getLugarRegistro().getEntidadFederativa() != null)
                    {
                        Document documentoEntidadFederativa = new Document();

                        if (vehiculo.get(j).getLugarRegistro().getEntidadFederativa().getClave() != null)
                        {
                            documentoEntidadFederativa.append("clave", vehiculo.get(j).getLugarRegistro().getEntidadFederativa().getClave());
                        }

                        if (vehiculo.get(j).getLugarRegistro().getEntidadFederativa().getValor() != null)
                        {
                            documentoEntidadFederativa.append("valor", vehiculo.get(j).getLugarRegistro().getEntidadFederativa().getValor());
                        }

                        if (documentoEntidadFederativa.isEmpty() == false)
                        {
                            documentoLugarRegistro.append("entidadFederativa", documentoEntidadFederativa);
                        }
                    }

                    if (documentoLugarRegistro.isEmpty() == false)
                    {
                        documentoVehiculo.append("lugarRegistro", documentoLugarRegistro);
                    }
                }

                if (vehiculo.get(j).getFormaAdquisicion() != null)
                {
                    Document documentoFormaAdquisicion = new Document();

                    if (vehiculo.get(j).getFormaAdquisicion().getClave() != null)
                    {
                        documentoFormaAdquisicion.append("clave", vehiculo.get(j).getFormaAdquisicion().getClave());
                    }

                    if (vehiculo.get(j).getFormaAdquisicion().getValor() != null)
                    {
                        documentoFormaAdquisicion.append("valor", vehiculo.get(j).getFormaAdquisicion().getValor());
                    }

                    if (documentoFormaAdquisicion.isEmpty() == false)
                    {
                        documentoVehiculo.append("formaAdquisicion", documentoFormaAdquisicion);
                    }
                }

                if (vehiculo.get(j).getFormaPago() != null)
                {
                    documentoVehiculo.append("formaPago", vehiculo.get(j).getFormaPago());
                }

                if (vehiculo.get(j).getValorAdquisicion() != null)
                {
                    Document documentoValorAdquisicion = new Document();

                    if (vehiculo.get(j).getValorAdquisicion().getValor() != null)
                    {
                        documentoValorAdquisicion.append("valor", vehiculo.get(j).getValorAdquisicion().getValor());
                    }

                    if (vehiculo.get(j).getValorAdquisicion().getMoneda() != null)
                    {
                        documentoValorAdquisicion.append("moneda", vehiculo.get(j).getValorAdquisicion().getMoneda());
                    }

                    if (documentoValorAdquisicion.isEmpty() == false)
                    {
                        documentoVehiculo.append("valorAdquisicion", documentoValorAdquisicion);
                    }
                }

                if (vehiculo.get(j).getFechaAdquisicion() != null)
                {
                    documentoVehiculo.append("fechaAdquisicion", vehiculo.get(j).getFechaAdquisicion());
                }

                if (vehiculo.get(j).getMotivoBaja() != null)
                {
                    Document documentoMotivoBaja = new Document();

                    if (vehiculo.get(j).getMotivoBaja().getClave() != null)
                    {
                        documentoMotivoBaja.append("clave", vehiculo.get(j).getMotivoBaja().getClave());
                    }

                    if (vehiculo.get(j).getMotivoBaja().getValor() != null)
                    {
                        documentoMotivoBaja.append("valor", vehiculo.get(j).getMotivoBaja().getValor());
                    }

                    if (documentoMotivoBaja.isEmpty() == false)
                    {
                        documentoVehiculo.append("motivoBaja", documentoMotivoBaja);
                    }
                }

                if (documentoVehiculo.isEmpty() == false)
                {
                    arregloListaVehiculos.add(documentoVehiculo);
                }
            }

        }

        return arregloListaVehiculos;

    }

}
