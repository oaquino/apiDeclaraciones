/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesMonto;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoMonto
{

    private DeclaracionesMonto monto;
    private Document documentoMonto;

    public DeclaracionesDocumentoMonto(DeclaracionesMonto monto)
    {
        this.monto = monto;
    }

    public Document obtenerDocumentoMonto()
    {

        documentoMonto = new Document();

        if (monto != null)
        {

            if (monto.getValor() != null)
            {
                documentoMonto.append("valor", monto.getValor());
            }

            if (monto.getMoneda() != null)
            {
                documentoMonto.append("moneda", monto.getMoneda());
            }

        }

        return documentoMonto;
    }

}
