package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesActividadAnualAnterior;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoActividadAnualAnterior
{

    private DeclaracionesActividadAnualAnterior datosActividadAnualAnterior;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoActividadAnualAnterior;
    private String strTipoDeclaracion;

    public DeclaracionesDocumentoActividadAnualAnterior(DeclaracionesActividadAnualAnterior datosActividadAnualAnterior, String strTipoDeclaracion, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosActividadAnualAnterior = datosActividadAnualAnterior;
        this.strTipoDeclaracion = strTipoDeclaracion;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoActividadAnualAnterior()
    {

        documentoActividadAnualAnterior = new Document();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {

            documentoActividadAnualAnterior.append("servidorPublicoAnioAnterior", datosActividadAnualAnterior.getServidorPublicoAnioAnterior());

            if (datosActividadAnualAnterior.getFechaIngreso() != null)
            {
                documentoActividadAnualAnterior.append("fechaIngreso", datosActividadAnualAnterior.getFechaIngreso());
            }

            if (datosActividadAnualAnterior.getFechaConclusion() != null)
            {
                documentoActividadAnualAnterior.append("fechaConclusion", datosActividadAnualAnterior.getFechaConclusion());
            }

            if (datosActividadAnualAnterior.getRemuneracionNetaCargoPublico() != null)
            {
                Document documentoRemuneracionNetaCargoPublico = new DeclaracionesDocumentoMonto(datosActividadAnualAnterior.getRemuneracionNetaCargoPublico()).obtenerDocumentoMonto();
                if (documentoRemuneracionNetaCargoPublico.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("remuneracionNetaCargoPublico", documentoRemuneracionNetaCargoPublico);
                }
            }

            if (datosActividadAnualAnterior.getOtrosIngresosTotal() != null)
            {
                Document documentoOtrosIngresosTotal = new DeclaracionesDocumentoMonto(datosActividadAnualAnterior.getOtrosIngresosTotal()).obtenerDocumentoMonto();
                if (documentoOtrosIngresosTotal.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("otrosIngresosTotal", documentoOtrosIngresosTotal);
                }
            }

            if (datosActividadAnualAnterior.getActividadIndustialComercialEmpresarial() != null)
            {
                Document documentoActividadIndustialComercialEmpresarial = new DeclaracionesDocumentoActividadIndustialComercialEmpresarial(datosActividadAnualAnterior.getActividadIndustialComercialEmpresarial()).obtenerDocumentoActividadIndustialComercialEmpresarial();
                if (documentoActividadIndustialComercialEmpresarial.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("actividadIndustialComercialEmpresarial", documentoActividadIndustialComercialEmpresarial);
                }
            }

            if (datosActividadAnualAnterior.getActividadFinanciera() != null)
            {
                Document documentoActividadFinanciera = new DeclaracionesDocumentoActividadFinanciera(datosActividadAnualAnterior.getActividadFinanciera()).obtenerDocumentoActividadFinanciera();
                if (documentoActividadFinanciera.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("actividadFinanciera", documentoActividadFinanciera);
                }
            }

            if (datosActividadAnualAnterior.getServiciosProfesionales() != null)
            {
                Document documentoServiciosProfesionales = new DeclaracionesDocumentoServiciosProfesionales(datosActividadAnualAnterior.getServiciosProfesionales()).obtenerDocumentoServiciosProfesionales();
                if (documentoServiciosProfesionales.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("serviciosProfesionales", documentoServiciosProfesionales);
                }
            }

            if (datosActividadAnualAnterior.getEnajenacionBienes() != null)
            {
                Document documentoEnajenacionBienes = new DeclaracionesDocumentoEnajenacionBienes(datosActividadAnualAnterior.getEnajenacionBienes(), strTipoDeclaracion).obtenerDocumentoEnajenacionBienes();
                if (documentoEnajenacionBienes.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("enajenacionBienes", documentoEnajenacionBienes);
                }
            }

            if (datosActividadAnualAnterior.getOtrosIngresos() != null)
            {
                Document documentoOtrosIngresos = new DeclaracionesDocumentoOtrosIngresos(datosActividadAnualAnterior.getOtrosIngresos()).obtenerDocumentoOtrosIngresos();
                if (documentoOtrosIngresos.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("otrosIngresos", documentoOtrosIngresos);
                }
            }

            if (datosActividadAnualAnterior.getIngresoNetoAnualDeclarante() != null)
            {
                Document documentoIngresoNetoAnualDeclarante = new DeclaracionesDocumentoMonto(datosActividadAnualAnterior.getIngresoNetoAnualDeclarante()).obtenerDocumentoMonto();
                if (documentoIngresoNetoAnualDeclarante.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("ingresoNetoAnualDeclarante", documentoIngresoNetoAnualDeclarante);
                }
            }

            if (datosActividadAnualAnterior.getTotalIngresosNetosAnuales() != null)
            {
                Document documenTototalIngresosNetosAnuales = new DeclaracionesDocumentoMonto(datosActividadAnualAnterior.getTotalIngresosNetosAnuales()).obtenerDocumentoMonto();
                if (documenTototalIngresosNetosAnuales.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("totalIngresosNetosAnuales", documenTototalIngresosNetosAnuales);
                }
            }

        } else
        {

            documentoActividadAnualAnterior.append("servidorPublicoAnioAnterior", datosActividadAnualAnterior.getServidorPublicoAnioAnterior());

            if (datosActividadAnualAnterior.getFechaIngreso() != null)
            {
                documentoActividadAnualAnterior.append("fechaIngreso", datosActividadAnualAnterior.getFechaIngreso());
            }

            if (datosActividadAnualAnterior.getFechaConclusion() != null)
            {
                documentoActividadAnualAnterior.append("fechaConclusion", datosActividadAnualAnterior.getFechaConclusion());
            }

            if (datosActividadAnualAnterior.getRemuneracionNetaCargoPublico() != null)
            {
                Document documentoRemuneracionNetaCargoPublico = new DeclaracionesDocumentoMonto(datosActividadAnualAnterior.getRemuneracionNetaCargoPublico()).obtenerDocumentoMonto();
                if (documentoRemuneracionNetaCargoPublico.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("remuneracionNetaCargoPublico", documentoRemuneracionNetaCargoPublico);
                }
            }

            if (datosActividadAnualAnterior.getOtrosIngresosTotal() != null)
            {
                Document documentoOtrosIngresosTotal = new DeclaracionesDocumentoMonto(datosActividadAnualAnterior.getOtrosIngresosTotal()).obtenerDocumentoMonto();
                if (documentoOtrosIngresosTotal.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("otrosIngresosTotal", documentoOtrosIngresosTotal);
                }
            }

            if (datosActividadAnualAnterior.getActividadIndustialComercialEmpresarial() != null)
            {
                Document documentoActividadIndustialComercialEmpresarial = new DeclaracionesDocumentoActividadIndustialComercialEmpresarial(datosActividadAnualAnterior.getActividadIndustialComercialEmpresarial()).obtenerDocumentoActividadIndustialComercialEmpresarial();
                if (documentoActividadIndustialComercialEmpresarial.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("actividadIndustialComercialEmpresarial", documentoActividadIndustialComercialEmpresarial);
                }
            }

            if (datosActividadAnualAnterior.getActividadFinanciera() != null)
            {
                Document documentoActividadFinanciera = new DeclaracionesDocumentoActividadFinanciera(datosActividadAnualAnterior.getActividadFinanciera()).obtenerDocumentoActividadFinanciera();
                if (documentoActividadFinanciera.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("actividadFinanciera", documentoActividadFinanciera);
                }
            }

            if (datosActividadAnualAnterior.getServiciosProfesionales() != null)
            {
                Document documentoServiciosProfesionales = new DeclaracionesDocumentoServiciosProfesionales(datosActividadAnualAnterior.getServiciosProfesionales()).obtenerDocumentoServiciosProfesionales();
                if (documentoServiciosProfesionales.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("serviciosProfesionales", documentoServiciosProfesionales);
                }
            }

            if (datosActividadAnualAnterior.getEnajenacionBienes() != null)
            {
                Document documentoEnajenacionBienes = new DeclaracionesDocumentoEnajenacionBienes(datosActividadAnualAnterior.getEnajenacionBienes(), strTipoDeclaracion).obtenerDocumentoEnajenacionBienes();
                if (documentoEnajenacionBienes.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("enajenacionBienes", documentoEnajenacionBienes);
                }
            }

            if (datosActividadAnualAnterior.getOtrosIngresos() != null)
            {
                Document documentoOtrosIngresos = new DeclaracionesDocumentoOtrosIngresos(datosActividadAnualAnterior.getOtrosIngresos()).obtenerDocumentoOtrosIngresos();
                if (documentoOtrosIngresos.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("otrosIngresos", documentoOtrosIngresos);
                }
            }

            if (datosActividadAnualAnterior.getIngresoNetoAnualDeclarante() != null)
            {
                Document documentoIngresoNetoAnualDeclarante = new DeclaracionesDocumentoMonto(datosActividadAnualAnterior.getIngresoNetoAnualDeclarante()).obtenerDocumentoMonto();
                if (documentoIngresoNetoAnualDeclarante.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("ingresoNetoAnualDeclarante", documentoIngresoNetoAnualDeclarante);
                }
            }

            if (datosActividadAnualAnterior.getIngresoNetoAnualParejaDependiente() != null)
            {
                Document documentoIngresoNetoAnualParejaDependiente = new DeclaracionesDocumentoMonto(datosActividadAnualAnterior.getIngresoNetoAnualParejaDependiente()).obtenerDocumentoMonto();
                if (documentoIngresoNetoAnualParejaDependiente.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("ingresoNetoAnualParejaDependiente", documentoIngresoNetoAnualParejaDependiente);
                }
            }

            if (datosActividadAnualAnterior.getTotalIngresosNetosAnuales() != null)
            {
                Document documenTototalIngresosNetosAnuales = new DeclaracionesDocumentoMonto(datosActividadAnualAnterior.getTotalIngresosNetosAnuales()).obtenerDocumentoMonto();
                if (documenTototalIngresosNetosAnuales.isEmpty() == false)
                {
                    documentoActividadAnualAnterior.append("totalIngresosNetosAnuales", documenTototalIngresosNetosAnuales);
                }
            }

            if (datosActividadAnualAnterior.getAclaracionesObservaciones() != null)
            {
                documentoActividadAnualAnterior.append("aclaracionesObservaciones", datosActividadAnualAnterior.getAclaracionesObservaciones());
            }

        }

        return documentoActividadAnualAnterior;

    }

}
