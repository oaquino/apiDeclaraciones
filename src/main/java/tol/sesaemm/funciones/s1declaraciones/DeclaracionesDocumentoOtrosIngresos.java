package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesOtrosIngresos;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoOtrosIngresos
{

    private DeclaracionesOtrosIngresos otrosIngresos;
    private Document documentoOtrosIngreos;

    public DeclaracionesDocumentoOtrosIngresos(DeclaracionesOtrosIngresos otrosIngresos)
    {
        this.otrosIngresos = otrosIngresos;
    }

    public Document obtenerDocumentoOtrosIngresos()
    {

        documentoOtrosIngreos = new Document();
        List<Document> arregloListaIngresos;

        if (otrosIngresos.getRemuneracionTotal() != null)
        {
            Document documentoRemuneracionTotal = new DeclaracionesDocumentoMonto(otrosIngresos.getRemuneracionTotal()).obtenerDocumentoMonto();
            if (documentoRemuneracionTotal.isEmpty() == false)
            {
                documentoOtrosIngreos.append("remuneracionTotal", documentoRemuneracionTotal);
            }
        }

        if (otrosIngresos.getIngresos() != null)
        {
            arregloListaIngresos = new ArrayList<>();
            for (int i = 0; i < otrosIngresos.getIngresos().size(); i++)
            {
                Document documentoIngreso = new Document();

                if (otrosIngresos.getIngresos().get(i).getRemuneracion() != null)
                {
                    Document documentoRemuneracion = new DeclaracionesDocumentoMonto(otrosIngresos.getIngresos().get(i).getRemuneracion()).obtenerDocumentoMonto();
                    if (documentoRemuneracion.isEmpty() == false)
                    {
                        documentoIngreso.append("remuneracion", documentoRemuneracion);
                    }
                }

                if (otrosIngresos.getIngresos().get(i).getTipoIngreso() != null)
                {
                    documentoIngreso.append("tipoIngreso", otrosIngresos.getIngresos().get(i).getTipoIngreso());
                }

                if (documentoIngreso.isEmpty() == false)
                {
                    arregloListaIngresos.add(documentoIngreso);
                }
            }
            documentoOtrosIngreos.append("ingresos", arregloListaIngresos);
        }

        return documentoOtrosIngreos;

    }

}
