package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesBeneficiosPrivados;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoBeneficiosPrivados
{

    private DeclaracionesBeneficiosPrivados beneficiosPrivados;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoBeneficiosPrivados;

    public DeclaracionesDocumentoBeneficiosPrivados(DeclaracionesBeneficiosPrivados beneficiosPrivados, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.beneficiosPrivados = beneficiosPrivados;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoBeneficiosPrivados()
    {

        documentoBeneficiosPrivados = new Document();

        if (beneficiosPrivados != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (beneficiosPrivados.getNinguno() != null)
                {
                    documentoBeneficiosPrivados.append("ninguno", beneficiosPrivados.getNinguno());
                }

                if (beneficiosPrivados.getBeneficio() != null)
                {
                    ArrayList listaBeneficio = new DeclaracionesListaDocumentosBeneficiosPrivados(beneficiosPrivados.getBeneficio(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaBeneficiosPrivados();
                    if (listaBeneficio.isEmpty() == false)
                    {
                        documentoBeneficiosPrivados.append("beneficio", listaBeneficio);
                    }
                }

            } else
            {

                if (beneficiosPrivados.getNinguno() != null)
                {
                    documentoBeneficiosPrivados.append("ninguno", beneficiosPrivados.getNinguno());
                }

                if (beneficiosPrivados.getBeneficio() != null)
                {
                    ArrayList listaBeneficio = new DeclaracionesListaDocumentosBeneficiosPrivados(beneficiosPrivados.getBeneficio(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaBeneficiosPrivados();
                    if (listaBeneficio.isEmpty() == false)
                    {
                        documentoBeneficiosPrivados.append("beneficio", listaBeneficio);
                    }
                }

                if (beneficiosPrivados.getAclaracionesObservaciones() != null)
                {
                    documentoBeneficiosPrivados.append("aclaracionesObservaciones", beneficiosPrivados.getAclaracionesObservaciones());
                }

            }
        }

        return documentoBeneficiosPrivados;

    }

}
