package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesPrestamoComodato;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoPrestamoOComodato
{

    private DeclaracionesPrestamoComodato pestamoOComodato;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoPretamoOComodato;

    public DeclaracionesDocumentoPrestamoOComodato(DeclaracionesPrestamoComodato pestamoOComodato, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.pestamoOComodato = pestamoOComodato;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoPrestamoOComodato()
    {

        documentoPretamoOComodato = new Document();

        if (pestamoOComodato != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (pestamoOComodato.getNinguno() != null)
                {
                    documentoPretamoOComodato.append("ninguno", pestamoOComodato.getNinguno());
                }

                if (pestamoOComodato.getPrestamo() != null)
                {
                    ArrayList<Document> listaPrestamo = new DeclaracionesListaDocumentosPrestamoOComodato(pestamoOComodato.getPrestamo(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaPrestamoOComodato();
                    if (listaPrestamo.isEmpty() == false)
                    {
                        documentoPretamoOComodato.append("prestamo", listaPrestamo);
                    }
                }

            } else
            {

                if (pestamoOComodato.getNinguno() != null)
                {
                    documentoPretamoOComodato.append("ninguno", pestamoOComodato.getNinguno());
                }

                if (pestamoOComodato.getPrestamo() != null)
                {
                    ArrayList<Document> listaPrestamo = new DeclaracionesListaDocumentosPrestamoOComodato(pestamoOComodato.getPrestamo(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaPrestamoOComodato();
                    if (listaPrestamo.isEmpty() == false)
                    {
                        documentoPretamoOComodato.append("prestamo", listaPrestamo);
                    }
                }

                if (pestamoOComodato.getAclaracionesObservaciones() != null)
                {
                    documentoPretamoOComodato.append("aclaracionesObservaciones", pestamoOComodato.getAclaracionesObservaciones());
                }

            }
        }

        return documentoPretamoOComodato;

    }

}
