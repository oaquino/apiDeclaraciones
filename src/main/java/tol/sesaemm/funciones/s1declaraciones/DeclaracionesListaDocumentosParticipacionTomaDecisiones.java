package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesParticipacionParticipacionTomaDecisiones;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosParticipacionTomaDecisiones
{

    private ArrayList<DeclaracionesParticipacionParticipacionTomaDecisiones> participacion;
    private boolean bolMostrarDatosPublicosYPrivados;
    private ArrayList<Document> arregloListaParticipacionTomaDecisiones;

    public DeclaracionesListaDocumentosParticipacionTomaDecisiones(ArrayList<DeclaracionesParticipacionParticipacionTomaDecisiones> participacion, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.participacion = participacion;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public ArrayList<Document> obtenerArregloDocumentosListaParticipacionTomaDecisiones()
    {

        arregloListaParticipacionTomaDecisiones = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            for (int j = 0; j < participacion.size(); j++)
            {
                Document documentoParticipacion = new Document();
                if (participacion.get(j).getTipoRelacion() != null)
                {
                    if (participacion.get(j).getTipoRelacion().equals("DECLARANTE"))
                    {

                        if (participacion.get(j).getTipoOperacion() != null)
                        {
                            documentoParticipacion.append("tipoOperacion", participacion.get(j).getTipoOperacion());
                        }

                        documentoParticipacion.append("tipoRelacion", participacion.get(j).getTipoRelacion());

                        if (participacion.get(j).getTipoInstitucion() != null)
                        {
                            Document documentoTipoInstitucion = new Document();

                            if (participacion.get(j).getTipoInstitucion().getClave() != null)
                            {
                                documentoTipoInstitucion.append("clave", participacion.get(j).getTipoInstitucion().getClave());
                            }

                            if (participacion.get(j).getTipoInstitucion().getValor() != null)
                            {
                                documentoTipoInstitucion.append("valor", participacion.get(j).getTipoInstitucion().getValor());
                            }

                            if (documentoTipoInstitucion.isEmpty() == false)
                            {
                                documentoParticipacion.append("tipoInstitucion", documentoTipoInstitucion);
                            }
                        }

                        if (participacion.get(j).getPuestoRol() != null)
                        {
                            documentoParticipacion.append("puestoRol", participacion.get(j).getPuestoRol());
                        }

                        if (participacion.get(j).getFechaInicioParticipacion() != null)
                        {
                            documentoParticipacion.append("fechaInicioParticipacion", participacion.get(j).getFechaInicioParticipacion());
                        }

                        if (participacion.get(j).getRecibeRemuneracion() != null)
                        {
                            documentoParticipacion.append("recibeRemuneracion", participacion.get(j).getRecibeRemuneracion());
                        }

                        if (participacion.get(j).getMontoMensual() != null)
                        {
                            Document documentoMontoMensual = new Document();

                            if (participacion.get(j).getMontoMensual().getValor() != null)
                            {
                                documentoMontoMensual.append("valor", participacion.get(j).getMontoMensual().getValor());
                            }

                            if (participacion.get(j).getMontoMensual().getMoneda() != null)
                            {
                                documentoMontoMensual.append("moneda", participacion.get(j).getMontoMensual().getMoneda());
                            }

                            if (documentoMontoMensual.isEmpty() == false)
                            {
                                documentoParticipacion.append("montoMensual", documentoMontoMensual);
                            }
                        }

                        if (participacion.get(j).getUbicacion() != null)
                        {
                            Document documentoUbicacion = new Document();

                            if (participacion.get(j).getUbicacion().getPais() != null)
                            {
                                documentoUbicacion.append("pais", participacion.get(j).getUbicacion().getPais());
                            }

                            if (participacion.get(j).getUbicacion().getEntidadFederativa() != null)
                            {
                                Document documentoEntidadFederativa = new Document();

                                if (participacion.get(j).getUbicacion().getEntidadFederativa().getClave() != null)
                                {
                                    documentoEntidadFederativa.append("clave", participacion.get(j).getUbicacion().getEntidadFederativa().getClave());
                                }

                                if (participacion.get(j).getUbicacion().getEntidadFederativa().getValor() != null)
                                {
                                    documentoEntidadFederativa.append("valor", participacion.get(j).getUbicacion().getEntidadFederativa().getValor());
                                }

                                if (documentoEntidadFederativa.isEmpty() == false)
                                {
                                    documentoUbicacion.append("entidadFederativa", documentoEntidadFederativa);
                                }
                            }

                            if (documentoUbicacion.isEmpty() == false)
                            {
                                documentoParticipacion.append("ubicacion", documentoUbicacion);
                            }
                        }

                    }
                }
                if (documentoParticipacion.isEmpty() == false)
                {
                    arregloListaParticipacionTomaDecisiones.add(documentoParticipacion);
                }
            }
        } else
        {

            for (int j = 0; j < participacion.size(); j++)
            {
                Document documentoParticipacion = new Document();

                if (participacion.get(j).getTipoOperacion() != null)
                {
                    documentoParticipacion.append("tipoOperacion", participacion.get(j).getTipoOperacion());
                }

                if (participacion.get(j).getTipoRelacion() != null)
                {
                    documentoParticipacion.append("tipoRelacion", participacion.get(j).getTipoRelacion());
                }

                if (participacion.get(j).getTipoInstitucion() != null)
                {
                    Document documentoTipoInstitucion = new Document();

                    if (participacion.get(j).getTipoInstitucion().getClave() != null)
                    {
                        documentoTipoInstitucion.append("clave", participacion.get(j).getTipoInstitucion().getClave());
                    }

                    if (participacion.get(j).getTipoInstitucion().getValor() != null)
                    {
                        documentoTipoInstitucion.append("valor", participacion.get(j).getTipoInstitucion().getValor());
                    }

                    if (documentoTipoInstitucion.isEmpty() == false)
                    {
                        documentoParticipacion.append("tipoInstitucion", documentoTipoInstitucion);
                    }
                }

                if (participacion.get(j).getNombreInstitucion() != null)
                {
                    documentoParticipacion.append("nombreInstitucion", participacion.get(j).getNombreInstitucion());
                }

                if (participacion.get(j).getRfc() != null)
                {
                    documentoParticipacion.append("rfc", participacion.get(j).getRfc());
                }

                if (participacion.get(j).getPuestoRol() != null)
                {
                    documentoParticipacion.append("puestoRol", participacion.get(j).getPuestoRol());
                }

                if (participacion.get(j).getFechaInicioParticipacion() != null)
                {
                    documentoParticipacion.append("fechaInicioParticipacion", participacion.get(j).getFechaInicioParticipacion());
                }

                if (participacion.get(j).getRecibeRemuneracion() != null)
                {
                    documentoParticipacion.append("recibeRemuneracion", participacion.get(j).getRecibeRemuneracion());
                }

                if (participacion.get(j).getMontoMensual() != null)
                {
                    Document documentoMontoMensual = new Document();

                    if (participacion.get(j).getMontoMensual().getValor() != null)
                    {
                        documentoMontoMensual.append("valor", participacion.get(j).getMontoMensual().getValor());
                    }

                    if (participacion.get(j).getMontoMensual().getMoneda() != null)
                    {
                        documentoMontoMensual.append("moneda", participacion.get(j).getMontoMensual().getMoneda());
                    }

                    if (documentoMontoMensual.isEmpty() == false)
                    {
                        documentoParticipacion.append("montoMensual", documentoMontoMensual);
                    }
                }

                if (participacion.get(j).getUbicacion() != null)
                {
                    Document documentoUbicacion = new Document();

                    if (participacion.get(j).getUbicacion().getPais() != null)
                    {
                        documentoUbicacion.append("pais", participacion.get(j).getUbicacion().getPais());
                    }

                    if (participacion.get(j).getUbicacion().getEntidadFederativa() != null)
                    {
                        Document documentoEntidadFederativa = new Document();

                        if (participacion.get(j).getUbicacion().getEntidadFederativa().getClave() != null)
                        {
                            documentoEntidadFederativa.append("clave", participacion.get(j).getUbicacion().getEntidadFederativa().getClave());
                        }

                        if (participacion.get(j).getUbicacion().getEntidadFederativa().getValor() != null)
                        {
                            documentoEntidadFederativa.append("valor", participacion.get(j).getUbicacion().getEntidadFederativa().getValor());
                        }

                        if (documentoEntidadFederativa.isEmpty() == false)
                        {
                            documentoUbicacion.append("entidadFederativa", documentoEntidadFederativa);
                        }
                    }

                    if (documentoUbicacion.isEmpty() == false)
                    {
                        documentoParticipacion.append("ubicacion", documentoUbicacion);
                    }
                }

                if (documentoParticipacion.isEmpty() == false)
                {
                    arregloListaParticipacionTomaDecisiones.add(documentoParticipacion);
                }
            }

        }

        return arregloListaParticipacionTomaDecisiones;

    }

}
