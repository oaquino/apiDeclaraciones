/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesResults;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoMetadata
{

    private DeclaracionesResults declaracion;

    public DeclaracionesDocumentoMetadata(DeclaracionesResults declaracion)
    {
        this.declaracion = declaracion;
    }

    public Document obtenerMetadata()
    {

        Document documentoMetadata = new Document();

        documentoMetadata.append("actualizacion", declaracion.getMetadata().getActualizacion())
                .append("institucion", declaracion.getMetadata().getInstitucion())
                .append("tipo", declaracion.getMetadata().getTipo()
                );

        return documentoMetadata;
    }

}
