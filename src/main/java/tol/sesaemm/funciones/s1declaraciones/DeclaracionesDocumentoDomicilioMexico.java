package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesDomicilioMexico;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoDomicilioMexico
{

    private DeclaracionesDomicilioMexico domicilioMexico;
    private Document documentoDomicilioMexico;

    public DeclaracionesDocumentoDomicilioMexico(DeclaracionesDomicilioMexico domicilioMexico)
    {
        this.domicilioMexico = domicilioMexico;
    }

    public Document obtenerDocumentoDomicilioMexico()
    {

        documentoDomicilioMexico = new Document();

        if (domicilioMexico.getCalle() != null)
        {
            documentoDomicilioMexico.append("calle", domicilioMexico.getCalle());
        }

        if (domicilioMexico.getNumeroExterior() != null)
        {
            documentoDomicilioMexico.append("numeroExterior", domicilioMexico.getNumeroExterior());
        }

        if (domicilioMexico.getNumeroInterior() != null)
        {
            documentoDomicilioMexico.append("numeroInterior", domicilioMexico.getNumeroInterior());
        }

        if (domicilioMexico.getColoniaLocalidad() != null)
        {
            documentoDomicilioMexico.append("coloniaLocalidad", domicilioMexico.getColoniaLocalidad());
        }

        if (domicilioMexico.getMunicipioAlcaldia() != null)
        {
            Document documentoMunicipioAlcaldia = new Document();
            if (domicilioMexico.getMunicipioAlcaldia().getClave() != null)
            {
                documentoMunicipioAlcaldia.append("clave", domicilioMexico.getMunicipioAlcaldia().getClave());
            }
            if (domicilioMexico.getMunicipioAlcaldia().getValor() != null)
            {
                documentoMunicipioAlcaldia.append("valor", domicilioMexico.getMunicipioAlcaldia().getValor());
            }
            if (documentoMunicipioAlcaldia.isEmpty() == false)
            {
                documentoDomicilioMexico.append("municipioAlcaldia", documentoMunicipioAlcaldia);
            }
        }

        if (domicilioMexico.getEntidadFederativa() != null)
        {
            Document documentoEntidadFederativa = new Document();
            if (domicilioMexico.getEntidadFederativa().getClave() != null)
            {
                documentoEntidadFederativa.append("clave", domicilioMexico.getEntidadFederativa().getClave());
            }
            if (domicilioMexico.getEntidadFederativa().getValor() != null)
            {
                documentoEntidadFederativa.append("valor", domicilioMexico.getEntidadFederativa().getValor());
            }
            if (documentoEntidadFederativa.isEmpty() == false)
            {
                documentoDomicilioMexico.append("entidadFederativa", documentoEntidadFederativa);
            }
        }

        if (domicilioMexico.getCodigoPostal() != null)
        {
            documentoDomicilioMexico.append("codigoPostal", domicilioMexico.getCodigoPostal());
        }

        return documentoDomicilioMexico;

    }
}
