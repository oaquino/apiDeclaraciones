package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesDatosDependientesEconomicos;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoDatosDependienteEconomico
{

    private DeclaracionesDatosDependientesEconomicos datosDependientesEconomicos;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoDependienteEconomico;

    public DeclaracionesDocumentoDatosDependienteEconomico(DeclaracionesDatosDependientesEconomicos datosDependientesEconomicos, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosDependientesEconomicos = datosDependientesEconomicos;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoDatosDependienteEconomico()
    {

        documentoDependienteEconomico = new Document();

        if (datosDependientesEconomicos != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

            } else
            {

                if (datosDependientesEconomicos.getNinguno() != null)
                {
                    documentoDependienteEconomico.append("ninguno", datosDependientesEconomicos.getNinguno());
                }

                if (datosDependientesEconomicos.getDependienteEconomico() != null)
                {
                    ArrayList<Document> arregloDependientesEconomicos = new DeclaracionesListaDocumentosDependientesEconomicos(datosDependientesEconomicos.getDependienteEconomico()).obtenerArregloDocumentosListaDependientesEconomicos();
                    if (arregloDependientesEconomicos.isEmpty() == false)
                    {
                        documentoDependienteEconomico.append("dependienteEconomico", arregloDependientesEconomicos);
                    }
                }

                if (datosDependientesEconomicos.getAclaracionesObservaciones() != null)
                {
                    documentoDependienteEconomico.append("aclaracionesObservaciones", datosDependientesEconomicos.getAclaracionesObservaciones());
                }

            }
        }

        return documentoDependienteEconomico;
    }

}
