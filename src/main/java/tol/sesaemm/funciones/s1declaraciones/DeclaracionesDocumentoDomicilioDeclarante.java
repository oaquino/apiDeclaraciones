package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesDomicilioDeclarante;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoDomicilioDeclarante
{

    private DeclaracionesDomicilioDeclarante domicilioDeclarante;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoDomicilioDeclarante;

    public DeclaracionesDocumentoDomicilioDeclarante(DeclaracionesDomicilioDeclarante domicilioDeclarante, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.domicilioDeclarante = domicilioDeclarante;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoDomicilioDeclarante()
    {

        documentoDomicilioDeclarante = new Document();

        if (domicilioDeclarante != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

            } else
            {

                if (domicilioDeclarante.getDomicilioMexico() != null)
                {
                    Document documentoDomicilioMexico = new DeclaracionesDocumentoDomicilioMexico(domicilioDeclarante.getDomicilioMexico()).obtenerDocumentoDomicilioMexico();
                    if (documentoDomicilioMexico.isEmpty() == false)
                    {
                        documentoDomicilioDeclarante.append("domicilioMexico", documentoDomicilioMexico);
                    }
                }

                if (domicilioDeclarante.getDomicilioExtranjero() != null)
                {
                    Document documentoDomicilioExtranjero = new DeclaracionesDocumentoDomicilioExtranjero(domicilioDeclarante.getDomicilioExtranjero()).obtenerDocumentoDomicilioExtranjero();
                    if (documentoDomicilioExtranjero.isEmpty() == false)
                    {
                        documentoDomicilioDeclarante.append("domicilioExtranjero", documentoDomicilioExtranjero);
                    }
                }

            }
        }

        return documentoDomicilioDeclarante;

    }

}
