package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesAdeudosPasivos;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoAdeudos
{

    private DeclaracionesAdeudosPasivos adeudos;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoAdeudos;
    private String strTipoDeclaracion;

    public DeclaracionesDocumentoAdeudos(DeclaracionesAdeudosPasivos adeudos, String strTipoDeclaracion, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.adeudos = adeudos;
        this.strTipoDeclaracion = strTipoDeclaracion;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoAdeudos()
    {

        documentoAdeudos = new Document();

        if (adeudos != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (adeudos.getNinguno() != null)
                {
                    documentoAdeudos.append("ninguno", adeudos.getNinguno());
                }

                if (adeudos.getAdeudo() != null)
                {
                    ArrayList<Document> listaAdeudo = new DeclaracionesListaDocumentosAdeudos(adeudos.getAdeudo(), this.strTipoDeclaracion, this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaAdeudos();
                    if (listaAdeudo.isEmpty() == false)
                    {
                        documentoAdeudos.append("adeudo", listaAdeudo);
                    }
                }

            } else
            {

                if (adeudos.getNinguno() != null)
                {
                    documentoAdeudos.append("ninguno", adeudos.getNinguno());
                }

                if (adeudos.getAdeudo() != null)
                {
                    ArrayList<Document> listaAdeudo = new DeclaracionesListaDocumentosAdeudos(adeudos.getAdeudo(), this.strTipoDeclaracion, this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaAdeudos();
                    if (listaAdeudo.isEmpty() == false)
                    {
                        documentoAdeudos.append("adeudo", listaAdeudo);
                    }
                }

                if (adeudos.getAclaracionesObservaciones() != null)
                {
                    documentoAdeudos.append("aclaracionesObservaciones", adeudos.getAclaracionesObservaciones());
                }

            }
        }

        return documentoAdeudos;

    }

}
