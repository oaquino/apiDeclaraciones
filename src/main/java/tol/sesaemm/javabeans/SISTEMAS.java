/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class SISTEMAS
  {
    private SISTEMA sistema;
    private NIVEL_ACCESO nivel_acceso;

    public SISTEMA getSistema()
      {
        return sistema;
      }

    public void setSistema(SISTEMA sistema)
      {
        this.sistema = sistema;
      }

    public NIVEL_ACCESO getNivel_acceso()
      {
        return nivel_acceso;
      }

    public void setNivel_acceso(NIVEL_ACCESO nivel_acceso)
      {
        this.nivel_acceso = nivel_acceso;
      }
    
    
  }
