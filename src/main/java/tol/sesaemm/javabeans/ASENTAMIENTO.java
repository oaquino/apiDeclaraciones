/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class ASENTAMIENTO
  {

    public String cve_asenta;
    public String nom_clave;
    public String nom_asenta;
    public String nom_tipo_asenta;
    public String nom_agem;
    public String nom_agee;
    public String nom_ciudad;
    public String cve_agee;
    public String cve_oficina;
    public String cve_tipo_asenta;
    public String cve_agem;
    public String nom_zona;
    public String cve_ciudad;

    public String getCve_asenta()
      {
        return cve_asenta;
      }

    public void setCve_asenta(String cve_asenta)
      {
        this.cve_asenta = cve_asenta;
      }

    public String getNom_clave()
      {
        return nom_clave;
      }

    public void setNom_clave(String nom_clave)
      {
        this.nom_clave = nom_clave;
      }

    public String getNom_asenta()
      {
        return nom_asenta;
      }

    public void setNom_asenta(String nom_asenta)
      {
        this.nom_asenta = nom_asenta;
      }

    public String getNom_tipo_asenta()
      {
        return nom_tipo_asenta;
      }

    public void setNom_tipo_asenta(String nom_tipo_asenta)
      {
        this.nom_tipo_asenta = nom_tipo_asenta;
      }

    public String getNom_agem()
      {
        return nom_agem;
      }

    public void setNom_agem(String nom_agem)
      {
        this.nom_agem = nom_agem;
      }

    public String getNom_agee()
      {
        return nom_agee;
      }

    public void setNom_agee(String nom_agee)
      {
        this.nom_agee = nom_agee;
      }

    public String getNom_ciudad()
      {
        return nom_ciudad;
      }

    public void setNom_ciudad(String nom_ciudad)
      {
        this.nom_ciudad = nom_ciudad;
      }

    public String getCve_agee()
      {
        return cve_agee;
      }

    public void setCve_agee(String cve_agee)
      {
        this.cve_agee = cve_agee;
      }

    public String getCve_oficina()
      {
        return cve_oficina;
      }

    public void setCve_oficina(String cve_oficina)
      {
        this.cve_oficina = cve_oficina;
      }

    public String getCve_tipo_asenta()
      {
        return cve_tipo_asenta;
      }

    public void setCve_tipo_asenta(String cve_tipo_asenta)
      {
        this.cve_tipo_asenta = cve_tipo_asenta;
      }

    public String getCve_agem()
      {
        return cve_agem;
      }

    public void setCve_agem(String cve_agem)
      {
        this.cve_agem = cve_agem;
      }

    public String getNom_zona()
      {
        return nom_zona;
      }

    public void setNom_zona(String nom_zona)
      {
        this.nom_zona = nom_zona;
      }

    public String getCve_ciudad()
      {
        return cve_ciudad;
      }

    public void setCve_ciudad(String cve_ciudad)
      {
        this.cve_ciudad = cve_ciudad;
      }

  }
