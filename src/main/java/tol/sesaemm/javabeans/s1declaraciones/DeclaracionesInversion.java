/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesInversion implements Serializable{
    
    private String tipoOperacion;
    private DeclaracionesTipoInversion tipoInversion;
    private DeclaracionesSubTipoInversion subTipoInversion;
    private ArrayList<DeclaracionesTitularBien> titular;
    private ArrayList<DeclaracionesTercero> tercero;
    private String numeroCuentaContrato;
    private DeclaracionesLocalizacionInversion localizacionInversion;
    private DeclaracionesMonto saldoSituacionActual;
    private DeclaracionesMonto saldoDiciembreAnterior;
    private DeclaracionesMonto saldoFechaConclusion;
    private Integer porcentajeIncrementoDecremento;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public DeclaracionesTipoInversion getTipoInversion() {
        return tipoInversion;
    }

    public void setTipoInversion(DeclaracionesTipoInversion tipoInversion) {
        this.tipoInversion = tipoInversion;
    }

    public DeclaracionesSubTipoInversion getSubTipoInversion() {
        return subTipoInversion;
    }

    public void setSubTipoInversion(DeclaracionesSubTipoInversion subTipoInversion) {
        this.subTipoInversion = subTipoInversion;
    }

    public ArrayList<DeclaracionesTitularBien> getTitular() {
        return titular;
    }

    public void setTitular(ArrayList<DeclaracionesTitularBien> titular) {
        this.titular = titular;
    }

    public ArrayList<DeclaracionesTercero> getTercero() {
        return tercero;
    }

    public void setTercero(ArrayList<DeclaracionesTercero> tercero) {
        this.tercero = tercero;
    }

    public String getNumeroCuentaContrato() {
        return numeroCuentaContrato;
    }

    public void setNumeroCuentaContrato(String numeroCuentaContrato) {
        this.numeroCuentaContrato = numeroCuentaContrato;
    }

    public DeclaracionesLocalizacionInversion getLocalizacionInversion() {
        return localizacionInversion;
    }

    public void setLocalizacionInversion(DeclaracionesLocalizacionInversion localizacionInversion) {
        this.localizacionInversion = localizacionInversion;
    }

    public DeclaracionesMonto getSaldoSituacionActual() {
        return saldoSituacionActual;
    }

    public void setSaldoSituacionActual(DeclaracionesMonto saldoSituacionActual) {
        this.saldoSituacionActual = saldoSituacionActual;
    }

    public DeclaracionesMonto getSaldoDiciembreAnterior() {
        return saldoDiciembreAnterior;
    }

    public void setSaldoDiciembreAnterior(DeclaracionesMonto saldoDiciembreAnterior) {
        this.saldoDiciembreAnterior = saldoDiciembreAnterior;
    }

    public DeclaracionesMonto getSaldoFechaConclusion() {
        return saldoFechaConclusion;
    }

    public void setSaldoFechaConclusion(DeclaracionesMonto saldoFechaConclusion) {
        this.saldoFechaConclusion = saldoFechaConclusion;
    }

    public Integer getPorcentajeIncrementoDecremento() {
        return porcentajeIncrementoDecremento;
    }

    public void setPorcentajeIncrementoDecremento(Integer porcentajeIncrementoDecremento) {
        this.porcentajeIncrementoDecremento = porcentajeIncrementoDecremento;
    }
}
