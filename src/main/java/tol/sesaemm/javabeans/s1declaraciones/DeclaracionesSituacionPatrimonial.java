/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesSituacionPatrimonial implements Serializable{

    private DeclaracionesDatosGenerales datosGenerales;
    private DeclaracionesDomicilioDeclarante domicilioDeclarante;
    private DeclaracionesDatosCurricularesDeclarante datosCurricularesDeclarante;
    private DeclaracionesDatosEmpleoCargoComision datosEmpleoCargoComision;
    private DeclaracionesExperienciaLaboral experienciaLaboral;
    private DeclaracionesDatosPareja datosPareja;
    private DeclaracionesDatosDependientesEconomicos datosDependienteEconomico;
    private DeclaracionesIngresos ingresos;
    private DeclaracionesActividadAnualAnterior actividadAnualAnterior;
    private DeclaracionesBienesInmuebles bienesInmuebles;
    private DeclaracionesVehiculos vehiculos;
    private DeclaracionesBienesMuebles bienesMuebles;
    private DeclaracionesInversionesCuentasValores inversiones;
    private DeclaracionesAdeudosPasivos adeudos;
    private DeclaracionesPrestamoComodato prestamoOComodato;

    public DeclaracionesDatosGenerales getDatosGenerales() {
        return datosGenerales;
    }

    public void setDatosGenerales(DeclaracionesDatosGenerales datosGenerales) {
        this.datosGenerales = datosGenerales;
    }

    public DeclaracionesDomicilioDeclarante getDomicilioDeclarante() {
        return domicilioDeclarante;
    }

    public void setDomicilioDeclarante(DeclaracionesDomicilioDeclarante domicilioDeclarante) {
        this.domicilioDeclarante = domicilioDeclarante;
    }

    public DeclaracionesDatosCurricularesDeclarante getDatosCurricularesDeclarante() {
        return datosCurricularesDeclarante;
    }

    public void setDatosCurricularesDeclarante(DeclaracionesDatosCurricularesDeclarante datosCurricularesDeclarante) {
        this.datosCurricularesDeclarante = datosCurricularesDeclarante;
    }

    public DeclaracionesDatosEmpleoCargoComision getDatosEmpleoCargoComision() {
        return datosEmpleoCargoComision;
    }

    public void setDatosEmpleoCargoComision(DeclaracionesDatosEmpleoCargoComision datosEmpleoCargoComision) {
        this.datosEmpleoCargoComision = datosEmpleoCargoComision;
    }

    public DeclaracionesExperienciaLaboral getExperienciaLaboral() {
        return experienciaLaboral;
    }

    public void setExperienciaLaboral(DeclaracionesExperienciaLaboral experienciaLaboral) {
        this.experienciaLaboral = experienciaLaboral;
    }

    public DeclaracionesDatosPareja getDatosPareja() {
        return datosPareja;
    }

    public void setDatosPareja(DeclaracionesDatosPareja datosPareja) {
        this.datosPareja = datosPareja;
    }

    public DeclaracionesDatosDependientesEconomicos getDatosDependienteEconomico() {
        return datosDependienteEconomico;
    }

    public void setDatosDependienteEconomico(DeclaracionesDatosDependientesEconomicos datosDependienteEconomico) {
        this.datosDependienteEconomico = datosDependienteEconomico;
    }

    public DeclaracionesIngresos getIngresos() {
        return ingresos;
    }

    public void setIngresos(DeclaracionesIngresos ingresos) {
        this.ingresos = ingresos;
    }

    public DeclaracionesActividadAnualAnterior getActividadAnualAnterior() {
        return actividadAnualAnterior;
    }

    public void setActividadAnualAnterior(DeclaracionesActividadAnualAnterior actividadAnualAnterior) {
        this.actividadAnualAnterior = actividadAnualAnterior;
    }

    public DeclaracionesBienesInmuebles getBienesInmuebles() {
        return bienesInmuebles;
    }

    public void setBienesInmuebles(DeclaracionesBienesInmuebles bienesInmuebles) {
        this.bienesInmuebles = bienesInmuebles;
    }

    public DeclaracionesVehiculos getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(DeclaracionesVehiculos vehiculos) {
        this.vehiculos = vehiculos;
    }

    public DeclaracionesBienesMuebles getBienesMuebles() {
        return bienesMuebles;
    }

    public void setBienesMuebles(DeclaracionesBienesMuebles bienesMuebles) {
        this.bienesMuebles = bienesMuebles;
    }

    public DeclaracionesInversionesCuentasValores getInversiones() {
        return inversiones;
    }

    public void setInversiones(DeclaracionesInversionesCuentasValores inversiones) {
        this.inversiones = inversiones;
    }

    public DeclaracionesAdeudosPasivos getAdeudos() {
        return adeudos;
    }

    public void setAdeudos(DeclaracionesAdeudosPasivos adeudos) {
        this.adeudos = adeudos;
    }

    public DeclaracionesPrestamoComodato getPrestamoOComodato() {
        return prestamoOComodato;
    }

    public void setPrestamoOComodato(DeclaracionesPrestamoComodato prestamoOComodato) {
        this.prestamoOComodato = prestamoOComodato;
    }
}
