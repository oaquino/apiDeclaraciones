/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesPrestamo implements Serializable{
    
    private String tipoOperacion;
    private DeclaracionesTipoBienPrestamo tipoBien;
    private DeclaracionesDuenoTitular duenoTitular;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public DeclaracionesTipoBienPrestamo getTipoBien() {
        return tipoBien;
    }

    public void setTipoBien(DeclaracionesTipoBienPrestamo tipoBien) {
        this.tipoBien = tipoBien;
    }

    public DeclaracionesDuenoTitular getDuenoTitular() {
        return duenoTitular;
    }

    public void setDuenoTitular(DeclaracionesDuenoTitular duenoTitular) {
        this.duenoTitular = duenoTitular;
    }
}
