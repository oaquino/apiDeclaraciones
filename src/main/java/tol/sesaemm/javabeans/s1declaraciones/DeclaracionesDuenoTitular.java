/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesDuenoTitular implements Serializable{
    
    private String tipoDuenoTitular;
    private String nombreTitular;
    private String rfc;
    private String relacionConTitular;

    public String getTipoDuenoTitular() {
        return tipoDuenoTitular;
    }

    public void setTipoDuenoTitular(String tipoDuenoTitular) {
        this.tipoDuenoTitular = tipoDuenoTitular;
    }

    public String getNombreTitular() {
        return nombreTitular;
    }

    public void setNombreTitular(String nombreTitular) {
        this.nombreTitular = nombreTitular;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getRelacionConTitular() {
        return relacionConTitular;
    }

    public void setRelacionConTitular(String relacionConTitular) {
        this.relacionConTitular = relacionConTitular;
    }
}