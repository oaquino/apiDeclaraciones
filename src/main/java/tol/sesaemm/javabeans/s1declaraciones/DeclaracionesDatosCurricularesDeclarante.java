/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesDatosCurricularesDeclarante implements Serializable{
    
    private ArrayList<DeclaracionesEscolaridad> escolaridad;
    private String aclaracionesObservaciones;

    public ArrayList<DeclaracionesEscolaridad> getEscolaridad() {
        return escolaridad;
    }

    public void setEscolaridad(ArrayList<DeclaracionesEscolaridad> escolaridad) {
        this.escolaridad = escolaridad;
    }

    public String getAclaracionesObservaciones() {
        return aclaracionesObservaciones;
    }

    public void setAclaracionesObservaciones(String aclaracionesObservaciones) {
        this.aclaracionesObservaciones = aclaracionesObservaciones;
    }
}
