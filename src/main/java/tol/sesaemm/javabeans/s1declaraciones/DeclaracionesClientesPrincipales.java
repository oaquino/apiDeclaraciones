/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesClientesPrincipales implements Serializable{
    
    private Boolean ninguno;
    private ArrayList<DeclaracionesCliente> cliente;
    private String aclaracionesObservaciones;

    public Boolean getNinguno() {
        return ninguno;
    }

    public void setNinguno(Boolean ninguno) {
        this.ninguno = ninguno;
    }

    public ArrayList<DeclaracionesCliente> getCliente() {
        return cliente;
    }

    public void setCliente(ArrayList<DeclaracionesCliente> cliente) {
        this.cliente = cliente;
    }

    public String getAclaracionesObservaciones() {
        return aclaracionesObservaciones;
    }

    public void setAclaracionesObservaciones(String aclaracionesObservaciones) {
        this.aclaracionesObservaciones = aclaracionesObservaciones;
    }
}
