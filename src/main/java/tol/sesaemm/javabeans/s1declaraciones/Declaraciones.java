/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class Declaraciones implements Serializable{
    
    private DeclaracionesPagination pagination;
    private ArrayList<DeclaracionesResults> results;

    public DeclaracionesPagination getPagination() {
        return pagination;
    }

    public void setPagination(DeclaracionesPagination pagination) {
        this.pagination = pagination;
    }

    public ArrayList<DeclaracionesResults> getResults() {
        return results;
    }

    public void setResults(ArrayList<DeclaracionesResults> results) {
        this.results = results;
    }
}
