/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesTipoBienPrestamo implements Serializable{
    
    private DeclaracionesInmueble inmueble;
    private DeclaracionesVehiculoTipoBienPrestamo vehiculo;

    public DeclaracionesInmueble getInmueble() {
        return inmueble;
    }

    public void setInmueble(DeclaracionesInmueble inmueble) {
        this.inmueble = inmueble;
    }

    public DeclaracionesVehiculoTipoBienPrestamo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(DeclaracionesVehiculoTipoBienPrestamo vehiculo) {
        this.vehiculo = vehiculo;
    }
}
