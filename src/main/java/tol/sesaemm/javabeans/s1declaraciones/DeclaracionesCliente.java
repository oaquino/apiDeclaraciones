/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesCliente implements Serializable{
    
    private String tipoOperacion;
    private Boolean realizaActividadLucrativa;
    private String tipoRelacion;
    private DeclaracionesEmpresa empresa;
    private DeclaracionesClientePrincipal clientePrincipal;
    private DeclaracionesSector sector;
    private DeclaracionesMonto montoAproximadoGanancia;
    private DeclaracionesUbicacion ubicacion;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public Boolean getRealizaActividadLucrativa() {
        return realizaActividadLucrativa;
    }

    public void setRealizaActividadLucrativa(Boolean realizaActividadLucrativa) {
        this.realizaActividadLucrativa = realizaActividadLucrativa;
    }

    public String getTipoRelacion() {
        return tipoRelacion;
    }

    public void setTipoRelacion(String tipoRelacion) {
        this.tipoRelacion = tipoRelacion;
    }

    public DeclaracionesEmpresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(DeclaracionesEmpresa empresa) {
        this.empresa = empresa;
    }

    public DeclaracionesClientePrincipal getClientePrincipal() {
        return clientePrincipal;
    }

    public void setClientePrincipal(DeclaracionesClientePrincipal clientePrincipal) {
        this.clientePrincipal = clientePrincipal;
    }

    public DeclaracionesSector getSector() {
        return sector;
    }

    public void setSector(DeclaracionesSector sector) {
        this.sector = sector;
    }

    public DeclaracionesMonto getMontoAproximadoGanancia() {
        return montoAproximadoGanancia;
    }

    public void setMontoAproximadoGanancia(DeclaracionesMonto montoAproximadoGanancia) {
        this.montoAproximadoGanancia = montoAproximadoGanancia;
    }

    public DeclaracionesUbicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(DeclaracionesUbicacion ubicacion) {
        this.ubicacion = ubicacion;
    }
}
