/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesBienesInmueblesSort implements Serializable{
    
    private String superficieConstruccion;
    private String superficieTerreno;
    private String formaAdquisicion;
    private String valorAdquisicion;

    public String getSuperficieConstruccion() {
        return superficieConstruccion;
    }

    public void setSuperficieConstruccion(String superficieConstruccion) {
        this.superficieConstruccion = superficieConstruccion;
    }

    public String getSuperficieTerreno() {
        return superficieTerreno;
    }

    public void setSuperficieTerreno(String superficieTerreno) {
        this.superficieTerreno = superficieTerreno;
    }

    public String getFormaAdquisicion() {
        return formaAdquisicion;
    }

    public void setFormaAdquisicion(String formaAdquisicion) {
        this.formaAdquisicion = formaAdquisicion;
    }

    public String getValorAdquisicion() {
        return valorAdquisicion;
    }

    public void setValorAdquisicion(String valorAdquisicion) {
        this.valorAdquisicion = valorAdquisicion;
    }
}
