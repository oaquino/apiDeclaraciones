/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesMetadata
  {

    private String actualizacion;
    private String institucion;
    private String contacto;
    private String personaContacto;
    private String tipo;

    public String getActualizacion()
      {
        return actualizacion;
      }

    public void setActualizacion(String actualizacion)
      {
        this.actualizacion = actualizacion;
      }

    public String getInstitucion()
      {
        return institucion;
      }

    public void setInstitucion(String institucion)
      {
        this.institucion = institucion;
      }

    public String getContacto()
      {
        return contacto;
      }

    public void setContacto(String contacto)
      {
        this.contacto = contacto;
      }

    public String getPersonaContacto()
      {
        return personaContacto;
      }

    public void setPersonaContacto(String personaContacto)
      {
        this.personaContacto = personaContacto;
      }

    public String getTipo()
      {
        return tipo;
      }

    public void setTipo(String tipo)
      {
        this.tipo = tipo;
      }

  }
