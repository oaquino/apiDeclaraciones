/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesEmpresa implements Serializable{
    
    private String nombreEmpresaServicio;
    private String rfc;

    public String getNombreEmpresaServicio() {
        return nombreEmpresaServicio;
    }

    public void setNombreEmpresaServicio(String nombreEmpresaServicio) {
        this.nombreEmpresaServicio = nombreEmpresaServicio;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }
}
