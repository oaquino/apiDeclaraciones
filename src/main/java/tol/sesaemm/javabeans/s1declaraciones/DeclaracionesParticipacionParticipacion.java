/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesParticipacionParticipacion implements Serializable{
    
    private String tipoOperacion;
    private String tipoRelacion;
    private String nombreEmpresaSociedadAsociacion;
    private String rfc;
    private Integer porcentajeParticipacion;
    private DeclaracionesTipoParticipacion tipoParticipacion;
    private Boolean recibeRemuneracion;
    private DeclaracionesMonto montoMensual;
    private DeclaracionesUbicacion ubicacion;
    private DeclaracionesSector sector;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getTipoRelacion() {
        return tipoRelacion;
    }

    public void setTipoRelacion(String tipoRelacion) {
        this.tipoRelacion = tipoRelacion;
    }

    public String getNombreEmpresaSociedadAsociacion() {
        return nombreEmpresaSociedadAsociacion;
    }

    public void setNombreEmpresaSociedadAsociacion(String nombreEmpresaSociedadAsociacion) {
        this.nombreEmpresaSociedadAsociacion = nombreEmpresaSociedadAsociacion;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public Integer getPorcentajeParticipacion() {
        return porcentajeParticipacion;
    }

    public void setPorcentajeParticipacion(Integer porcentajeParticipacion) {
        this.porcentajeParticipacion = porcentajeParticipacion;
    }

    public DeclaracionesTipoParticipacion getTipoParticipacion() {
        return tipoParticipacion;
    }

    public void setTipoParticipacion(DeclaracionesTipoParticipacion tipoParticipacion) {
        this.tipoParticipacion = tipoParticipacion;
    }

    public Boolean getRecibeRemuneracion() {
        return recibeRemuneracion;
    }

    public void setRecibeRemuneracion(Boolean recibeRemuneracion) {
        this.recibeRemuneracion = recibeRemuneracion;
    }

    public DeclaracionesMonto getMontoMensual() {
        return montoMensual;
    }

    public void setMontoMensual(DeclaracionesMonto montoMensual) {
        this.montoMensual = montoMensual;
    }

    public DeclaracionesUbicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(DeclaracionesUbicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public DeclaracionesSector getSector() {
        return sector;
    }

    public void setSector(DeclaracionesSector sector) {
        this.sector = sector;
    }
}
