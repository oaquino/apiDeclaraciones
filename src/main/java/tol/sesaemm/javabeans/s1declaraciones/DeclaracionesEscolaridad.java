/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesEscolaridad implements Serializable{
    
    private String tipoOperacion;
    private DeclaracionesNivel nivel;
    private DeclaracionesInstitucionEducativa institucionEducativa;
    private String carreraAreaConocimiento;
    private String estatus;
    private String documentoObtenido;
    private String fechaObtencion;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public DeclaracionesNivel getNivel() {
        return nivel;
    }

    public void setNivel(DeclaracionesNivel nivel) {
        this.nivel = nivel;
    }

    public DeclaracionesInstitucionEducativa getInstitucionEducativa() {
        return institucionEducativa;
    }

    public void setInstitucionEducativa(DeclaracionesInstitucionEducativa institucionEducativa) {
        this.institucionEducativa = institucionEducativa;
    }

    public String getCarreraAreaConocimiento() {
        return carreraAreaConocimiento;
    }

    public void setCarreraAreaConocimiento(String carreraAreaConocimiento) {
        this.carreraAreaConocimiento = carreraAreaConocimiento;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getDocumentoObtenido() {
        return documentoObtenido;
    }

    public void setDocumentoObtenido(String documentoObtenido) {
        this.documentoObtenido = documentoObtenido;
    }

    public String getFechaObtencion() {
        return fechaObtencion;
    }

    public void setFechaObtencion(String fechaObtencion) {
        this.fechaObtencion = fechaObtencion;
    }
}
