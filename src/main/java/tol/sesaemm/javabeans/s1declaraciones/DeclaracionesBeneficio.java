/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesBeneficio implements Serializable{
    
    private String tipoOperacion;
    private String tipoPersona;
    private DeclaracionesTipoBeneficio tipoBeneficio;
    private ArrayList<DeclaracionesBeneficiariosPrograma> beneficiario;
    private DeclaracionesOtorgante otorgante;
    private String formaRecepcion;
    private String especifiqueBeneficio;
    private DeclaracionesMonto montoMensualAproximado;
    private DeclaracionesSector sector;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public DeclaracionesTipoBeneficio getTipoBeneficio() {
        return tipoBeneficio;
    }

    public void setTipoBeneficio(DeclaracionesTipoBeneficio tipoBeneficio) {
        this.tipoBeneficio = tipoBeneficio;
    }

    public ArrayList<DeclaracionesBeneficiariosPrograma> getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(ArrayList<DeclaracionesBeneficiariosPrograma> beneficiario) {
        this.beneficiario = beneficiario;
    }

    public DeclaracionesOtorgante getOtorgante() {
        return otorgante;
    }

    public void setOtorgante(DeclaracionesOtorgante otorgante) {
        this.otorgante = otorgante;
    }

    public String getFormaRecepcion() {
        return formaRecepcion;
    }

    public void setFormaRecepcion(String formaRecepcion) {
        this.formaRecepcion = formaRecepcion;
    }

    public String getEspecifiqueBeneficio() {
        return especifiqueBeneficio;
    }

    public void setEspecifiqueBeneficio(String especifiqueBeneficio) {
        this.especifiqueBeneficio = especifiqueBeneficio;
    }

    public DeclaracionesMonto getMontoMensualAproximado() {
        return montoMensualAproximado;
    }

    public void setMontoMensualAproximado(DeclaracionesMonto montoMensualAproximado) {
        this.montoMensualAproximado = montoMensualAproximado;
    }

    public DeclaracionesSector getSector() {
        return sector;
    }

    public void setSector(DeclaracionesSector sector) {
        this.sector = sector;
    }
}
