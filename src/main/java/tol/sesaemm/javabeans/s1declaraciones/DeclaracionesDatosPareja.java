/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesDatosPareja implements Serializable{
    
    private Boolean ninguno;
    private String tipoOperacion;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String fechaNacimiento;
    private String rfc;
    private String relacionConDeclarante;
    private Boolean ciudadanoExtranjero;
    private String curp;
    private Boolean esDependienteEconomico;
    private Boolean habitaDomicilioDeclarante;
    private String lugarDondeReside;
    private DeclaracionesDomicilioMexico domicilioMexico;
    private DeclaracionesDomicilioExtranjero domicilioExtranjero;
    private DeclaracionesActividadLaboral actividadLaboral;
    private DeclaracionesActividadLaboralSectorPublicoDatosPareja actividadLaboralSectorPublico;
    private DeclaracionesActividadLaboralSectorPrivadoOtroDatosPareja actividadLaboralSectorPrivadoOtro;
    private String aclaracionesObservaciones;

    public Boolean getNinguno() {
        return ninguno;
    }

    public void setNinguno(Boolean ninguno) {
        this.ninguno = ninguno;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getRelacionConDeclarante() {
        return relacionConDeclarante;
    }

    public void setRelacionConDeclarante(String relacionConDeclarante) {
        this.relacionConDeclarante = relacionConDeclarante;
    }

    public Boolean getCiudadanoExtranjero() {
        return ciudadanoExtranjero;
    }

    public void setCiudadanoExtranjero(Boolean ciudadanoExtranjero) {
        this.ciudadanoExtranjero = ciudadanoExtranjero;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public Boolean getEsDependienteEconomico() {
        return esDependienteEconomico;
    }

    public void setEsDependienteEconomico(Boolean esDependienteEconomico) {
        this.esDependienteEconomico = esDependienteEconomico;
    }

    public Boolean getHabitaDomicilioDeclarante() {
        return habitaDomicilioDeclarante;
    }

    public void setHabitaDomicilioDeclarante(Boolean habitaDomicilioDeclarante) {
        this.habitaDomicilioDeclarante = habitaDomicilioDeclarante;
    }

    public String getLugarDondeReside() {
        return lugarDondeReside;
    }

    public void setLugarDondeReside(String lugarDondeReside) {
        this.lugarDondeReside = lugarDondeReside;
    }

    public DeclaracionesDomicilioMexico getDomicilioMexico() {
        return domicilioMexico;
    }

    public void setDomicilioMexico(DeclaracionesDomicilioMexico domicilioMexico) {
        this.domicilioMexico = domicilioMexico;
    }

    public DeclaracionesDomicilioExtranjero getDomicilioExtranjero() {
        return domicilioExtranjero;
    }

    public void setDomicilioExtranjero(DeclaracionesDomicilioExtranjero domicilioExtranjero) {
        this.domicilioExtranjero = domicilioExtranjero;
    }

    public DeclaracionesActividadLaboral getActividadLaboral() {
        return actividadLaboral;
    }

    public void setActividadLaboral(DeclaracionesActividadLaboral actividadLaboral) {
        this.actividadLaboral = actividadLaboral;
    }

    public DeclaracionesActividadLaboralSectorPublicoDatosPareja getActividadLaboralSectorPublico() {
        return actividadLaboralSectorPublico;
    }

    public void setActividadLaboralSectorPublico(DeclaracionesActividadLaboralSectorPublicoDatosPareja actividadLaboralSectorPublico) {
        this.actividadLaboralSectorPublico = actividadLaboralSectorPublico;
    }

    public DeclaracionesActividadLaboralSectorPrivadoOtroDatosPareja getActividadLaboralSectorPrivadoOtro() {
        return actividadLaboralSectorPrivadoOtro;
    }

    public void setActividadLaboralSectorPrivadoOtro(DeclaracionesActividadLaboralSectorPrivadoOtroDatosPareja actividadLaboralSectorPrivadoOtro) {
        this.actividadLaboralSectorPrivadoOtro = actividadLaboralSectorPrivadoOtro;
    }

    public String getAclaracionesObservaciones() {
        return aclaracionesObservaciones;
    }

    public void setAclaracionesObservaciones(String aclaracionesObservaciones) {
        this.aclaracionesObservaciones = aclaracionesObservaciones;
    }
}
