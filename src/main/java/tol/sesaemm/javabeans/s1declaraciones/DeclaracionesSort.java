/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesSort implements Serializable{
    
    private String nombres;
    private String primerApellido;
    private String segundoApellido;
    private String escolaridadNivel;
    private DeclaracionesDatosEmpleoCargoComisionSort datosEmpleoCargoComision;
    private String totalIngresosNetos;
    private DeclaracionesBienesInmueblesSort bienesInmuebles;

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getEscolaridadNivel() {
        return escolaridadNivel;
    }

    public void setEscolaridadNivel(String escolaridadNivel) {
        this.escolaridadNivel = escolaridadNivel;
    }

    public DeclaracionesDatosEmpleoCargoComisionSort getDatosEmpleoCargoComision() {
        return datosEmpleoCargoComision;
    }

    public void setDatosEmpleoCargoComision(DeclaracionesDatosEmpleoCargoComisionSort datosEmpleoCargoComision) {
        this.datosEmpleoCargoComision = datosEmpleoCargoComision;
    }

    public String getTotalIngresosNetos() {
        return totalIngresosNetos;
    }

    public void setTotalIngresosNetos(String totalIngresosNetos) {
        this.totalIngresosNetos = totalIngresosNetos;
    }

    public DeclaracionesBienesInmueblesSort getBienesInmuebles() {
        return bienesInmuebles;
    }

    public void setBienesInmuebles(DeclaracionesBienesInmueblesSort bienesInmuebles) {
        this.bienesInmuebles = bienesInmuebles;
    }
}