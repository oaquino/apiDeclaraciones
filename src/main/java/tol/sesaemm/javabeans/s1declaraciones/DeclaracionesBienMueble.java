/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesBienMueble implements Serializable{
    
    private String tipoOperacion;
    private ArrayList<DeclaracionesTitularBien> titular;
    private DeclaracionesTipoBien tipoBien;
    private ArrayList<DeclaracionesTransmisor> transmisor;
    private ArrayList<DeclaracionesTercero> tercero;
    private String descripcionGeneralBien;
    private DeclaracionesFormaAdquisicion formaAdquisicion;
    private String formaPago;
    private DeclaracionesMonto valorAdquisicion;
    private String fechaAdquisicion;
    private DeclaracionesMotivoBaja motivoBaja;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public ArrayList<DeclaracionesTitularBien> getTitular() {
        return titular;
    }

    public void setTitular(ArrayList<DeclaracionesTitularBien> titular) {
        this.titular = titular;
    }

    public DeclaracionesTipoBien getTipoBien() {
        return tipoBien;
    }

    public void setTipoBien(DeclaracionesTipoBien tipoBien) {
        this.tipoBien = tipoBien;
    }

    public ArrayList<DeclaracionesTransmisor> getTransmisor() {
        return transmisor;
    }

    public void setTransmisor(ArrayList<DeclaracionesTransmisor> transmisor) {
        this.transmisor = transmisor;
    }

    public ArrayList<DeclaracionesTercero> getTercero() {
        return tercero;
    }

    public void setTercero(ArrayList<DeclaracionesTercero> tercero) {
        this.tercero = tercero;
    }

    public String getDescripcionGeneralBien() {
        return descripcionGeneralBien;
    }

    public void setDescripcionGeneralBien(String descripcionGeneralBien) {
        this.descripcionGeneralBien = descripcionGeneralBien;
    }

    public DeclaracionesFormaAdquisicion getFormaAdquisicion() {
        return formaAdquisicion;
    }

    public void setFormaAdquisicion(DeclaracionesFormaAdquisicion formaAdquisicion) {
        this.formaAdquisicion = formaAdquisicion;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public DeclaracionesMonto getValorAdquisicion() {
        return valorAdquisicion;
    }

    public void setValorAdquisicion(DeclaracionesMonto valorAdquisicion) {
        this.valorAdquisicion = valorAdquisicion;
    }

    public String getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(String fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public DeclaracionesMotivoBaja getMotivoBaja() {
        return motivoBaja;
    }

    public void setMotivoBaja(DeclaracionesMotivoBaja motivoBaja) {
        this.motivoBaja = motivoBaja;
    }
}
